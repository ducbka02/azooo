module.exports = {
  project: {
    ios: {},
    android: {},
  },
  dependencies: {
    'react-native-maps': {
      platforms: {
        ios: null,
      },
    },
  },
  assets: ['./src/assets/fonts'],
};
