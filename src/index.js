import React, {Component} from 'react';
import {Provider} from 'react-redux';
import Navigator from './screens';
import store from './store';
//import {enableScreens} from 'react-native-screens';

//enableScreens();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}
