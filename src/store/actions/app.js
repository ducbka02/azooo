import {
    GET_APP_INFO,
    DRAWER_SEARCH_REQUEST,
    DRAWER_GET_PRODUCT_CATEGORY_REQUEST,
    DRAWER_GET_PRODUCT_COLLECTION_REQUEST,
    DRAWER_GET_PRODUCT_BRAND_REQUEST,
    DRAWER_GET_PRODUCT_SALE_REQUEST,
    DRAWER_HARD_PRODUCT_LIST,
    GET_LOCATION
} from "../types/app";

export function getAppInfo(cb) {
    return {
        type: GET_APP_INFO,
        cb
    };
}

export function getLocation(info, cb) {
    return {
        type: GET_LOCATION,
        info,
        cb
    };
}

export function drawerSeachProduct(keyword, cb) {
    return {
        type: DRAWER_SEARCH_REQUEST,
        keyword,
        cb
    };
}

export function drawerGetProductCategory(id, cb) {
    return {
        type: DRAWER_GET_PRODUCT_CATEGORY_REQUEST,
        id,
        cb
    };
}

export function drawerGetProductCollection(id, cb) {
    return {
        type: DRAWER_GET_PRODUCT_COLLECTION_REQUEST,
        id,
        cb
    };
}

export function drawerGetProductBrand(id, cb) {
    return {
        type: DRAWER_GET_PRODUCT_BRAND_REQUEST,
        id,
        cb
    };
}

export function drawerGetProductSale(id, cb) {
    return {
        type: DRAWER_GET_PRODUCT_SALE_REQUEST,
        id,
        cb
    };
}

export function hardDrawerProductList(payload) {
    return {
        type: DRAWER_HARD_PRODUCT_LIST,
        payload
    };
}