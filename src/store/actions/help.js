import {
    REGISTER_AGENCY_REQUEST, FEEDBACK_REQUEST,
} from "../types/help";

export function registerAgency(data, cb) {
    return {
        type: REGISTER_AGENCY_REQUEST,
        data,
        cb
    };
}

export function sendFeedback(data, cb) {
    return {
        type: FEEDBACK_REQUEST,
        data,
        cb
    };
}