import {
    REGISTER_AGENCY_REQUEST,
    REGISTER_AGENCY_SUCCESS,
    REGISTER_AGENCY_ERROR,
    FEEDBACK_REQUEST,
    FEEDBACK_SUCCESS,
    FEEDBACK_ERROR,
} from "../types/help";

const defaultState = {
    helpLoading: false
};

function reducer(state = defaultState, action) {
    switch (action.type) {
        case REGISTER_AGENCY_REQUEST:
            return {
                ...state,
                helpLoading: true,
            };
        case REGISTER_AGENCY_SUCCESS:
            return {
                ...state,
                helpLoading: false,
            };
        case REGISTER_AGENCY_ERROR:
            return {
                ...state,
                helpLoading: false,
            };
        case FEEDBACK_REQUEST:
            return {
                ...state,
                helpLoading: true,
            };
        case FEEDBACK_SUCCESS:
            return {
                ...state,
                helpLoading: false,
            };
        case FEEDBACK_ERROR:
            return {
                ...state,
                helpLoading: false,
            };
        default:
            return state;
    }
}

export default reducer;