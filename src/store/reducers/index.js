// @flow

import { combineReducers } from 'redux';
import app from './app';
import help from './help';

export const subReducers = {
    app,
    help
};

export default combineReducers(subReducers);
