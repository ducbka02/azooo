import {
    GET_APP_INFO,
    GET_FAQ_INFO_SUCESS,
    GET_SUBFAQ_INFO_SUCESS,
    GET_BANNER_SUCESS,
    GET_BRAND_SUCESS,
    GET_PROMOTION_SUCESS,
    GET_COLLECTION_SUCESS,
    GET_CATEGORY_PRODUCT_SUCESS,
    GET_BEST_SELLING_SUCESS,
    GET_NEW_PRODUCT_SUCESS,
    GET_POPULAR_KEYWORD_SUCESS,
    GET_SUBCATEGORY_PRODUCT_SUCESS,
    DRAWER_SEARCH_REQUEST,
    DRAWER_SEARCH_SUCCESS,
    DRAWER_SEARCH_ERROR,
    DRAWER_GET_PRODUCT_CATEGORY_REQUEST,
    DRAWER_GET_PRODUCT_CATEGORY_SUCCESS,
    DRAWER_GET_PRODUCT_CATEGORY_ERROR,
    DRAWER_GET_PRODUCT_COLLECTION_REQUEST,
    DRAWER_GET_PRODUCT_COLLECTION_SUCCESS,
    DRAWER_GET_PRODUCT_COLLECTION_ERROR,
    DRAWER_GET_PRODUCT_BRAND_REQUEST,
    DRAWER_GET_PRODUCT_BRAND_SUCCESS,
    DRAWER_GET_PRODUCT_BRAND_ERROR,
    DRAWER_GET_PRODUCT_SALE_REQUEST,
    DRAWER_GET_PRODUCT_SALE_SUCCESS,
    DRAWER_GET_PRODUCT_SALE_ERROR,
    DRAWER_HARD_PRODUCT_LIST,
    GET_LOCATION
} from "../types/app";

const defaultState = {
    listBestSelling: [],
    listNewProduct: [],
    listBanner: [],
    listCollection: [],
    listBrand: [],
    listPromotion: [],
    listCategoryProduct: [],
    listSubCategoryProduct: {},
    listFAQ: [],
    listSubFAQ: {},
    listPopularKeyword: [],
    drawerSearchProcess: false,
    listDrawerSearchResult: [],
    listDrawerProduct: [],
    region: {
        latitude: 21.004584,
        longitude: 105.804393,
        latitudeDelta: 0.08,
        longitudeDelta: 0.08 * 0.5
    }
};

function reducer(state = defaultState, action) {
    switch (action.type) {
        case GET_LOCATION:
            return {
                ...state,
                region: {
                    latitude: action.info.lat,
                    longitude: action.info.long,
                    latitudeDelta: 0.08,
                    longitudeDelta: 0.08 * 0.5
                }
            };
        case GET_BEST_SELLING_SUCESS:
            return {
                ...state,
                listBestSelling: action.payload
            };
        case GET_NEW_PRODUCT_SUCESS:
            return {
                ...state,
                listNewProduct: action.payload
            };
        case GET_POPULAR_KEYWORD_SUCESS:
            return {
                ...state,
                listPopularKeyword: action.payload,
            }
        case GET_BANNER_SUCESS:
            return {
                ...state,
                listBanner: action.payload
            };
        case GET_COLLECTION_SUCESS:
            return {
                ...state,
                listCollection: action.payload
            };
        case GET_BRAND_SUCESS:
            return {
                ...state,
                listBrand: action.payload
            };
        case GET_PROMOTION_SUCESS:
            return {
                ...state,
                listPromotion: action.payload
            };
        case GET_CATEGORY_PRODUCT_SUCESS:
            return {
                ...state,
                listCategoryProduct: action.payload
            };
        case GET_SUBCATEGORY_PRODUCT_SUCESS: {
            let newList = state.listSubCategoryProduct;
            const { id, payload } = action;
            newList[id] = payload;
            return {
                ...state,
                listSubCategoryProduct: newList,
            };
        }
        case GET_FAQ_INFO_SUCESS:
            return {
                ...state,
                listFAQ: action.payload
            };
        case GET_SUBFAQ_INFO_SUCESS: {
            let newSubFAQ = state.listSubFAQ;
            const { id, payload } = action;
            newSubFAQ[id] = payload;
            return {
                ...state,
                listSubFAQ: newSubFAQ,
            };
        }
        case DRAWER_SEARCH_REQUEST:
            return {
                ...state,
                drawerSearchProcess: true,
            };
        case DRAWER_SEARCH_SUCCESS:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerSearchResult: action.payload
            };
        case DRAWER_SEARCH_ERROR:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerSearchResult: []
            };
        case DRAWER_GET_PRODUCT_CATEGORY_REQUEST:
            return {
                ...state,
                drawerSearchProcess: true,
            };
        case DRAWER_GET_PRODUCT_CATEGORY_SUCCESS:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerProduct: action.payload
            };
        case DRAWER_GET_PRODUCT_CATEGORY_ERROR:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerProduct: []
            };
        case DRAWER_GET_PRODUCT_COLLECTION_REQUEST:
            return {
                ...state,
                drawerSearchProcess: true,
            };
        case DRAWER_GET_PRODUCT_COLLECTION_SUCCESS:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerProduct: action.payload
            };
        case DRAWER_GET_PRODUCT_COLLECTION_ERROR:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerProduct: []
            };
        case DRAWER_GET_PRODUCT_BRAND_REQUEST:
            return {
                ...state,
                drawerSearchProcess: true,
            };
        case DRAWER_GET_PRODUCT_BRAND_SUCCESS:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerProduct: action.payload
            };
        case DRAWER_GET_PRODUCT_BRAND_ERROR:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerProduct: []
            };
        case DRAWER_GET_PRODUCT_SALE_REQUEST:
            return {
                ...state,
                drawerSearchProcess: true,
            };
        case DRAWER_GET_PRODUCT_SALE_SUCCESS:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerProduct: action.payload
            };
        case DRAWER_GET_PRODUCT_SALE_ERROR:
            return {
                ...state,
                drawerSearchProcess: false,
                listDrawerProduct: []
            };
        case DRAWER_HARD_PRODUCT_LIST:
            return {
                ...state,
                listDrawerProduct: action.payload
            }
        default:
            return state;
    }
}

export default reducer;