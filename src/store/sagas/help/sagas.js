// @flow
/* eslint-disable no-use-before-define */
import { call, put, select } from "redux-saga/effects";
import { getRegisterAgency, getFeedback } from 'src/services/api/help';
import { REGISTER_AGENCY_SUCCESS, REGISTER_AGENCY_ERROR, FEEDBACK_SUCCESS, FEEDBACK_ERROR } from '../../types/help';

export function* registerAgency(action) {
  try {
    const payload = yield call(getRegisterAgency, action.data);
    if (payload.status === 1) {
      yield put({ type: REGISTER_AGENCY_SUCCESS, payload: payload.message });
    } else {
      yield put({ type: REGISTER_AGENCY_ERROR, payload: payload.message });
    }
    if (action.cb) {
      action.cb();
    }
    console.warn(payload);
  } catch (error) {
    yield put({ type: REGISTER_AGENCY_ERROR, payload: error.message });
    if (action.cb) {
      action.cb();
    }
  }
}

export function* sendFeedback(action) {
  try {
    const payload = yield call(getFeedback, action.data);
    if (payload.status === 1) {
      yield put({ type: FEEDBACK_SUCCESS, payload: payload.message });
    } else {
      yield put({ type: FEEDBACK_ERROR, payload: payload.message });
    }
    if (action.cb) {
      action.cb();
    }
    console.warn(payload);
  } catch (error) {
    yield put({ type: FEEDBACK_ERROR, payload: error.message });
    if (action.cb) {
      action.cb();
    }
  }
}
