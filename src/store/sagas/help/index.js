import { takeEvery } from 'redux-saga/effects';
import {
  registerAgency,
  sendFeedback
} from './sagas';
import {
  REGISTER_AGENCY_REQUEST,
  FEEDBACK_REQUEST
} from '../../types/help';

/**
 * @desc Root navigation saga. Handles showing of correct screens flow.
 * @return {void}
 */
export default function* rootSaga() {
  yield takeEvery([REGISTER_AGENCY_REQUEST], registerAgency);
  yield takeEvery([FEEDBACK_REQUEST], sendFeedback);
}
