import {takeEvery} from 'redux-saga/effects';
import {
  getAppInfoSaga,
  drawerSearchProductSaga,
  drawerGetProductCategorySaga,
  drawerGetProductCollectionSaga,
  drawerGetProductBrandSaga,
  drawerGetProductSaleSaga,
} from './sagas';
import {
  GET_APP_INFO,
  DRAWER_SEARCH_REQUEST,
  DRAWER_GET_PRODUCT_CATEGORY_REQUEST,
  DRAWER_GET_PRODUCT_COLLECTION_REQUEST,
  DRAWER_GET_PRODUCT_BRAND_REQUEST,
  DRAWER_GET_PRODUCT_SALE_REQUEST,
} from '../../types/app';

/**
 * @desc Root navigation saga. Handles showing of correct screens flow.
 * @return {void}
 */
export default function* rootSaga() {
  yield takeEvery([GET_APP_INFO], getAppInfoSaga);
  yield takeEvery([DRAWER_SEARCH_REQUEST], drawerSearchProductSaga);
  yield takeEvery(
    [DRAWER_GET_PRODUCT_CATEGORY_REQUEST],
    drawerGetProductCategorySaga,
  );
  yield takeEvery(
    [DRAWER_GET_PRODUCT_COLLECTION_REQUEST],
    drawerGetProductCollectionSaga,
  );
  yield takeEvery(
    [DRAWER_GET_PRODUCT_BRAND_REQUEST],
    drawerGetProductBrandSaga,
  );
  yield takeEvery([DRAWER_GET_PRODUCT_SALE_REQUEST], drawerGetProductSaleSaga);
}
