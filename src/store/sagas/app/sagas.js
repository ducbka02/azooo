// @flow
/* eslint-disable no-use-before-define */
import {call, put, all} from 'redux-saga/effects';
import get from 'lodash/get';
import {
  getListFAQ,
  getListFAQById,
  getHomeBanner,
  getCategoryProduct,
  getListCollection,
  getListBrand,
  getListPromotion,
  getSubCategoryProduct,
  getListBestSelling,
  getListNewProduct,
  getListKeywordPopular,
  azSearchKeyword,
  getProductInCategory,
  getListProductInCollection,
  getListProductInBrand,
  getListProductInSale,
} from 'src/services/api/app';
import {
  GET_BEST_SELLING_SUCESS,
  GET_NEW_PRODUCT_SUCESS,
  GET_FAQ_INFO_SUCESS,
  GET_SUBFAQ_INFO_SUCESS,
  GET_BANNER_SUCESS,
  GET_BRAND_SUCESS,
  GET_PROMOTION_SUCESS,
  GET_COLLECTION_SUCESS,
  GET_CATEGORY_PRODUCT_SUCESS,
  GET_SUBCATEGORY_PRODUCT_SUCESS,
  GET_POPULAR_KEYWORD_SUCESS,
  DRAWER_SEARCH_SUCCESS,
  DRAWER_SEARCH_ERROR,
  DRAWER_GET_PRODUCT_CATEGORY_SUCCESS,
  DRAWER_GET_PRODUCT_CATEGORY_ERROR,
  DRAWER_GET_PRODUCT_COLLECTION_SUCCESS,
  DRAWER_GET_PRODUCT_COLLECTION_ERROR,
  DRAWER_GET_PRODUCT_BRAND_SUCCESS,
  DRAWER_GET_PRODUCT_BRAND_ERROR,
  DRAWER_GET_PRODUCT_SALE_SUCCESS,
  DRAWER_GET_PRODUCT_SALE_ERROR,
} from '../../types/app';

export function* getAppInfoSaga(action) {
  yield call(getBestSellingSaga);
  yield call(getNewProductSaga);
  yield call(getAppBannerSaga);
  yield call(getListCollectionSaga);
  yield call(getListFAQSaga);
  yield call(getListBrandSaga);
  yield call(getListPromotionSaga);
  yield call(getListCategorySaga);
  yield call(getListKeywordSaga);

  if (action.cb) {
    action.cb();
  }
}

export function* getBestSellingSaga(action) {
  try {
    const limit = get(action, 'limit', 20);
    const offset = get(action, 'offset', 0);
    const listBestSelling = yield call(getListBestSelling, limit, offset);
    yield put({type: GET_BEST_SELLING_SUCESS, payload: listBestSelling});
  } catch (error) {}
}

export function* getNewProductSaga(action) {
  try {
    const limit = get(action, 'limit', 20);
    const offset = get(action, 'offset', 0);
    const listNewProduct = yield call(getListNewProduct, limit, offset);
    yield put({type: GET_NEW_PRODUCT_SUCESS, payload: listNewProduct});
  } catch (error) {}
}

export function* getAppBannerSaga() {
  try {
    const listBanner = yield call(getHomeBanner);
    yield put({type: GET_BANNER_SUCESS, payload: listBanner});
  } catch (error) {}
}

export function* getListCollectionSaga() {
  try {
    const listCollection = yield call(getListCollection);
    yield put({type: GET_COLLECTION_SUCESS, payload: listCollection});
  } catch (error) {}
}

export function* getListFAQSaga() {
  try {
    const listFAQ = yield call(getListFAQ);
    yield put({type: GET_FAQ_INFO_SUCESS, payload: listFAQ});
    for (const faq of listFAQ) {
      const id = faq.tid;
      const subFAQ = yield call(getListFAQById, id);
      yield put({type: GET_SUBFAQ_INFO_SUCESS, payload: subFAQ, id: id});
    }
  } catch (error) {}
}

export function* getListBrandSaga() {
  try {
    const listBrand = yield call(getListBrand);
    yield put({type: GET_BRAND_SUCESS, payload: listBrand});
  } catch (error) {}
}

export function* getListPromotionSaga() {
  try {
    const listPromotion = yield call(getListPromotion);
    yield put({type: GET_PROMOTION_SUCESS, payload: listPromotion});
  } catch (error) {}
}

export function* getListCategorySaga() {
  try {
    const listCategoryProduct = yield call(getCategoryProduct);
    yield put({
      type: GET_CATEGORY_PRODUCT_SUCESS,
      payload: listCategoryProduct,
    });
    for (const category of listCategoryProduct) {
      const id = category.category_id;
      const subCategory = yield call(getSubCategoryProduct, id);
      yield put({
        type: GET_SUBCATEGORY_PRODUCT_SUCESS,
        payload: subCategory,
        id: id,
      });
    }
  } catch (error) {}
}

export function* getListKeywordSaga() {
  try {
    const listPopularKeyword = yield call(getListKeywordPopular);
    yield put({type: GET_POPULAR_KEYWORD_SUCESS, payload: listPopularKeyword});
  } catch (error) {}
}

export function* drawerSearchProductSaga(action) {
  try {
    if (action.keyword.length === 0) {
      yield put({type: DRAWER_SEARCH_SUCCESS, payload: []});
      if (action.cb) {
        action.cb([]);
      }
    } else {
      const listSearchProduct = yield call(azSearchKeyword, action.keyword);
      yield put({type: DRAWER_SEARCH_SUCCESS, payload: listSearchProduct});
      if (action.cb) {
        action.cb(listSearchProduct);
      }
    }
  } catch (error) {
    yield put({type: DRAWER_SEARCH_ERROR});
    if (action.cb) {
      action.cb([]);
    }
  }
}

export function* drawerGetProductCategorySaga(action) {
  try {
    const listSearchProduct = yield call(getProductInCategory, action.id);
    yield put({
      type: DRAWER_GET_PRODUCT_CATEGORY_SUCCESS,
      payload: listSearchProduct,
    });
    if (action.cb) {
      action.cb(listSearchProduct);
    }
  } catch (error) {
    yield put({type: DRAWER_GET_PRODUCT_CATEGORY_ERROR});
    if (action.cb) {
      action.cb([]);
    }
  }
}

export function* drawerGetProductCollectionSaga(action) {
  try {
    const listSearchProduct = yield call(getListProductInCollection, action.id);
    yield put({
      type: DRAWER_GET_PRODUCT_COLLECTION_SUCCESS,
      payload: listSearchProduct,
    });
    if (action.cb) {
      action.cb(listSearchProduct);
    }
  } catch (error) {
    yield put({type: DRAWER_GET_PRODUCT_COLLECTION_ERROR});
    if (action.cb) {
      action.cb([]);
    }
  }
}

export function* drawerGetProductSaleSaga(action) {
  try {
    const listSearchProduct = yield call(getListProductInSale, action.id);
    yield put({
      type: DRAWER_GET_PRODUCT_SALE_SUCCESS,
      payload: listSearchProduct,
    });
    if (action.cb) {
      action.cb(listSearchProduct);
    }
  } catch (error) {
    yield put({type: DRAWER_GET_PRODUCT_SALE_ERROR});
    if (action.cb) {
      action.cb([]);
    }
  }
}

export function* drawerGetProductBrandSaga(action) {
  try {
    const listSearchProduct = yield call(getListProductInBrand, action.id);
    yield put({
      type: DRAWER_GET_PRODUCT_BRAND_SUCCESS,
      payload: listSearchProduct,
    });
    if (action.cb) {
      action.cb(listSearchProduct);
    }
  } catch (error) {
    yield put({type: DRAWER_GET_PRODUCT_BRAND_ERROR});
    if (action.cb) {
      action.cb([]);
    }
  }
}
