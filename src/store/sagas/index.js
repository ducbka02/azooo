// @flow

import {all, call} from 'redux-saga/effects';
import app from './app';
import help from './help';

export default function* rootSaga() {
  yield all([call(app), call(help)]);
}
