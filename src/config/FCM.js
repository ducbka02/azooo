import firebase from 'react-native-firebase';
import { Platform } from 'react-native';

class Fcm {
  logEvent (event, params?) {
    try {
      firebase.analytics().logEvent(event, params);
    } catch (e) {}
  }

  setCurrentScreen (screenName, screenClassOverride?) {
    try {
      firebase.analytics().setCurrentScreen(screenName, screenClassOverride);
    } catch (e) {}
  }

  initAnalytics () {
    try {
      firebase.analytics().setAnalyticsCollectionEnabled(true);
    } catch (e) {
      
    }
  }
}

export default new Fcm();
