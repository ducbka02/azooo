export default {
  pinkishColor: 'rgb(242, 114, 129)',
  pinkLightColor: 'rgb(255, 243, 244)',
  blackIconColor: 'rgb(60, 60, 60)',
  blackLightColor: 'rgb(112, 112, 112)',
  blackSuperLightColor: 'rgb(204, 204, 204)',
  placeholderColor: 'rgb(240, 240, 240)',
  pureWhite: '#FFFFFF',
  pureBlack: '#000000',
  darkBlue: '#2196F3',
  green: 'rgb(112, 173, 29)',
  greenSearch: '#5ddcf9',
  red: 'rgb(255, 50, 50)'
};
