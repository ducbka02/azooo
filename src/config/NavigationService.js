import {
  NavigationActions,
  NavigationContainerComponent,
  StackActions,
} from 'react-navigation';

let navigator;

export const ProductShowType = {
  COLLECTION: 'COLLECTION',
  SEARCH: 'SEARCH',
  CATEGORY: 'CATEGORY',
  SALE: 'SALE',
  BRAND: 'BRAND',
  PRODUCT_LIST: 'PRODUCT_LIST',
}

function setTopLevelNavigator(navigatorRef) {
  navigator = navigatorRef;
}

function navigate(routeName, params) {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function push(routeName, params) {
  navigator.push(routeName, params);
}

function goBack(key) {
  if (key) {
    navigator.dispatch(NavigationActions.back({ key }));
  } else {
    navigator.dispatch(NavigationActions.back());
  }
}

function resetToTabNavigator() {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: 'MainNavigator' }),
    ],
  });
  navigator.dispatch(resetAction);
}

function popToTop() {
  navigator.dispatch(StackActions.popToTop());
}

function goToCategoryNavigator(params) {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName: 'MainCategoryScreen',
      params,
    })
  );
}

export default {
  navigate,
  push,
  goBack,
  setTopLevelNavigator,
  resetToTabNavigator,
  popToTop,
  goToCategoryNavigator,
};
