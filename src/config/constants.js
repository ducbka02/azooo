export default {
  progressMaxValue: 100,
};

export const GoalType = {
  PAGES: 'PAGES',
  CHAPTERS: 'CHAPTERS',
  MINUTES: 'MINUTES',
  BOOKS: 'BOOKS',
  YESNO: 'YESNO',
};
