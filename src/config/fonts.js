export default {
  ROBOTO_BOLD: 'Roboto-Bold',
  ROBOTO_BOLD_ITALIC: 'Roboto-BoldItalic',
  ROBOTO_ITALIC: 'Roboto-Italic',
  ROBOTO_MEDIUM: 'Roboto-Medium',
  ROBOTO_MEDIUM_ITALIC: 'Roboto-MediumItalic',
  ROBOTO_REGULAR: 'Roboto-Regular',
  ROBOTO_THIN: 'Roboto-Thin',
  ROBOTO_THIN_ITALIC: 'Roboto-ThinItalic',
};
