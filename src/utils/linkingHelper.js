import { Linking, Platform } from 'react-native';
import _ from 'lodash';

const TEL = 'tel:';
const TELPROMPT = 'telprompt:';
const FACEBOOK_PROFILE = 'fb://profile/';
const FACEBOOK_PAGE = 'fb://page/';
const FACEBOOK_MESSENGER = 'https://m.me/';

const Supported = (url) => {
    return Linking.canOpenURL(url);
}

const Open = (url) => {
    return Linking.openURL(url);
}

const Launch = (url) => {
    return new Promise((resolve, reject) => {
        Supported(url).then(supported => {
            if (supported) {
                return Open(url);
            }
            reject();
        }).catch((error) => {
            reject(error);
        });
    });
}

const LaunchString = (name, url) => {
    return new Promise((resolve, reject) => {
        if (!_.isString(name)) reject('The provided ' + name + ' must be a string');

        Launch(url).then(() => resolve()).catch(error => reject(error));
    });
}

const Call = (phone, prompt = false) => {
    return new Promise((resolve, reject) => {
        if (!_.isString(phone)) reject('The provided phone number must be a string');

        let url = ((Platform.OS == 'ios' && prompt) ? TELPROMPT : TEL) + phone;

        Launch(url).then(() => resolve()).catch(error => {

            if (url.includes(TELPROMPT)) {
                return resolve();
            }

            reject(error);
        });
    });
}

const FacebookPage = (id) => {

    return new Promise((resolve, reject) => {
        let url = `${FACEBOOK_MESSENGER}${id}`;

        Launch(url).then(() => resolve()).catch(error => reject(error));
    });
}

const FacebookAgencyPage = (id) => {
    return new Promise((resolve, reject) => {
        let url = `${FACEBOOK_PROFILE}${id}`;

        //Launch(url).then(() => resolve()).catch(error => reject(error));
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                return Linking.openURL(url);
            } else {
                return Linking.openURL(`https://www.facebook.com/${id}`);
            }
        })
    });

}

const FacebookShopPage = (id) => {

    // return new Promise((resolve, reject) => {
    //     let url = `https://www.facebook.com/azooo.vn/`;

    //     Launch(url).then(() => resolve()).catch(error => reject(error));
    // });
    // Linking.canOpenURL("fb://page/292209594717822").then(supported => {
    //     if (supported) {
    //         return Linking.openURL("fb://page/292209594717822");
    //     } else {
    //         return Linking.openURL("https://www.facebook.com/292209594717822");
    //     }
    // })
    return new Promise((resolve, reject) => {
        let url = `${FACEBOOK_PROFILE}${id}`;

        Launch(url).then(() => resolve()).catch(error => reject(error));
    });
}

const FacebookYoutubeChannelPage = (id) => {

    return new Promise((resolve, reject) => {
        let url = "https://www.youtube.com/channel/UCMA33eCFvo4aHhXleWeCyTQ";

        Launch(url).then(() => resolve()).catch(error => reject(error));
    });
}

const FacebookInstagramPage = (id) => {

    return new Promise((resolve, reject) => {
        let url = "https://www.instagram.com/azooo_vn/";

        Launch(url).then(() => resolve()).catch(error => reject(error));
    });
}

export {
    Launch,
    Open,
    LaunchString,
    Supported,
    Call,
    FacebookPage,
    FacebookAgencyPage,
    FacebookShopPage,
    FacebookYoutubeChannelPage,
    FacebookInstagramPage
};