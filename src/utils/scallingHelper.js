import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const guidelineBaseWidth = 320;
const guidelineBaseHeight = 570;

export function scale(size) {
  return (height / guidelineBaseHeight) * size;
}

export function verticleSale(size) {
  return (width / guidelineBaseWidth) * size;
}

export function moderateScale(size, factor = 0.5) {
  return size + (scale(size) - size) * factor;
}
