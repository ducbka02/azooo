import * as yup from 'yup';

export const validationSchemaProfile = yup.object().shape({
  firstName: yup
    .string()
    .label('First Name')
    .required(),
  lastName: yup
    .string()
    .label('Last Name')
    .required(),
  email: yup
    .string()
    .label('Email')
    .email('Email is not valid')
    .required('Email is required'),
  password: yup
    .string()
    .label('Password')
    .required()
    .min(2, 'Seems a bit short...'),
  confirmPassword: yup
    .string()
    .label('Confirm Password')
    .required()
    .min(2, 'Seems a bit short...'),
});
