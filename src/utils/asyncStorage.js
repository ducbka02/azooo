//import { AsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const SECRET = '@AZOOO:';
export const SEARCH_KEY = 'search_key';

const saveObject = async (key, data) => {
  try {
    await AsyncStorage.setItem(`${SECRET}${key}`, data);
  } catch (error) {
    console.error(error);
  }
};

const readObject = async key => {
  try {
    const value = await AsyncStorage.getItem(`${SECRET}${key}`);
    return value;
  } catch (exception) {
    return null;
  }
};

const removeObject = async key => {
  try {
    await AsyncStorage.removeItem(`${SECRET}${key}`);
  } catch (exception) {

  }
};

export {
  saveObject,
  readObject,
  removeObject,
};
