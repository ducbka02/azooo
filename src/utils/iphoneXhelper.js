import { Dimensions, Platform, PlatformIOSStatic } from 'react-native';

export function isIphoneX() {
  const dimen = Dimensions.get('window');
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 812 ||
      dimen.width === 812 ||
      (dimen.height === 896 || dimen.width === 896))
  );
}

export function isIpad() {
  const dimen = Dimensions.get('window');
  const aspectRatio = dimen.height / dimen.width;

  if (aspectRatio <= 1.6) {
    return true;
  }
  return false;
}

const { height } = Dimensions.get('window');
export const isSmallScreen = height < 569;
