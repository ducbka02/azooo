import request from './request';

export async function getListProductAvailable(lat, long, limit, offset) {
  return request.call({
    url: `/list-product-available?lat=${lat}&lng=${long}&limit=${limit}&offset=${offset}`,
    method: 'GET',
    params: {},
  });
}

export async function getHomeBanner() {
  return request.call({
    url: '/content-banner?position=23&_format=json',
    method: 'GET',
    params: {},
  });
}

export async function getCategoryProduct() {
  return request.call({
    url: '/category?_format=json',
    method: 'GET',
    params: {},
  });
}

export async function getSubCategoryProduct(id) {
  return request.call({
    url: `/category/${id}?_format=json`,
    method: 'GET',
    params: {},
  });
}

export async function getProductInCategory(id) {
  return request.call({
    url: `/list-product/${id}?_format=json`,
    method: 'GET',
    params: {},
  });
}

export async function getListFAQ() {
  return request.call({
    url: '/faq-categories?_format=json',
    method: 'GET',
    params: {},
  });
}

export async function getListFAQById(id) {
  return request.call({
    url: `/faq-content/${id}?_format=json`,
    method: 'GET',
    params: {},
  });
}

export async function getListBestSelling(limit, offset, order) {
  console.log(limit + ' ' + offset + ' ' + order);
  const url = order
    ? `/best-sells?_format=json&limit=${limit}&offset=${offset}&order=${order}`
    : `/best-sells?_format=json&limit=${limit}&offset=${offset}`;
  return request.call({
    url: url,
    method: 'GET',
    params: {},
  });
}

export async function getListNewProduct(limit, offset, order) {
  const url = order
    ? `/new-products?_format=json&limit=${limit}&offset=${offset}&order=${order}`
    : `/new-products?_format=json&limit=${limit}&offset=${offset}`;
  return request.call({
    url: url,
    method: 'GET',
    params: {},
  });
}

export async function getListCollection() {
  return request.call({
    url: '/content-collection?items_per_page=8&_format=json',
    method: 'GET',
    params: {},
  });
}

export async function getListProductInCollection(id) {
  return request.call({
    url: `/collection-products/${id}?_format=json`,
    method: 'GET',
    params: {},
  });
}

export async function getListProductInBrand(id) {
  return request.call({
    url: `/brand-products/${id}?_format=json`,
    method: 'GET',
    params: {},
  });
}

export async function getListProductInSale(id, cancelSame = true) {
  return request.call(
    {
      url: `/promotion-products/${id}?_format=json`,
      method: 'GET',
      params: {},
    },
    cancelSame,
  );
}

export async function getDetailProduct(id) {
  return request.call({
    url: `/detail-product/${id}?_format=json`,
    method: 'GET',
    params: {},
  });
}

export async function getListBrand() {
  return request.call({
    url: '/brand?_format=json',
    method: 'GET',
    params: {},
  });
}

export async function getListPromotion() {
  return request.call({
    url: '/content-promotion?_format=json',
    method: 'GET',
    params: {},
  });
}

export async function getListAgency(id, lat, long) {
  return request.call({
    url: `/agent-distance/${id}?lat=${lat}&lng=${long}&_format=json`,
    method: 'GET',
    params: {},
  });
}

export async function getListKeywordPopular() {
  return request.call({
    url: '/popular-keyword?_format=json',
    method: 'GET',
    params: {},
  });
}

export async function azSearchKeyword(keyword) {
  return request.call({
    url: `/az-search?keyword=${keyword}&_format=json`,
    method: 'GET',
    params: {},
  });
}

export async function getAboutAzooo() {
  return request.call({
    url: '/content-about?_format=json',
    method: 'GET',
    params: {},
  });
}
