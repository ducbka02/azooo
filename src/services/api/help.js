import request from './request';

export async function getRegisterAgency({ name, phone, address, fb_url }) {
    return request.call({
        url: `/azooo?_format=hal_json&name=${name}&type=register_agent&phome=${phone}&address=${address}&fb_url=${fb_url}`,
        method: 'GET',
        params: {

        },
    });
}

export async function getFeedback({ name, phone, message }) {
    return request.call({
        url: `/azooo?_format=hal_json&name=${name}&type=feedback_form&phome=${phone}&message=${message}`,
        method: 'GET',
        params: {

        },
    });
}

export async function getSocialInfo() {
    return request.call({
        url: `/link-social?_format=json`,
        method: 'GET',
        params: {

        },
    });
}