import { default as BaseAxios, AxiosInstance, AxiosRequestConfig, CancelTokenSource } from 'axios';
import get from 'lodash/get';

const DEV_URL = 'http://backend.sandbox.azooo.com.vn/api';
const PRO_URL = 'https://quanly.azooo.vn/api';
const PRO_URL1 = 'https://api.azooo.vn/api';

class Request {
  public axios: AxiosInstance;
  public source: CancelTokenSource;

  constructor() {
    this.axios = BaseAxios.create({
      baseURL: PRO_URL1,
      timeout: 30000,
    });
  }

  public async call(config: AxiosRequestConfig, cancelSameRequest: boolean) {
    try {
      let headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      };
      if (cancelSameRequest === undefined) cancelSameRequest = true;
      if (cancelSameRequest) {
        if (this.source) {
          this.source.cancel("Only one request allowed at a time.");
        }
        this.source = BaseAxios.CancelToken.source();
        config.cancelToken = this.source.token;
      }
      const res = await this.axios.request({ headers, ...config });
      return res.data;
    } catch (error) {
      if (BaseAxios.isCancel) {
        // Request was canceled beacause timeout or other same request call
      }
      if (error.response) {
        const message = get(error, 'response.data.message');
        if (
          message ===
          'The resource owner or authorization server denied the request.'
        ) {
          // if we got this error message means, session is expired, we can logout the user
        }
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        throw { message };
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        throw error.request;
      } else {
        // Something happened in setting up the request that triggered an Error
        throw { message: error.message };
      }
    }
  }
}

export default new Request();
