import * as React from 'react';
import {connect} from 'react-redux';
import {View, ImageBackground, TouchableOpacity, Image} from 'react-native';
import {compose, defaultProps} from 'recompose';
import Text from 'src/components/Text';
import images from 'src/config/images';
import theme from 'src/config/theme';
import NavigationService from 'src/config/NavigationService';
import styles from './styles';

class Header extends React.Component {
  _renderLeftIcon() {
    const {leftIcon, onLeftFunc, back, showLeftButton} = this.props;
    if (!showLeftButton) {
      return <View />;
    }
    if (back) {
      // if we want back button
      const onBackPress = onLeftFunc
        ? onLeftFunc
        : () => {
            NavigationService.goBack();
          };
      return (
        <TouchableOpacity style={styles.leftIcon} onPress={onBackPress}>
          <Image
            resizeMode="contain"
            source={images.ICON_BACK}
            style={{width: '100%', height: '100%'}}
          />
        </TouchableOpacity>
      );
    }
    if (leftIcon) {
      return (
        <TouchableOpacity style={styles.leftIcon} onPress={onLeftFunc}>
          {leftIcon}
        </TouchableOpacity>
      );
    }
    return <View />;
  }

  _renderTitleComponent() {
    const {title, back, titleClick, onLeftFunc} = this.props;
    let titleDom = null;
    if (title) {
      if (typeof title === 'string') {
        titleDom = (
          <Text
            nOfLines={1}
            onPress={() => {
              if (back && titleClick) {
                NavigationService.goBack();
              }
            }}
            style={styles.titleText}>
            {title}
          </Text>
        );
      } else {
        titleDom = title;
      }
      return <View style={styles.title}>{titleDom}</View>;
    }
    return <View style={styles.title} />;
  }

  _renderRightIcon() {
    const {rightIcon, onRightFunc, showRightButton} = this.props;
    if (!showRightButton) {
      return null;
    }
    if (rightIcon) {
      return (
        <TouchableOpacity style={styles.rightIconWrapper} onPress={onRightFunc}>
          {rightIcon}
        </TouchableOpacity>
      );
    }
    return null;
  }

  render() {
    return (
      <ImageBackground source={images.HEADER_IMAGE} style={styles.wrapper}>
        <View style={styles.contentView}>
          {this._renderLeftIcon()}
          {this._renderTitleComponent()}
          {this._renderRightIcon()}
        </View>
      </ImageBackground>
    );
  }
}

export default compose(
  defaultProps({
    back: false,
    showLeftButton: true,
    showRightButton: true,
    navBarColor: theme.formActive,
  }),
  connect(state => {
    const firstName = 'Duc';
    const lastName = 'Nguyen';
    return {
      userInitial: `${firstName}${lastName}`,
    };
  }),
)(Header);
