import { StyleSheet, Platform } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import { isIphoneX } from 'src/utils/iphoneXhelper';
export default StyleSheet.create({
  wrapper: {
    zIndex: 2000,
    width: '100%',
    height: isIphoneX() ? scale(64) : scale(60),
    alignItems: 'flex-end',
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        shadowRadius: 2,
        shadowColor: "rgba(0, 0, 0, 1.0)",
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 2 }
      },
      android: {
        elevation: 5
      }
    })
  },
  contentView: {
    flex: 1,
    alignItems: 'flex-end',
    paddingHorizontal: scale(8),
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingBottom: scale(4),
  },
  leftIcon: {
    width: scale(13),
    height: scale(21.4),
    marginBottom: scale(4),
  },
  rightIconWrapper: {
    marginBottom: scale(4),
  },
  rightIcon: {

  },
  rightIconDefault: {
    color: 'white',
  },
  title: {
    flex: 1,
    justifyContent: 'center',
  },
  titleText: {
    fontSize: scale(12),
    color: 'white',
    marginBottom: scale(6),
    paddingHorizontal: scale(10),
  },
});
