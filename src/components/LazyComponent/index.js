import React from 'react';
import {Platform, View} from 'react-native';
import Spinner from "react-native-spinkit";
import theme from 'src/config/theme';

//To fix lag, android scrollable tabview
class LazyComponent extends React.Component {

  constructor() {
    super();
  }

  render() {
      if (this.props.showLoading) {
        return <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 0}}>
          <Spinner style={{width: 25}} size={25} type="Wave" color={theme.pinkishColor}/>
        </View>
      } 
    return this.props.children;
  }
}

LazyComponent.defaultProps = {
  delay: 200,
  showLoading: false,
  devices: 'android, ios'
}
export default LazyComponent;