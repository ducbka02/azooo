import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import { isIphoneX } from 'src/utils/iphoneXhelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');
export default StyleSheet.create({
  wrapper: {
    zIndex: 1000,
    position: "absolute",
    top: isIphoneX() ? scale(64) : scale(60),
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'white',
    flex: 1,
    paddingHorizontal: scale(10),
  },
  container: {

  },
  wrapperResult: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: scale(5),
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.blackSuperLightColor,
  },
  titleResult: {
    color: theme.blackIconColor,
    fontSize: scale(10),
  },
  chip: {
    backgroundColor: theme.pinkishColor,
    marginRight: scale(6),
    marginBottom: scale(6),
    paddingVertical: scale(5),
    paddingHorizontal: scale(6),
    borderRadius: scale(15)
  },
  textChip: {
    fontSize: scale(9),
    color: 'white'
  },
  chipCloseBtn: {
    borderRadius: 8,
    width: 16,
    height: 16,
    backgroundColor: '#ddd',
    alignItems: 'center',
    justifyContent: 'center'
  },
  chipCloseBtnTxt: {
    color: '#555',
    paddingBottom: 3
  },
  headerCategory: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: scale(10),
    marginTop: scale(10),
  },
  trendText: {
    color: theme.blackIconColor,
    fontSize: scale(12),
    marginLeft: scale(5),
  },
  textChipStyle: {
    backgroundColor: theme.greenSearch
  }
});
