import * as React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  InteractionManager,
  ScrollView,
  Dimensions,
  FlatList
} from 'react-native';
import PropTypes from 'prop-types';
import { compose, withState } from 'recompose';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { scale } from 'src/utils/scallingHelper';
import Text from 'src/components/Text';
import images from 'src/config/images';
import theme from 'src/config/theme';
import styles from './styles';
import { readObject, removeObject, SEARCH_KEY } from 'src/utils/asyncStorage';

const { width } = Dimensions.get('window');

const Chips = (props) => {
  const { value, onPress, textChipStyle } = props;
  return (
    <TouchableOpacity style={[styles.chip, textChipStyle]} onPress={onPress}>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.textChip}>{value}</Text>
      </View>
    </TouchableOpacity>
  )
}

class SearchView extends React.Component {

  constructor(props) {
    super(props);
    this._renderProgram = this._renderProgram.bind(this);
  }

  async componentDidMount() {
    const { setSearchKey } = this.props;
    InteractionManager.runAfterInteractions(async () => {
      const searchHistory = await readObject(SEARCH_KEY);
      setSearchKey(searchHistory);
    });
  }

  _renderProgram(item) {
    const { onClickSearchResult } = this.props;
    const { product_id, title } = item;
    return (
      <TouchableOpacity style={styles.wrapperResult} activeOpacity={1} onPress={() => onClickSearchResult(item)}>
        <View style={{ flex: 0.8, }}>
          <Text nOfLines={1} style={styles.titleResult}>{title}</Text>
        </View>
        <View style={{ flex: 0.2, alignItems: 'flex-end', }}>
          <Image resizeMode='contain' source={images.ICON_RECALL} style={{ tintColor: theme.blackLightColor }} />
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const { show, searchKey, setSearchKey, searchResult, onTextClick } = this.props;

    if (!show) return null;

    if (searchResult.length > 0) {
      return (
        <View style={styles.wrapper}>
          <FlatList
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag"
            showsVerticalScrollIndicator={false}
            style={styles.container}
            data={searchResult}
            renderItem={({ item }) => this._renderProgram(item)}
            extraData={this.props}
            keyExtractor={(item) => item.product_id.toString()}
            removeClippedSubviews={false}
            initialNumToRender={8}
            maxToRenderPerBatch={2}
          />
        </View>
      )
    }

    const chips = this.props.listPopularKeyword.map((item, index) => (
      <Chips
        key={index}
        value={item.keyword}
        textChipStyle={styles.textChipStyle}
        onPress={() => onTextClick(item.keyword)}
      />
    ));

    let chipsHistory = null;
    if (searchKey) {
      chipsHistory = searchKey.split(',').map((item, index) => (
        <Chips
          key={index}
          value={item}
          onPress={() => onTextClick(item)}
        />
      ));
    }

    return (
      <View style={styles.wrapper}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
        >
          {searchKey !== null && (
            <View>
              <View style={[styles.headerCategory, { justifyContent: 'space-between' }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                  <Image source={images.ICON_HISTORY} />
                  <Text style={styles.trendText}>Lịch sử tìm kiếm</Text>
                </View>
                <Text
                  onPress={async () => {
                    await removeObject(SEARCH_KEY);
                    setSearchKey(null);
                  }}
                  style={[styles.trendText, { fontSize: scale(8) }]}
                >
                  Xóa tất cả
              </Text>
              </View>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center' }}>
                {chipsHistory}
              </View>
            </View>
          )}
          <View style={styles.headerCategory}>
            <Image source={images.ICON_TREND} />
            <Text style={styles.trendText}>Được nhiều người quan tâm</Text>
          </View>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center' }}>
            {chips}
          </View>
        </ScrollView>
      </View>
    );
  }
}

SearchView.propTypes = {
  searchResult: PropTypes.arrayOf(PropTypes.any),
  onClickSearchResult: PropTypes.func,
  show: PropTypes.bool,
};

export default compose(
  withState('searchKey', 'setSearchKey', null),
  withState('textSearch', 'setTextSearch', ''),
  connect(
    (state) => ({
      listPopularKeyword: get(state, 'app.listPopularKeyword', []),
    }),
    {

    }
  )
)(SearchView);
