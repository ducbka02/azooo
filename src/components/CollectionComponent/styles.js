import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const DEVICE_WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    width: scale(100),
    height: scale(150),
    backgroundColor: 'white',
    borderRadius: scale(5),
    marginRight: scale(5),
  },
  imageStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: scale(100),
    height: scale(100)
  },
  titleText: {
    fontSize: scale(10),
    color: theme.blackIconColor,
  },
});
