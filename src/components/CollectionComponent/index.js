import * as React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import Text from 'src/components/Text';
import images from 'src/config/images';
import theme from 'src/config/theme';
import ProgressiveImage from 'src/components/ProgressiveImage';
import styles from './styles';
import { scale } from 'src/utils/scallingHelper';

class CollectionComponent extends React.Component {

  constructor(props) {
    super(props);

  }

  render() {
    const {
      title,
      field_image,
      onPress,
      linesOfTilte,
    } = this.props;
    return (
      <TouchableOpacity activeOpacity={1} style={styles.wrapper} onPress={onPress}>
        <View style={styles.imageStyle}>
          <ProgressiveImage
            thumbnailSource={images.DEFAULT}
            style={{ width: scale(100), height: scale(100), }}
            source={field_image.length === 0 ? images.DEFAULT : { uri: field_image }}
          />
        </View>
        <View style={{ flex: 1, margin: 5 }}>
          <View style={{ flex: 0.7 }}>
            <Text nOfLines={linesOfTilte} style={styles.titleText}>{title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

CollectionComponent.propTypes = {
  title: PropTypes.string,
  field_image: PropTypes.string,
  onPress: PropTypes.func,
  linesOfTilte: PropTypes.number,
};

CollectionComponent.defaultProps = {
  title: "",
  field_image: "",
  onPress: null,
  linesOfTilte: 3
};

export default CollectionComponent;
