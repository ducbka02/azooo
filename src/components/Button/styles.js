import { StyleSheet } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';

export default StyleSheet.create({
  container: {
    //height: scale(50),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(5),
    backgroundColor: 'transparent',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.pinkishColor,
    borderRadius: 10,
  },
  title: {
    //paddingHorizontal: scale(10),
    fontSize: scale(10),
    color: theme.pinkishColor
  },
  rightIcon: {
    paddingLeft: scale(10),
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: scale(10),
    width: scale(10),
    marginLeft: scale(3),
    marginRight: scale(5),
  },
  titleContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
