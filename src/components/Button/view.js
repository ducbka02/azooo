import * as React from 'react';
import { View, TouchableOpacity, ActivityIndicator } from 'react-native';
import Text from 'src/components/Text';
import styles from './styles';

class Button extends React.Component {
  renderLeftIcon() {
    const { leftIcon, iconLeftStyle } = this.props;
    if (!leftIcon) {
      return <View style={[styles.iconContainer, { width: 1 }]} />;
    }
    return <View style={[styles.iconContainer, iconLeftStyle]}>{leftIcon}</View>;
  }

  renderTitle() {
    const { title, leftIcon, titleStyle } = this.props;
    
    if (!title) {
      return null;
    }
    if (typeof title === 'string') {
      return (
        <View style={styles.titleContainer}>
          <Text style={[styles.title, titleStyle]}>
            {title}
          </Text>
        </View>
      );
    }
    
    return title;
  }

  renderRightIcon() {
    const { rightIcon, loading, textColor } = this.props;
    if (loading) {
      return (
        <View style={[styles.iconContainer, styles.icon]}>
          <ActivityIndicator color={'white'} />
        </View>
      );
    }
    if (!rightIcon) {
      return <View style={[styles.iconContainer, { width: 1 }]} />;
    }
    return <View style={[styles.iconContainer, styles.icon]}>{rightIcon}</View>;
  }

  render() {
    const { onPress, disabled, loading, styleContainer } = this.props;
    return (
        <TouchableOpacity
          style={[styles.container, styleContainer]}
          onPress={onPress}
          disabled={disabled || loading}
        >
          {this.renderLeftIcon()}
          {this.renderTitle()}
          {this.renderRightIcon()}
        </TouchableOpacity>
    );
  }
}

export default Button;
