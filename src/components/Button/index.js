import { compose, defaultProps } from 'recompose';
import View from './view';

const Enhanser = compose(
  defaultProps({
    title: null,
    leftIcon: null,
    rightIcon: null,
    color: 'transparent',
    textColor: 'black',
    bordered: false,
    disabled: false,
    fullWidth: false,
    onPress: () => {},
  })
)(View);

export default Enhanser;
