/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-unused-vars */
import * as React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Text from 'src/components/Text';
import images from 'src/config/images';
import ProgressiveImage from 'src/components/ProgressiveImage';
import styles from './styles';
import {scale} from 'src/utils/scallingHelper';

class ProductComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      inSale,
      inMap,
      title,
      field_image,
      category_name,
      price__number,
      promotion_price,
      field_thumbnail,
      onPress,
      linesOfTilte,
      imageStyleProps,
    } = this.props;

    if (inMap) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          style={styles.wrapperInMap}
          onPress={onPress}>
          <View style={styles.textMapView}>
            <View style={{}}>
              <Text nOfLines={linesOfTilte} style={styles.titleText}>
                {title}
              </Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text nOfLines={1} style={styles.priceText}>
                {price__number}
              </Text>
            </View>
          </View>
          <View style={styles.imageMapStyle}>
            <ProgressiveImage
              thumbnailSource={images.DEFAULT}
              style={styles.imageInViewStyle}
              source={
                field_image.length === 0 ? images.DEFAULT : {uri: field_image}
              }
            />
          </View>
        </TouchableOpacity>
      );
    }

    if (inSale) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          style={styles.wrapperInSale}
          onPress={onPress}>
          <View style={styles.imageInSaleStyle}>
            <ProgressiveImage
              thumbnailSource={images.DEFAULT}
              style={{width: scale(150), height: scale(97.5)}}
              source={
                field_image.length === 0 ? images.DEFAULT : {uri: field_image}
              }
            />
          </View>
          <View style={{flex: 1, paddingHorizontal: 5}}>
            <View style={{flex: 0.6}}>
              <Text
                nOfLines={linesOfTilte}
                style={[styles.titleText, {textAlign: 'left'}]}>
                {title}
              </Text>
            </View>
            <View style={{flex: 0.4}}>
              <Text
                nOfLines={1}
                style={[styles.priceText, {textAlign: 'left'}]}>
                {price__number}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    }

    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.wrapper}
        onPress={onPress}>
        <View style={[styles.imageStyle, imageStyleProps]}>
          <ProgressiveImage
            thumbnailSource={images.DEFAULT}
            style={{width: scale(100), height: scale(65)}}
            source={
              field_image.length === 0 ? images.DEFAULT : {uri: field_image}
            }
          />
        </View>
        <View style={{flex: 1, marginTop: 5, paddingHorizontal: 5}}>
          <View style={{flex: 0.7}}>
            <Text nOfLines={linesOfTilte} style={styles.titleText}>
              {title}
            </Text>
          </View>
          <View style={{flex: 0.3}}>
            <Text nOfLines={1} style={styles.priceText}>
              {price__number}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

ProductComponent.propTypes = {
  inSale: PropTypes.bool,
  inMap: PropTypes.bool,
  title: PropTypes.string,
  field_image: PropTypes.string,
  category_name: PropTypes.string,
  price__number: PropTypes.string,
  promotion_price: PropTypes.string,
  field_thumbnail: PropTypes.string,
  onPress: PropTypes.func,
  imageStyleProps: PropTypes.object,
  linesOfTilte: PropTypes.number,
};

ProductComponent.defaultProps = {
  inSale: false,
  inMap: false,
  title: '',
  field_image: '',
  category_name: '',
  price__number: '',
  promotion_price: '',
  field_thumbnail: '',
  onPress: null,
  imageStyleProps: {},
  linesOfTilte: 3,
};

export default ProductComponent;
