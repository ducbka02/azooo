import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';

const DEVICE_WIDTH = Dimensions.get('window').width;
const IMAGE_WIDTH = DEVICE_WIDTH * 0.85 * 0.9 * 0.5;

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    width: scale(100),
    height: scale(150),
    backgroundColor: 'white',
    borderRadius: scale(5),
    marginRight: scale(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapperInSale: {
    flex: 1,
    width: scale(200),
    height: scale(150),
    backgroundColor: 'white',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.blackSuperLightColor,
    marginRight: scale(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapperInMap: {
    flexDirection: 'row',
    width: "95%",
    height: "95%",
    borderRadius: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  imageStyle: {
    width: scale(100),
    height: scale(90),
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageInSaleStyle: {
    width: scale(120),
    height: scale(100),
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    fontSize: scale(9),
    color: theme.blackIconColor,
    textAlign: 'center'
  },
  priceText: {
    fontSize: scale(9),
    color: theme.green,
    textAlign: 'center',
  },
  burnImage: {
    width: scale(90),
  },
  dabanText: {
    position: 'absolute',
    color: 'white',
    fontSize: scale(8),
    top: scale(15),
    right: scale(20)
  },
  imageMapStyle: {
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textMapView: {
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    paddingHorizontal: scale(5),
  },
  imageInViewStyle: {
    width: IMAGE_WIDTH,
    height: IMAGE_WIDTH * 325 / 500,
  }
});
