import * as React from 'react';
import { Text as BaseText } from 'react-native';
import styles from './styles';

class Text extends React.Component {
  render() {
    const { nOfLines, style, ...rest } = this.props;
    return (
      <BaseText
        numberOfLines={nOfLines}
        allowFontScaling={false}
        style={[styles.text, style]}
        {...rest}
      />
    );
  }
}

export default Text;
