import { StyleSheet } from 'react-native';
import { scale } from 'src/utils/scallingHelper';
import fonts from 'src/config/fonts';

export default StyleSheet.create({
  text: {
    fontFamily: fonts.ROBOTO_REGULAR,
    fontSize: scale(12),
  },
});