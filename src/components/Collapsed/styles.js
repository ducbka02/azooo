import { StyleSheet, Platform } from 'react-native';
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';

export default StyleSheet.create({
  container: {
    overflow: 'hidden',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingTop: scale(10),
  },
  title: {
    flex: 1,
    color: 'white',
    fontSize: scale(10),
    flexWrap: 'wrap',
    marginRight: scale(5),
  },
  subContentView: {
    marginLeft: scale(10)
  },
  itemText: {
    color: 'white',
    fontSize: scale(10),
    paddingTop: scale(5),
  }
});
