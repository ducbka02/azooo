import React, { Component } from 'react';
import { View, TouchableOpacity, Animated, Image } from 'react-native';
import Text from 'src/components/Text';
import images from 'src/config/images';
import styles from './styles';
class collapsed extends Component {
  constructor(props) {
    super(props);
  }

  toggle = () => {
    this.props.onItemClick();
  };

  render() {
    const { title, expand, onSubItemClick } = this.props;

    const menu = this.props.description || [];

    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.toggle}>
          <View style={styles.titleContainer}>
            <Text numberOfLines={2} style={styles.title}>
              {title}
            </Text>
            <Image
              style={{
                tintColor: 'white'
              }}
              source={expand ? images.UP : images.DOWN}
            />
          </View>
        </TouchableOpacity>
        {expand && (
          <View style={styles.subContentView}>
            {menu.map(({ category_id, category_name }, index) => {
              return (
                <TouchableOpacity key={index} onPress={() => onSubItemClick(category_id)}>
                  <Text style={styles.itemText}>{category_name}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
        )}
      </View>
    );
  }
}

export default collapsed;
