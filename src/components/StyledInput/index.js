import { compose, withState } from 'recompose';
import View from './view';

export default compose(
  withState('focus', 'setFocus', false)
)(View);
