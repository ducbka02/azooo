import { StyleSheet } from 'react-native';
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
import fonts from 'src/config/fonts';

export default StyleSheet.create({
  wrapper: {
    width: '100%',
    borderBottomWidth: 1,
    paddingTop: scale(5),
    alignSelf: 'center',
    paddingHorizontal: scale(30),
  },
  titleText: {
    marginBottom: scale(5),
    fontSize: scale(10),
    color: theme.blackLightColor
  },
  viewInputWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  inputStyleDefault: {
    color: theme.blackIconColor,
    width: '90%',
    fontSize: scale(12),
    paddingBottom: scale(5),
  },
  errorText: {
    color: theme.pinkishColor,
    fontSize: scale(10),
    marginVertical: scale(5),
  },
  rightIconButton: {
    paddingVertical: scale(5),
    paddingHorizontal: scale(10),
    bottom: scale(10),
    right: scale(-5),
  },
});
