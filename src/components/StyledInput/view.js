import * as React from 'react';
import {View, TextInput, Image, TouchableOpacity} from 'react-native';
import theme from 'src/config/theme';
import Text from 'src/components/Text';
import styles from './styles';

class StyledInput extends React.Component {
  static defaultProps = {
    inputStyle: {},
    indexFocus: 0,
  };

  render() {
    const {
      label,
      stateFocus,
      indexFocus,
      onFocusProp,
      formikProps,
      formikKey,
      inputStyle,
      rightIcon,
      onPressIcon,
      setFocus,
      focus,
      ...rest
    } = this.props;

    let colorState = theme.blackSuperLightColor;
    if (focus) {
      if (formikProps.touched[formikKey] && formikProps.errors[formikKey]) {
        // colorState = theme.formError;
      } else {
        colorState = theme.pinkishColor;
      }
    }
    let opacityParents = 1;
    let withParents = '100%';
    let borderBottomWidthParents = 1;
    let paddingHorizontalParents = 0;

    if (stateFocus !== -1 && stateFocus !== undefined) {
      if (stateFocus !== indexFocus) {
        opacityParents = 0.2;
      } else {
        //withParents = '90%';
        //borderBottomWidthParents = 5;
        //paddingHorizontalParents = 10;
      }
    }

    return (
      <View>
        <View
          style={[
            styles.wrapper,
            {
              borderColor: colorState,
              opacity: opacityParents,
              width: withParents,
              borderBottomWidth: borderBottomWidthParents,
              paddingHorizontal: paddingHorizontalParents,
            },
          ]}>
          <Text style={styles.titleText}>{label}</Text>
          <View style={styles.viewInputWrapper}>
            <TextInput
              style={[styles.inputStyleDefault, inputStyle]}
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={value => {
                formikProps.setFieldTouched(formikKey);
                formikProps.setFieldValue(formikKey, value);
              }}
              onBlur={() => {
                setFocus(false);
                formikProps.handleBlur(formikKey);
                if (onFocusProp) {
                  onFocusProp(-1);
                }
              }}
              onFocus={() => {
                setFocus(true);
                if (onFocusProp) {
                  onFocusProp(indexFocus);
                }
              }}
              placeholderTextColor="rgba(255, 255, 255, 0.6)"
              underlineColorAndroid="transparent"
              value={formikProps.values[formikKey]}
              {...rest}
            />
            {!!rightIcon && stateFocus === -1 && (
              <TouchableOpacity
                style={styles.rightIconButton}
                onPress={onPressIcon}>
                <Image source={rightIcon} style={{tintColor: colorState}} />
              </TouchableOpacity>
            )}
          </View>
        </View>
        {formikProps.touched[formikKey] && formikProps.errors[formikKey] && (
          <Text style={styles.errorText}>{formikProps.errors[formikKey]}</Text>
        )}
      </View>
    );
  }
}

export default StyledInput;
