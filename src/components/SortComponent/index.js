/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {compose, withState} from 'recompose';
import Text from 'src/components/Text';
import images from 'src/config/images';
import styles from './styles';
import theme from 'src/config/theme';

class SortComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      sortIndex,
      setSortIndex,
      sortASC,
      setSortASC,
      onPopularClick,
      onBestSellingClick,
      onNewestClick,
      onAmountClick,
    } = this.props;
    return (
      <View style={styles.wrapperSortView}>
        {/* <TouchableOpacity
          onPress={() => {
            setSortIndex(0);
            onPopularClick();
          }}
          style={[styles.sortItemView, { flex: 0.2, borderBottomWidth: sortIndex === 0 ? 1 : 0 }]}>
          <Text style={styles.sortText}>Phổ biến</Text>
          <View style={styles.verticalLine} />
        </TouchableOpacity> */}
        <TouchableOpacity
          onPress={() => {
            setSortIndex(1);
            onBestSellingClick();
          }}
          style={[
            styles.sortItemView,
            {flex: 0.3, borderBottomWidth: sortIndex === 1 ? 1 : 0},
          ]}>
          <Text style={styles.sortText}>Bán chạy</Text>
          <View style={styles.verticalLine} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setSortIndex(2);
            onNewestClick();
          }}
          style={[
            styles.sortItemView,
            {flex: 0.3, borderBottomWidth: sortIndex === 2 ? 1 : 0},
          ]}>
          <Text style={styles.sortText}>Mới nhất</Text>
          <View style={styles.verticalLine} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            onAmountClick(sortASC !== 1);
            setSortASC(sortASC !== 1 ? 1 : 0);
          }}
          style={[styles.sortItemView, {flex: 0.4}]}>
          <Text style={styles.sortText}>Giá sản phẩm</Text>
          <View style={styles.sortButtonView}>
            <Image
              style={{
                tintColor:
                  sortASC === 1 ? theme.pinkishColor : theme.blackIconColor,
              }}
              source={images.SORT_UP}
            />
            <Image
              style={{
                tintColor:
                  sortASC === 0 ? theme.pinkishColor : theme.blackIconColor,
              }}
              source={images.SORT_DOWN}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default compose(
  withState('sortIndex', 'setSortIndex', -1),
  withState('sortASC', 'setSortASC', -1),
)(SortComponent);
