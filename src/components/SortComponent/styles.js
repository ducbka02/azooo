import { StyleSheet, Platform } from 'react-native';
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';

export default StyleSheet.create({
  wrapperSortView: {
    flexDirection: 'row',
    height: scale(35),
    width: '100%',
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        shadowRadius: 2,
        shadowColor: "rgba(0, 0, 0, 1.0)",
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 2 }
      },
      android: {
        elevation: 5
      }
    }),
    //justifyContent: 'space-between',
    alignItems: 'center'
  },
  sortItemView: {
    height: scale(35),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: scale(8),
    borderBottomColor: theme.pinkishColor
  },
  sortText: {
    fontSize: scale(9),
    color: theme.blackIconColor,
  },
  verticalLine: {
    height: scale(14),
    width: StyleSheet.hairlineWidth,
    backgroundColor: theme.blackSuperLightColor,
    position: 'absolute',
    right: 0
  },
  sortButtonView: {
    height: scale(12),
    justifyContent: 'space-between',
    paddingLeft: scale(3),
  }
});
