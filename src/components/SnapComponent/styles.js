import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');
export default StyleSheet.create({
  wrapper: {
    width: width,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red'
  },
  wrapperItemStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.7,
    height: '100%',
    backgroundColor: 'red'
  },
  slider: {
    
  },
  sliderContentContainer: {
    
  },
});
