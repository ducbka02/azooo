import * as React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { scale } from 'src/utils/scallingHelper';
import Carousel from 'react-native-snap-carousel';
import Text from 'src/components/Text';
import images from 'src/config/images';
import theme from 'src/config/theme';
import styles from './styles';

const { width } = Dimensions.get('window');

class SnapComponent extends React.Component {

  constructor(props) {
    super(props);
    this.renderItemWithParallax = this.renderItemWithParallax.bind(this);
  }

  renderItemWithParallax({ item, index }) {
    return (
      <TouchableOpacity style={styles.wrapperItemStyle} activeOpacity={1}>
        <Image source={{ uri: item.field_image }} style={{ width: '100%', height: '100%' }} />
      </TouchableOpacity>
    );
  }

  render() {
    const { brands, renderItemWithParallax } = this.props;
    return (
      <View style={{ width: '100%' }}>
        <Carousel
          data={brands}
          renderItem={renderItemWithParallax}
          sliderWidth={width}
          itemWidth={scale(width * 0.7)}
          hasParallaxImages={true}
          firstItem={0}
          inactiveSlideScale={0.8}
          inactiveSlideOpacity={0.9}
          containerCustomStyle={styles.slider}
          contentContainerCustomStyle={styles.sliderContentContainer}
          loop={false}
          autoplay={false}
          onSnapToItem={(index) => {

          }}
        />
      </View>
    )
  }
}

export default SnapComponent;
