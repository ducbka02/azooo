import { StyleSheet, Platform } from 'react-native';
import theme from 'src/config/theme';

export default StyleSheet.create({
  container: {
    overflow: 'hidden',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingVertical: 10,
  },
  title: {
    flex: 1,
    color: theme.blackIconColor,
    fontWeight: 'normal',
    flexWrap: 'wrap',
  },
  moreText: {
    color: theme.blackIconColor,
    fontWeight: 'normal',
    fontSize: 12,
    paddingBottom: 30,
  },
  button: { flexDirection: 'row' },
});
