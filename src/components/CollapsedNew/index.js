import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import Text from 'src/components/Text';
import HTMLComponent from 'src/components/HTMLComponent';
import images from 'src/config/images';
class CollapsedNew extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
    };
  }

  toggle = () => {
    this.setState({
      expanded: !this.state.expanded,
    });
  };

  render() {
    if (!this.props.description || this.props.description === 'NULL') {
      return null;
    }
    return (
      <View
        style={[
          styles.container,
          {
            borderBottomWidth: this.props.borderBottomWidth,
          },
        ]}
      >
        <TouchableOpacity onPress={this.toggle}>
          <View style={styles.titleContainer}>
            <Text numberOfLines={5} style={styles.title}>
              {this.props.title}
            </Text>
            <Image
              style={{
                marginRight: 16,
              }}
              source={this.state.expanded ? images.UP : images.DOWN}
            />
          </View>
        </TouchableOpacity>
        {this.state.expanded && (
          <View>
            {/* <Text style={styles.moreText}>{this.props.description}</Text> */}
            <HTMLComponent html={this.props.description} />
          </View>
        )}
      </View>
    );
  }
}

export default CollapsedNew;
