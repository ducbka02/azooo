import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  placeholderView: {
    width: '100%',
    height: '100%',
    padding: scale(2),
    borderRadius: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  placeholderContent: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: theme.placeholderColor,
    alignItems: 'center',
    padding: scale(5)
  },
  linePlaceholderStyle: {
    backgroundColor: theme.blackSuperLightColor,
    alignSelf: 'center',
  }
});
