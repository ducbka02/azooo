import * as React from 'react';
import {
  View,
  Image,
} from 'react-native';
import images from 'src/config/images';
import theme from 'src/config/theme';
import styles from './styles';
import { scale } from 'src/utils/scallingHelper';
import Placeholder, { Line } from 'src/components/PlaceholderAnimation';

class ProductPlaceholder extends React.Component {
  render() {
    const {

    } = this.props;
    return (
      <Placeholder
        animation="fade"
        isReady={false}
        whenReadyRender={() => null}
      >
        <View style={styles.placeholderView}>
          <View style={styles.placeholderContent}>
            <Image
              resizeMode='contain'
              source={images.IMG_PLACEHOLDER}
              style={{
                tintColor: theme.blackSuperLightColor,
              }}
            />
            <Line width="80%" style={[styles.linePlaceholderStyle, { marginTop: scale(15) }]} />
          </View>
        </View>
      </Placeholder>
    )
  }
}
export default ProductPlaceholder;
