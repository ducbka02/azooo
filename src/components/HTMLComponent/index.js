import * as React from 'react';
import {
  Dimensions,
  View
} from 'react-native';
import HTML from 'react-native-render-html';
import Text from 'src/components/Text';
import styles from './styles';
const { width } = Dimensions.get('window');
class HTMLComponent extends React.Component {

  constructor(props) {
    super(props);

  }

  render() {
    const { html } = this.props;

    if (html === "") {
      return (
        <View style={styles.updateView}>
          <Text>Chưa có thông tin cho sản phẩm này</Text>
        </View>
      )
    }
    if (html) {
      let xxx = html.trim().replace(/\n\n/g, '');
      xxx = xxx.replace(/<p>&nbsp;<\/p>/g, '');
      return (
        <View>
          <HTML html={xxx} imagesMaxWidth={Dimensions.get('window').width * 0.8} />
        </View>
      )
    }
    return (
      <View style={styles.updateView}>
        <Text>Thông tin đang được cập nhật</Text>
      </View>
    )
  }
}

export default HTMLComponent;
