import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';

export default StyleSheet.create({
  noInfoText: {
    color: theme.blackIconColor,
    fontSize: scale(10),
    lineHeight: scale(16),
    paddingVertical: scale(5),
  },
  updateView: {
    paddingVertical: scale(10)
  }
});
