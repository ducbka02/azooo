import * as React from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import { compose, defaultProps } from 'recompose';
import Text from 'src/components/Text';
import images from 'src/config/images';
import { scale } from 'src/utils/scallingHelper';
import styles from './styles';

class SearchHeader extends React.Component {

  constructor(props) {
    super(props);

  }

  _renderRightIcon() {
    const {
      rightIcon,
      onRightFunc,
    } = this.props;
    if (rightIcon) {
      return (
        <TouchableOpacity style={styles.rightIconWrapper} onPress={onRightFunc}>
          {rightIcon}
        </TouchableOpacity>
      );
    }
    return null;
  }

  render() {
    const { onFocus, rightIcon } = this.props;
    if (rightIcon) {
      return (
        <ImageBackground source={images.HEADER_IMAGE} style={[styles.wrapperSearchView, {
          paddingHorizontal: scale(8),
          justifyContent: 'space-between',
        }]}>
          <TouchableOpacity style={styles.searchView} activeOpacity={1} onPress={onFocus}>
            <View style={styles.wrapperInput}>
              <Image source={images.ICON_SEARCH} style={styles.iconSearch} resizeMode='contain' />
              <View style={styles.wrapperTextInput}>
                <Text style={styles.placeHolder}>Bạn cần tìm gì?</Text>
              </View>
            </View>
          </TouchableOpacity>
          {this._renderRightIcon()}
        </ImageBackground>
      )
    }
    return (
      <ImageBackground source={images.HEADER_IMAGE} style={styles.wrapperSearchView}>
        <TouchableOpacity style={styles.searchView} activeOpacity={1} onPress={onFocus}>
          <View style={styles.wrapperInput}>
            <Image source={images.ICON_SEARCH} style={styles.iconSearch} resizeMode='contain' />
            <View style={styles.wrapperTextInput}>
              <Text style={styles.placeHolder}>Bạn cần tìm gì?</Text>
            </View>
          </View>
        </TouchableOpacity>
      </ImageBackground>
    )
  }
}

export default compose(
  connect((state) => {
    const firstName = 'Duc';
    const lastName = 'Nguyen';
    return {
      userInitial: `${firstName}${lastName}`,
    };
  })
)(SearchHeader);
