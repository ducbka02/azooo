import { StyleSheet, Platform } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import { isIphoneX } from 'src/utils/iphoneXhelper';
import theme from 'src/config/theme';

export default StyleSheet.create({
  wrapperSearchView: {
    width: '100%',
    height: isIphoneX() ? scale(64) : scale(60),
    alignItems: 'flex-end',
    flexDirection: 'row',
    backgroundColor: 'transparent',
  },
  searchView: {
    flex: 1,
    alignItems: 'flex-end',
    paddingHorizontal: scale(8),
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingBottom: scale(4),
  },
  wrapperInput: {
    flex: 1,
    backgroundColor: 'white',
    width: '100%',
    height: scale(30),
    borderRadius: scale(2),
    flexDirection: 'row',
    paddingHorizontal: scale(5),
    alignItems: 'center'
  },
  iconSearch: {
    width: scale(14)
  },
  wrapperTextInput: {
    marginHorizontal: scale(5),
    width: '100%'
  },
  placeHolder: {
    color: theme.blackSuperLightColor,
    fontSize: scale(10),
    opacity: 0.8
  },
  rightIconWrapper: {
    marginBottom: scale(10),
  },
});
