import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    rightIcon: {
        width: scale(15),
        height: scale(20),
    },
    contentView: {
        flex: 1,
    },
    faqCategory: {
        paddingTop: scale(15),
        paddingBottom: scale(5),
        width: '100%',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: theme.blackIconColor,
    },
    faqView: {
        width: width / 3,
        paddingHorizontal: scale(5),
        flex: 1,
        alignItems: 'center',

    },
    faqImage: {
        width: scale(38),
        height: scale(38),
    },
    faqText: {
        color: theme.blackIconColor,
        fontSize: scale(10),
        textAlign: 'center',
        lineHeight: scale(16),
        marginTop: scale(3),
    },
    page: {
        paddingVertical: scale(10),
        flex: 1
    },
    titleCongrats: {
        marginLeft: scale(10),
        color: theme.pinkishColor,
        fontSize: scale(12),
        lineHeight: scale(16)
    },
    programItem: {
        //flexDirection: 'row',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: theme.blackSuperLightColor,
        marginHorizontal: scale(10),
        paddingBottom: scale(5),
        paddingTop: scale(10),
        //justifyContent: 'space-between',
    },
    textItemProgram: {
        color: theme.blackIconColor,
        fontSize: scale(12),
        lineHeight: scale(16),
        marginRight: scale(10),
    }
});
