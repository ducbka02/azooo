import React from 'react';
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {compose, withState} from 'recompose';
import {connect} from 'react-redux';
import get from 'lodash/get';
import SvgRenderer from 'react-native-svg-renderer';
import Header from 'src/components/Header';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import CollapsedNew from 'src/components/CollapsedNew';
import images from 'src/config/images';
import styles from './styles';

class HelpCenterScreen extends React.Component {
  constructor(props) {
    super(props);

    this._renderProgram = this._renderProgram.bind(this);
  }

  _renderProgram(item) {
    const {title, body, nid} = item;
    return (
      // <TouchableOpacity style={styles.programItem} onPress={() => {
      //   this.props.navigation.navigate('DetailHelpScreen', { title, body })
      // }}>
      //   <Text nOfLines={1} style={styles.textItemProgram}>{title}</Text>
      //   <Image source={images.FORWARD} style={{ width: 10, height: 20 }} />
      // </TouchableOpacity>
      <View style={styles.programItem}>
        <CollapsedNew title={title} description={body} />
      </View>
    );
  }

  render() {
    const {listFAQ, listSubFAQ, faqIndex, setFAQIndex} = this.props;
    console.log(listSubFAQ);
    const collapsedItems = listSubFAQ[listFAQ[faqIndex].tid];
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header
          back
          titleClick
          title="Trung tâm trợ giúp"
          rightIcon={
            <Image source={images.NOTIFICATION} style={styles.rightIcon} />
          }
          onRightFunc={() =>
            this.props.navigation.navigate('NotificationScreen')
          }
        />
        <View style={styles.contentView}>
          <View style={styles.faqCategory}>
            <ScrollView horizontal={true}>
              {listFAQ.map((faq, index) => {
                const {name, field_icon, tid} = faq;
                return (
                  <TouchableOpacity
                    style={styles.faqView}
                    key={tid}
                    onPress={() => {
                      setFAQIndex(index);
                    }}>
                    <Image
                      source={{uri: field_icon}}
                      style={styles.faqImage}
                      resizeMode="contain"
                    />
                    <Text style={styles.faqText}>{name}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
          <View style={styles.page}>
            <Text style={styles.titleCongrats}>CÂU HỎI THƯỜNG GẶP</Text>
            <FlatList
              keyboardShouldPersistTaps="always"
              keyboardDismissMode="on-drag"
              data={collapsedItems}
              renderItem={({item}) => this._renderProgram(item)}
              extraData={this.props}
              keyExtractor={(item, index) => index.toString()}
              removeClippedSubviews={false}
              initialNumToRender={8}
              maxToRenderPerBatch={2}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default compose(
  withState('faqIndex', 'setFAQIndex', 0),
  connect(
    state => ({
      listFAQ: get(state, 'app.listFAQ'),
      listSubFAQ: get(state, 'app.listSubFAQ'),
    }),
    {},
  ),
)(HelpCenterScreen);
