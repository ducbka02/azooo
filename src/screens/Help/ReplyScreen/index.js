/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import get from 'lodash/get';
import {View, Image} from 'react-native';
import {compose, withState} from 'recompose';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {validationSchemaProfile} from 'src/utils/validate';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Header from 'src/components/Header';
import StyledInput from 'src/components/StyledInput';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import images from 'src/config/images';
import styles from './styles';
import NavigationService from 'src/config/NavigationService';

import * as helpActions from 'src/store/actions/help';

class ReplyScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      indexFocus,
      setIndexFocus,
      feedbackProcess,
      sendFeedback,
    } = this.props;
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header
          back
          titleClick
          title="Phản hồi với chúng tôi"
          rightIcon={
            <Image source={images.NOTIFICATION} style={styles.rightIcon} />
          }
          onRightFunc={() =>
            this.props.navigation.navigate('NotificationScreen')
          }
        />
        <View style={styles.contentView}>
          <View style={styles.formView}>
            <Formik
              initialValues={{
                name: '',
                phone: '',
                mess: '',
              }}
              enableReinitialize
              onSubmit={(values, actions) => {
                sendFeedback(
                  {
                    name: values.name,
                    phone: values.phone,
                    message: values.mess,
                  },
                  () => {
                    NavigationService.navigate('SuccessScreen');
                  },
                );
              }}
              validationSchema={Yup.object().shape({
                name: Yup.string().required('Vui lòng nhập thông tin'),
                mess: Yup.string().required('Vui lòng nhập thông tin'),
                phone: Yup.string().required('Vui lòng nhập thông tin'),
              })}>
              {formikProps => {
                return (
                  <React.Fragment>
                    <KeyboardAwareScrollView>
                      <View style={{flex: 1, alignItems: 'center'}}>
                        <View style={styles.wrapperImage}>
                          <Image
                            resizeMode="contain"
                            source={images.PHANHOI}
                            style={styles.imagePresent}
                          />
                        </View>
                        <View style={styles.descriptionText}>
                          <Text style={styles.mainText}>
                            Phản hồi với chúng tôi về chất lượng cũng như hỗ trợ
                            của các đại lý để nâng cao chất lượng của Azooo
                          </Text>
                          <Text style={styles.mainText}>
                            Hotline: Gọi 19001169 để được hỗ trợ 24/7
                          </Text>
                          <Text style={styles.mainText}>
                            Email: hotro@azooo.vn
                          </Text>
                        </View>
                        <View style={styles.formField}>
                          <StyledInput
                            indexFocus={0}
                            stateFocus={indexFocus}
                            label="Tên của bạn"
                            formikProps={formikProps}
                            formikKey="name"
                            onFocusProp={index => setIndexFocus(index)}
                            onPressIcon={() => setIndexFocus(0)}
                          />
                        </View>
                        <View style={styles.formField}>
                          <StyledInput
                            indexFocus={1}
                            stateFocus={indexFocus}
                            label="Số điện thoại"
                            formikProps={formikProps}
                            formikKey="phone"
                            rightIcon={images.ICON_EDIT}
                            onFocusProp={index => setIndexFocus(index)}
                            onPressIcon={() => setIndexFocus(1)}
                          />
                        </View>
                        <View style={styles.formField}>
                          <StyledInput
                            indexFocus={2}
                            stateFocus={indexFocus}
                            label="Nội dung phản ánh"
                            formikProps={formikProps}
                            formikKey="mess"
                            rightIcon={images.ICON_EDIT}
                            onFocusProp={index => setIndexFocus(index)}
                            onPressIcon={() => setIndexFocus(2)}
                          />
                        </View>
                      </View>
                      <View style={styles.buttonWrapper}>
                        <Button
                          title={'GỬI PHẢN HỒI'}
                          loading={feedbackProcess}
                          titleStyle={styles.buttonTextStyle}
                          styleContainer={styles.buttonContainer}
                          onPress={formikProps.handleSubmit}
                        />
                        <Text style={styles.hotlineText}>
                          Gọi hotline: 1900 1169
                        </Text>
                      </View>
                    </KeyboardAwareScrollView>
                  </React.Fragment>
                );
              }}
            </Formik>
          </View>
        </View>
      </View>
    );
  }
}

export default compose(
  withState('indexFocus', 'setIndexFocus', -1),
  connect(
    state => ({
      feedbackProcess: get(state, 'help.helpLoading', false),
    }),
    {sendFeedback: helpActions.sendFeedback},
  ),
)(ReplyScreen);
