import React from 'react';
import {View, Image} from 'react-native';
import {compose, withState} from 'recompose';
import {connect} from 'react-redux';
import get from 'lodash/get';
import {Formik} from 'formik';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Header from 'src/components/Header';
import StyledInput from 'src/components/StyledInput';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import images from 'src/config/images';
import styles from './styles';
import NavigationService from 'src/config/NavigationService';
import * as Yup from 'yup';

import * as helpActions from 'src/store/actions/help';

class RegisterAgencyScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      indexFocus,
      setIndexFocus,
      registerAgency,
      registerAgencyProcess,
    } = this.props;
    console.log(registerAgencyProcess);
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header
          titleClick
          back
          title="Đăng ký làm đại lý"
          rightIcon={
            <Image source={images.NOTIFICATION} style={styles.rightIcon} />
          }
          onRightFunc={() =>
            this.props.navigation.navigate('NotificationScreen')
          }
        />
        <View style={styles.contentView}>
          <View style={styles.formView}>
            <Formik
              initialValues={{
                nameShop: '',
                address: '',
                phone: '',
                fb: '',
              }}
              enableReinitialize
              onSubmit={(values, actions) => {
                // setTimeout(() => {
                //   actions.setSubmitting(false);
                // }, 1000);
                registerAgency(
                  {
                    name: values.nameShop,
                    phone: values.phone,
                    address: values.address,
                    //fb_url: newValues.fb
                  },
                  () => {
                    NavigationService.navigate('SuccessScreen');
                  },
                );
              }}
              validationSchema={Yup.object().shape({
                nameShop: Yup.string().required('Vui lòng nhập thông tin'),
                address: Yup.string().required('Vui lòng nhập thông tin'),
                phone: Yup.string().required('Vui lòng nhập thông tin'),
                //fb: Yup.string().required('Vui lòng nhập thông tin'),
              })}>
              {formikProps => {
                return (
                  <React.Fragment>
                    <KeyboardAwareScrollView>
                      <View style={{flex: 1, alignItems: 'center'}}>
                        <View style={styles.wrapperImage}>
                          <Image
                            source={images.DAILY_ICON}
                            style={styles.imagePresent}
                          />
                        </View>
                        <View style={styles.descriptionText}>
                          <Text style={styles.mainText}>
                            Đăng ký nhanh để nhận nhiều ưu đãi cũng như các
                            chương trình khuyến mại đặc biệt từ Azooo!
                          </Text>
                        </View>
                        <View style={styles.formField}>
                          <StyledInput
                            indexFocus={0}
                            stateFocus={indexFocus}
                            label="Tên"
                            formikProps={formikProps}
                            formikKey="nameShop"
                            onFocusProp={index => setIndexFocus(index)}
                            onPressIcon={() => setIndexFocus(0)}
                          />
                        </View>

                        <View style={styles.formField}>
                          <StyledInput
                            indexFocus={1}
                            stateFocus={indexFocus}
                            label="Địa chỉ"
                            formikProps={formikProps}
                            formikKey="address"
                            onFocusProp={index => setIndexFocus(index)}
                            onPressIcon={() => setIndexFocus(1)}
                          />
                        </View>
                        <View style={styles.formField}>
                          <StyledInput
                            indexFocus={2}
                            stateFocus={indexFocus}
                            label="Số điện thoại"
                            formikProps={formikProps}
                            formikKey="phone"
                            rightIcon={images.ICON_EDIT}
                            onFocusProp={index => setIndexFocus(index)}
                            onPressIcon={() => setIndexFocus(2)}
                          />
                        </View>
                      </View>
                      <View style={styles.buttonWrapper}>
                        <Button
                          title={'ĐĂNG KÝ NGAY'}
                          loading={registerAgencyProcess}
                          titleStyle={styles.buttonTextStyle}
                          styleContainer={styles.buttonContainer}
                          onPress={formikProps.handleSubmit}
                        />
                        <Text
                          style={styles.hotlineText}
                          onPress={() => {
                            NavigationService.popToTop();
                          }}>
                          Quay lại trang chủ
                        </Text>
                      </View>
                    </KeyboardAwareScrollView>
                  </React.Fragment>
                );
              }}
            </Formik>
          </View>
        </View>
      </View>
    );
  }
}

export default compose(
  withState('indexFocus', 'setIndexFocus', -1),
  connect(
    state => ({
      registerAgencyProcess: get(state, 'help.helpLoading', false),
    }),
    {registerAgency: helpActions.registerAgency},
  ),
)(RegisterAgencyScreen);
