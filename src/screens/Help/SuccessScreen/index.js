import React from 'react';
import {View, Image} from 'react-native';
import {compose, withState} from 'recompose';
import {connect} from 'react-redux';
import get from 'lodash/get';
import {Formik} from 'formik';
import {validationSchemaProfile} from 'src/utils/validate';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Header from 'src/components/Header';
import StyledInput from 'src/components/StyledInput';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import images from 'src/config/images';
import styles from './styles';
import NavigationService from 'src/config/NavigationService';

import * as helpActions from 'src/store/actions/help';

class SuccessScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header titleClick back title="Gửi yêu cầu thành công" />
        <View style={styles.contentView}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <View style={styles.wrapperImage}>
              <Image source={images.DONE} style={styles.imagePresent} />
            </View>
            <View style={styles.descriptionText}>
              <Text style={styles.mainText}>
                Cảm ơn bạn đã gửi yêu cầu đến chúng tôi. Yêu cầu sẽ được xử lý
                trong vòng 24h các ngày làm việc.
              </Text>
            </View>
          </View>
          <View style={styles.buttonWrapper}>
            <Button
              title={'QUAY LẠI TRANG CHỦ'}
              titleStyle={styles.buttonTextStyle}
              styleContainer={styles.buttonContainer}
              onPress={() => {
                NavigationService.popToTop();
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default compose(
  connect(
    state => ({}),
    {},
  ),
)(SuccessScreen);
