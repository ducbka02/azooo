import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper: {

    },
    rightIcon: {
        width: scale(15),
        height: scale(20),
    },
    contentView: {
        flex: 1,
        alignItems: 'center',

    },
    wrapperImage: {
        paddingVertical: scale(25)
    },
    imagePresent: {
        width: scale(90),
        height: scale(90)
    },
    descriptionText: {
        paddingHorizontal: scale(10),
    },
    mainText: {
        lineHeight: scale(16),
        color: theme.blackIconColor,
        fontSize: scale(10),
        textAlign: 'center'
    },
    formField: {
        marginTop: scale(15),
    },
    formView: {
        flex: 1,

    },
    buttonWrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: scale(15),
    },
    buttonContainer: {
        marginBottom: scale(10),
        height: scale(40),
        marginLeft: scale(15),
        marginRight: scale(15),
        borderRadius: scale(30),
        borderWidth: 1,
        borderColor: theme.pinkishColor,
        backgroundColor: theme.pinkishColor,
        justifyContent: 'center'
    },
    buttonTextStyle: {
        color: theme.pureWhite,
        fontSize: scale(15),
        lineHeight: scale(18),
        fontWeight: '600',
    },
    hotlineText: {
        fontSize: scale(10),
        color: theme.blackIconColor,
        textAlign: 'center'
    }
});
