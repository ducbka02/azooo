import React from 'react';
import {View, Image} from 'react-native';
import get from 'lodash/get';
import Header from 'src/components/Header';
import HTMLComponent from 'src/components/HTMLComponent';
import Text from 'src/components/Text';
import styles from './styles';

class DetailHelpScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {navigation} = this.props;
    const title = navigation.getParam('title', 'NO-ID');
    const body = navigation.getParam('body', 'NO-ID');
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header titleClick back title={title} />
        <View style={styles.contentView}>
          <HTMLComponent html={body} />
          {/* <Text style={styles.bodyText}>{body.trim()}</Text> */}
        </View>
      </View>
    );
  }
}

export default DetailHelpScreen;
