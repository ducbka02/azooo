import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    contentView: {
        paddingHorizontal: scale(10),
        paddingVertical: scale(5),
    },
    bodyText: {
        color: theme.blackIconColor,
        fontSize: scale(10),
        lineHeight: scale(16)
    }
});
