import React from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';
import { compose, withState } from 'recompose';
import { connect } from 'react-redux';
import get from 'lodash/get';
import Header from 'src/components/Header';
import CollectionComponent from 'src/components/CollectionComponent';
import images from 'src/config/images';
import theme from 'src/config/theme';
import styles from './styles';
import NavigationService, { ProductShowType } from 'src/config/NavigationService';

const formatData = (data, numColumns = 2) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ product_id: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

class CollectionListScreen extends React.Component {
  constructor(props) {
    super(props);
    this._renderProgram = this._renderProgram.bind(this);
    this.handleEnd = this.handleEnd.bind(this);
  }

  componentDidMount() {
    const { setListCollection } = this.props;
    const listCollection = this.props.navigation.getParam('listCollection', []);
    setListCollection(listCollection)
  }

  handleEnd = () => {

  };

  _renderProgram({ item, idx }) {
    if (item.empty === true) {
      return <View style={styles.itemInvisible} />
    }
    const { id, title, field_image } = item;
    return (
      <CollectionComponent
        key={id}
        title={title}
        field_image={field_image}
        onPress={() => {
          NavigationService.goToCategoryNavigator({ type: ProductShowType.COLLECTION, id: id });
        }}
      />
    );
  }

  render() {
    const { listCollection, isLoadMore } = this.props;
    return (
      <View style={{ flex: 1, backgroundColor: theme.placeholderColor }}>
        <Header
          titleClick
          back
          title="Bộ sưu tập"
        />
        <FlatList
          style={styles.container}
          numColumns={2}
          data={formatData(listCollection)}
          renderItem={this._renderProgram}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
          extraData={this.props}
          keyExtractor={(item) => item.id.toString()}
          removeClippedSubviews={false}
          initialNumToRender={8}
          maxToRenderPerBatch={2}
          ListFooterComponent={() => !isLoadMore
            ? null
            :
            <View style={{ paddingVertical: scale(10) }}>
              <ActivityIndicator size="large" animating />
            </View>
          }
          onEndReached={() => this.handleEnd()}
          onEndReachedThreshold={0}
        />
      </View>
    );
  }
}

export default compose(
  withState("listCollection", "setListCollection", []),
  withState('isLoadMore', 'setIsLoadMore', false),
  connect(
    (state) => ({

    }),
    {}
  )
)(CollectionListScreen);

