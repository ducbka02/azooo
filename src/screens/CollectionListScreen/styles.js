import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper: {

    },
    rightIcon: {
        width: scale(15),
        height: scale(20),
    },
    contentView: {
        flex: 1,
        alignItems: 'center',
    },
    container: {
        flex: 1,
        marginHorizontal: scale(5),
        marginVertical: scale(5),
    },
    separator: {
        height: scale(5)
    },
    itemInvisible: {
        backgroundColor: 'transparent', flex: 1,
        width: scale(100),
        height: scale(115),
        marginRight: scale(5),
        justifyContent: 'center',
        alignItems: 'center',
    },
});
