import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import { isIphoneX } from 'src/utils/iphoneXhelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    chip: {
        backgroundColor: theme.pinkishColor,
        marginRight: scale(6),
        marginBottom: scale(6),
        paddingVertical: scale(5),
        paddingHorizontal: scale(6),
        borderRadius: scale(15)
    },
    textChip: {
        fontSize: scale(9),
        color: 'white'
    },
    chipCloseBtn: {
        borderRadius: 8,
        width: 16,
        height: 16,
        backgroundColor: '#ddd',
        alignItems: 'center',
        justifyContent: 'center'
    },
    chipCloseBtnTxt: {
        color: '#555',
        paddingBottom: 3
    },
    wrapperSearchView: {
        width: '100%',
        height: isIphoneX() ? scale(64) : scale(60),
        alignItems: 'flex-end',
        backgroundColor: 'transparent',
    },
    wrapperResult: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: scale(5),
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: theme.blackSuperLightColor,
    },
    titleResult: {
        color: theme.blackIconColor,
        fontSize: scale(10),
    },
    searchView: {
        flex: 1,
        alignItems: 'flex-end',
        paddingHorizontal: scale(8),
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%',
        paddingBottom: scale(4),
    },
    wrapperInput: {
        backgroundColor: 'white',
        width: width - scale(8) * 2,
        height: scale(30),
        borderRadius: scale(2),
        flexDirection: 'row',
        paddingHorizontal: scale(10),
        alignItems: 'center'
    },
    inputStyleDefault: {
        color: theme.blackIconColor,
        width: '90%',
        paddingRight: scale(10),
        fontSize: scale(10),
    },
    iconSearch: {
        width: scale(14)
    },
    wrapperClose: {
        position: 'absolute',
        right: scale(14),
        bottom: scale(-2),
    },
    iconClose: {
        width: scale(14),
    },
    wrapperTextInput: {
        marginHorizontal: scale(5),
        width: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    leftView: {
        width: scale(33),
        height: '100%',
        position: 'absolute',
        alignItems: 'center',
        paddingTop: isIphoneX() ? scale(33) : scale(23),
        left: 0,
    },
    rightView: {
        width: scale(33),
        height: '100%',
        position: 'absolute',
        alignItems: 'center',
        paddingTop: isIphoneX() ? scale(30) : scale(20),
        right: 0,
    },
    wrapperIconLeft: {
        width: scale(10),
        height: scale(16.5),
    },
    wrapperIconRight: {
        width: scale(13),
        height: scale(21.4),
    },
    contentSearchView: {
        flex: 1,
        paddingHorizontal: scale(10),
    },
    headerCategory: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: scale(10),
        marginTop: scale(10),
    },
    trendText: {
        color: theme.blackIconColor,
        fontSize: scale(12),
        marginLeft: scale(5),
    },
    textChipStyle: {
        backgroundColor: theme.greenSearch
    }
});
