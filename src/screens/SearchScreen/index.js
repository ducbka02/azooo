import React from 'react';
import {
  View,
  Image,
  InteractionManager,
  ImageBackground,
  FlatList,
  TextInput,
  Animated,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {compose, withState} from 'recompose';
import {connect} from 'react-redux';
import get from 'lodash/get';
import debounce from 'lodash/debounce';
import SmallAnimation from 'src/config/animations/smallAnimation';
import NavigationService, {ProductShowType} from 'src/config/NavigationService';
import Text from 'src/components/Text';
import images from 'src/config/images';
import styles from './styles';
import theme from 'src/config/theme';
import {scale} from 'src/utils/scallingHelper';
import {azSearchKeyword} from 'src/services/api/app';
import {
  readObject,
  saveObject,
  removeObject,
  SEARCH_KEY,
} from 'src/utils/asyncStorage';

const {width} = Dimensions.get('window');
const timeAnimation = 200;

const Chips = props => {
  const {value, onPress, textChipStyle} = props;
  return (
    <TouchableOpacity style={[styles.chip, textChipStyle]} onPress={onPress}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.textChip}>{value}</Text>
      </View>
    </TouchableOpacity>
  );
};

class SearchScreen extends React.Component {
  constructor(props) {
    super(props);

    this.wrapperInputAnimated = new Animated.Value(0);
    this.focusInputAction = this.focusInputAction.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onChangeTextDelayed = debounce(this.onChangeText, 300);
    this._renderProgram = this._renderProgram.bind(this);
    this.onNavigateToAgency = this.onNavigateToAgency.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  async componentDidMount() {
    const {setSearchKey} = this.props;
    InteractionManager.runAfterInteractions(async () => {
      const searchHistory = await readObject(SEARCH_KEY);
      setSearchKey(searchHistory);
    });
  }

  onNavigateToAgency(data) {
    NavigationService.navigate('Agency');
  }

  handleClick(data) {
    const type = this.props.navigation.getParam('type', '');
    if (type === 'HOME') {
      NavigationService.goToCategoryNavigator({
        type: ProductShowType.SEARCH,
        id: data,
      });
    } else {
      NavigationService.goBack();
      this.props.navigation.state.params.goBackFromSearchScreen(data);
    }
  }

  focusInputAction() {
    return Animated.sequence([
      Animated.delay(0),
      SmallAnimation(this.wrapperInputAnimated, 1, timeAnimation),
    ]).start();
  }

  async onChangeText(text) {
    const {setTextSearch, setProductSearch} = this.props;
    setTextSearch(text);

    const listSearch = await azSearchKeyword(text);
    setProductSearch(listSearch);
  }

  _renderProgram(item) {
    const {product_id, title} = item;
    return (
      <TouchableOpacity
        style={styles.wrapperResult}
        activeOpacity={1}
        onPress={() => {
          const type = this.props.navigation.getParam('type', '');
          if (type === 'HOME') {
            this.props.navigation.replace('ProductDetailScreen', {
              data: item,
              onNavigateToAgency: this.onNavigateToAgency,
            });
          } else {
            this.handleClick(item);
          }
        }}>
        <View style={{flex: 0.8}}>
          <Text nOfLines={1} style={styles.titleResult}>
            {title}
          </Text>
        </View>
        <View style={{flex: 0.2, alignItems: 'flex-end'}}>
          <Image
            resizeMode="contain"
            source={images.ICON_RECALL}
            style={{tintColor: theme.blackLightColor}}
          />
        </View>
      </TouchableOpacity>
    );
  }

  renderContentView() {
    const {productSearch, searchKey, setSearchKey} = this.props;

    if (productSearch.length > 0) {
      return (
        <View style={styles.contentSearchView}>
          <FlatList
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag"
            showsVerticalScrollIndicator={false}
            style={styles.container}
            data={productSearch}
            renderItem={({item}) => this._renderProgram(item)}
            extraData={this.props}
            keyExtractor={item => item.product_id.toString()}
            removeClippedSubviews={false}
            initialNumToRender={8}
            maxToRenderPerBatch={2}
          />
        </View>
      );
    }

    const chips = this.props.listPopularKeyword.map((item, index) => (
      <Chips
        key={index}
        value={item.keyword}
        textChipStyle={styles.textChipStyle}
        onPress={() => {
          this.handleClick(item.keyword);
        }}
      />
    ));

    let chipsHistory = null;
    if (searchKey) {
      chipsHistory = searchKey.split(',').map((item, index) => (
        <Chips
          key={index}
          value={item}
          onPress={() => {
            this.handleClick(item);
          }}
        />
      ));
    }

    return (
      <View style={styles.contentSearchView}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag">
          {searchKey !== null && (
            <View>
              <View
                style={[
                  styles.headerCategory,
                  {justifyContent: 'space-between'},
                ]}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image source={images.ICON_HISTORY} />
                  <Text style={styles.trendText}>Lịch sử tìm kiếm</Text>
                </View>
                <Text
                  onPress={async () => {
                    await removeObject(SEARCH_KEY);
                    setSearchKey(null);
                  }}
                  style={[styles.trendText, {fontSize: scale(8)}]}>
                  Xóa tất cả
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  alignItems: 'center',
                }}>
                {chipsHistory}
              </View>
            </View>
          )}
          <View style={styles.headerCategory}>
            <Image source={images.ICON_TREND} />
            <Text style={styles.trendText}>Được nhiều người quan tâm</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems: 'center',
            }}>
            {chips}
          </View>
        </ScrollView>
      </View>
    );
  }

  render() {
    const {searchKey, setSearchKey, textSearch, setTextSearch} = this.props;

    const wrapperInputTranslateWidth = this.wrapperInputAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [width - scale(8) * 2, width - scale(8) * 2 - scale(25)],
      extrapolateRight: 'clamp',
    });
    const wrapperInputTranslateMargin = this.wrapperInputAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [0, scale(25)],
      extrapolateRight: 'clamp',
    });

    return (
      <View style={{flex: 1}}>
        <ImageBackground
          source={images.HEADER_IMAGE}
          style={styles.wrapperSearchView}>
          <View style={styles.searchView}>
            <Animated.View
              style={[
                styles.wrapperInput,
                {
                  width: wrapperInputTranslateWidth,
                  marginLeft: wrapperInputTranslateMargin,
                },
              ]}>
              <Image
                source={images.ICON_SEARCH}
                style={styles.iconSearch}
                resizeMode="contain"
              />
              <View style={styles.wrapperTextInput}>
                <TextInput
                  ref={c => (this.tf = c)}
                  style={styles.inputStyleDefault}
                  maxLength={20}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType="search"
                  returnKeyLabel="Tìm"
                  autoFocus={true}
                  onFocus={() => this.focusInputAction()}
                  placeholder="Bạn cần tìm gì?"
                  onChangeText={this.onChangeTextDelayed}
                  onSubmitEditing={async () => {
                    if (textSearch.length === 0) return;
                    let searchKeyTmp = '';
                    if (searchKey) {
                      searchKeyTmp = searchKey.slice();
                      searchKeyTmp = searchKeyTmp.concat(',', textSearch);
                    } else {
                      searchKeyTmp = textSearch;
                    }
                    this.handleClick(textSearch);
                    await saveObject(SEARCH_KEY, searchKeyTmp);
                    setSearchKey(searchKeyTmp);
                  }}
                />
                {textSearch.length > 0 && (
                  <TouchableOpacity
                    style={styles.wrapperClose}
                    onPress={() => {
                      this.tf.clear();
                      setTextSearch('');
                    }}>
                    <Image source={images.ICON_CLOSE} />
                  </TouchableOpacity>
                )}
              </View>
            </Animated.View>
            <TouchableOpacity
              style={styles.leftView}
              onPress={() => NavigationService.goBack()}>
              <View style={styles.wrapperIconLeft}>
                <Image
                  resizeMode="contain"
                  source={images.ICON_BACK}
                  style={{width: '100%', height: '100%'}}
                />
              </View>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.rightView}>
              <View style={styles.wrapperIconRight}>
                <Image resizeMode='contain' source={images.FILTER} style={{ width: '100%', height: '100%' }} />
              </View>
            </TouchableOpacity> */}
          </View>
        </ImageBackground>
        {this.renderContentView()}
      </View>
    );
  }
}

export default compose(
  withState('searchKey', 'setSearchKey', null),
  withState('textSearch', 'setTextSearch', ''),
  withState('productSearch', 'setProductSearch', []),
  connect(
    state => ({
      listPopularKeyword: get(state, 'app.listPopularKeyword', []),
    }),
    {},
  ),
)(SearchScreen);
