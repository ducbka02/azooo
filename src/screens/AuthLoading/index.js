import React from 'react';
import { View, Image, ImageBackground, InteractionManager } from 'react-native';
import { compose, withState } from 'recompose';
import { connect } from 'react-redux';
import Text from 'src/components/Text';
import images from 'src/config/images';
import styles from './styles';
import FCM from '../../config/FCM';

import * as appActions from 'src/store/actions/app';

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      FCM.initAnalytics();
      this.props.getAppInfo(() => {
        this.props.navigation.navigate('App');
      });
    });
  }

  render() {
    return (
      <ImageBackground source={images.BACKGROUND} style={styles.wrapper}>
        <View style={styles.wrapperImage}>
          <Image source={images.AZOOO_LOGO_RAW} resizeMode='contain' />
        </View>
        <View style={styles.wrapperBottom}>
          <Text style={styles.versionText}>
            Azooo 1.0 Version
          </Text>
        </View>
      </ImageBackground>
    );
  }
}

export default compose(
  connect(
    (state) => ({
      
    }),
    { getAppInfo: appActions.getAppInfo }
  )
)(AuthLoadingScreen);