import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
    },
    wrapperImage: {
        flex: 0.9,
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrapperBottom: {
        flex: 0.1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    versionText: {
        color: 'white',
        fontSize: scale(12),
    }
});
