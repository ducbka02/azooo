import React from 'react';
import {View, TouchableOpacity, FlatList, Image} from 'react-native';
import {compose, withState} from 'recompose';
import {connect} from 'react-redux';
import get from 'lodash/get';
import Header from 'src/components/Header';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import images from 'src/config/images';
import styles from './styles';
import NavigationService from 'src/config/NavigationService';

class NotificationScreen extends React.Component {
  constructor(props) {
    super(props);
    this._renderProgram = this._renderProgram.bind(this);
  }

  _renderProgram(item) {
    const {title, content, time} = item;
    return (
      <TouchableOpacity style={styles.itemContainer}>
        <View style={{flex: 0.2}}>
          <View style={styles.iconNotiView}>
            <Image source={images.NOTIFICATION} style={styles.notiIcon} />
          </View>
        </View>
        <View style={{flex: 0.8}}>
          <Text numberOfLines={1} style={styles.title}>
            {title}
          </Text>
          <Text numberOfLines={2} style={styles.content}>
            {content}
          </Text>
          <Text numberOfLines={1} style={styles.time}>
            {time}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const dataPlaceholder = [
      // {
      //   id: 1,
      //   title: 'Chia sẻ nhận xét về sản phẩm',
      //   content:
      //     'Chia sẻ cảm nhận của bạn về đơn hàng 1213123AEERECASDA cho Người bán.',
      //   time: '10:10AM 26/5/19',
      // },
      // {
      //   id: 2,
      //   title: 'Giao hàng thành công',
      //   content:
      //     'Đơn hàng 1213123AEERECASDA đã giao đến bàn ngày 26/5/19. Vui lòng xác nhận',
      //   time: '10:10AM 26/5/19',
      // },
      // {
      //   id: 3,
      //   title: 'Cập nhật sản phẩm',
      //   content: 'Có rất nhiều sản phẩm mới trên Azooo. Mời các bạn vào xem',
      //   time: '10:10AM 26/5/19',
      // },
    ];
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header titleClick back title="Thông báo" />
        <View style={styles.contentView}>
          <FlatList
            showsVerticalScrollIndicator={false}
            style={styles.flatlistStyle}
            data={dataPlaceholder}
            renderItem={({item}) => this._renderProgram(item)}
            keyExtractor={item => item.id.toString()}
          />
        </View>
      </View>
    );
  }
}

export default compose(
  connect(
    state => ({}),
    {},
  ),
)(NotificationScreen);
