import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper: {

    },
    rightIcon: {
        width: scale(15),
        height: scale(20),
    },
    contentView: {
        flex: 1,
        alignItems: 'center',
    },
    flatlistStyle: {
        flex: 1,
        width: '100%',
        marginHorizontal: scale(10),
        marginVertical: scale(10),
    },
    itemContainer: {
        marginHorizontal: scale(10),
        marginVertical: scale(5),
        paddingHorizontal: scale(10),
        paddingVertical: scale(10),
        borderRadius: scale(5),
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: theme.blackLightColor,
        flexDirection: 'row',
    },
    notiIcon: {
        width: scale(15),
        height: scale(20),
    },
    iconNotiView: {
        backgroundColor: theme.pinkishColor,
        width: scale(32),
        height: scale(32),
        borderRadius: scale(16),
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: theme.blackIconColor,
        fontSize: scale(12)
    },
    content: {
        color: theme.blackLightColor,
        fontSize: scale(10),
        lineHeight: scale(12),
    },
    time: {
        color: theme.blackLightColor,
        fontSize: scale(9),
        lineHeight: scale(16),
        alignSelf: 'flex-end',
    }
});
