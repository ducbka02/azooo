import * as React from 'react';
import {Dimensions} from 'react-native';
import {createDrawerNavigator, DrawerActions} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import MainCategoryScreen from './MainCategoryScreen';
import CategoryDrawer from './CategoryDrawer';
import theme from 'src/config/theme';

const DEVICE_WIDTH = Dimensions.get('window').width;

const MainStack = createStackNavigator(
  {
    MainCategoryScreen: {
      screen: MainCategoryScreen,
    },
  },
  {
    initialRouteName: 'MainCategoryScreen',
    headerMode: 'none',
    navigationOptions: ({navigation}) => {
      //console.warn(navigation);
      const extraConfig = {
        tabBarVisible: true,
      };
      return {
        ...extraConfig,
        gesturesEnabled: false,
      };
    },
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
  },
);

export default createDrawerNavigator(
  {
    MainStack: {
      path: '/main',
      screen: MainStack,
    },
  },
  {
    contentOptions: {
      activeTintColor: '#e91e63',
    },
    drawerBackgroundColor: theme.pinkishColor,
    contentComponent: CategoryDrawer,
    drawerLockMode: 'locked-open',
    //initialRouteName: 'MainStack',
    // defaultNavigationOptions: {
    //   drawerLockMode: 'locked-closed',
    // },
  },
);

// export default createDrawerNavigator(
//   {
//     StackLeft: {
//       screen: StackLeft,
//     },
//   },
//   {
//     contentOptions: {
//       activeTintColor: '#e91e63',
//     },
//     drawerPosition: 'right',
//     contentComponent: CategoryDrawer,
//     initialRouteName: 'StackLeft',
//     getCustomActionCreators: (route, stateKey) => {
//       return {
//         toggleOuterDrawer: () => DrawerActions.toggleDrawer({key: stateKey}),
//       };
//     },
//     defaultNavigationOptions: {
//       drawerLockMode: 'locked-closed',
//     },
//     navigationOptions: {
//       drawerLockMode: 'locked-closed',
//     },
//   },
// );
