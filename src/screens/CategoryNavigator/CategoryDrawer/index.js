import React from 'react';
import {
  View,
  Image,
  InteractionManager,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import {compose, withState} from 'recompose';
import {connect} from 'react-redux';
import get from 'lodash/get';
import Text from 'src/components/Text';
import Collapsed from 'src/components/Collapsed';
import images from 'src/config/images';
import styles from './styles';
import {scale} from 'src/utils/scallingHelper';
import * as appActions from 'src/store/actions/app';

class CategoryDrawer extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {});
  }

  render() {
    const {
      listCategoryProduct,
      listSubCategoryProduct,
      navigation,
      indexExpand,
      setIndexExpand,
      drawerGetProductCategory,
    } = this.props;
    return (
      <SafeAreaView style={{flex: 1}}>
        <TouchableOpacity
          style={styles.logoView}
          onPress={() => navigation.closeDrawer()}>
          <Image source={images.AZOOO_LOGO_RAW} />
        </TouchableOpacity>
        <View style={styles.contentView}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={images.ICON_LIST} />
            <Text style={[styles.textCommon, {marginLeft: scale(15)}]}>
              Tất cả danh mục
            </Text>
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            {listCategoryProduct.map(({category_id, category_name}, index) => (
              <Collapsed
                expand={index === indexExpand}
                onItemClick={() => {
                  if (index === indexExpand) {
                    setIndexExpand(-1);
                  } else {
                    setIndexExpand(index);
                  }
                  drawerGetProductCategory(category_id);
                }}
                onSubItemClick={c_id => {
                  this.props.navigation.closeDrawer();
                  drawerGetProductCategory(c_id);
                }}
                key={category_id}
                title={category_name}
                description={listSubCategoryProduct[category_id]}
              />
            ))}
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

export default compose(
  withState('indexExpand', 'setIndexExpand', -1),
  connect(
    state => ({
      listCategoryProduct: get(state, 'app.listCategoryProduct', []),
      listSubCategoryProduct: get(state, 'app.listSubCategoryProduct', {}),
    }),
    {drawerGetProductCategory: appActions.drawerGetProductCategory},
  ),
)(CategoryDrawer);
