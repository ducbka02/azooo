import {StyleSheet, Dimensions} from 'react-native';
import {scale} from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },
  logoView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentView: {
    marginVertical: scale(10),
    marginLeft: scale(15),
    marginRight: scale(10),
  },
  textCommon: {
    fontSize: scale(12),
    color: 'white',
  },
});
