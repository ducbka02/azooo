/* eslint-disable no-unused-vars */
/* eslint-disable eqeqeq */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  FlatList,
  RefreshControl,
  TextInput,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {compose, withState} from 'recompose';
import {connect} from 'react-redux';
import debounce from 'lodash/debounce';
import get from 'lodash/get';
import Text from 'src/components/Text';
import Header from 'src/components/Header';
import SearchView from 'src/components/SearchView';
import SortComponent from 'src/components/SortComponent';
import ProductComponent from 'src/components/ProductComponent';
import images from 'src/config/images';
import styles from './styles';
import {scale} from 'src/utils/scallingHelper';
import {getListBestSelling, getListNewProduct} from 'src/services/api/app';
import theme from 'src/config/theme';
import NavigationService, {ProductShowType} from 'src/config/NavigationService';
import Spinner from 'react-native-spinkit';

import * as appActions from 'src/store/actions/app';

const formatData = (data, numColumns = 2) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
  while (
    numberOfElementsLastRow !== numColumns &&
    numberOfElementsLastRow !== 0
  ) {
    data.push({product_id: `blank-${numberOfElementsLastRow}`, empty: true});
    numberOfElementsLastRow++;
  }

  return data;
};

class MainCategoryScreen extends React.Component {
  constructor(props) {
    super(props);

    const index = this.props.navigation.getParam('index', -1);
    this.pageIndex = 0;
    this.state = {
      refreshing: false,
      showLoading: true,
      index,
    };

    this._renderProgram = this._renderProgram.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onChangeTextDelayed = debounce(this.onChangeText, 300);
    this.onNavigateToAgency = this.onNavigateToAgency.bind(this);
    this.hardTextSearch = this.hardTextSearch.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.handleEnd = this.handleEnd.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  componentDidMount() {
    const {
      navigation,
      drawerGetProductCategory,
      drawerGetProductCollection,
      drawerGetProductBrand,
      drawerGetProductSale,
      hardDrawerProductList,
    } = this.props;
    const type = navigation.getParam('type', '');
    const id = navigation.getParam('id', 0);
    if (type === ProductShowType.COLLECTION) {
      drawerGetProductCollection(id);
    } else if (type === ProductShowType.CATEGORY) {
      drawerGetProductCategory(id);
    } else if (type === ProductShowType.SEARCH) {
      this.hardTextSearch(id);
    } else if (type === ProductShowType.SALE) {
      drawerGetProductSale(id);
    } else if (type === ProductShowType.BRAND) {
      drawerGetProductBrand(id);
    } else if (type === ProductShowType.PRODUCT_LIST) {
      if (id !== 0) {
        if (Platform.OS === 'ios') {
          setTimeout(() => {
            hardDrawerProductList(id);
          }, 100);
        } else {
          hardDrawerProductList(id);
        }
      }
    }
  }

  hardTextSearch(text) {
    const {
      setTextSearch,
      drawerSeachProduct,
      hardDrawerProductList,
    } = this.props;
    setTextSearch(text);
    drawerSeachProduct(text, list => {
      hardDrawerProductList(list);
    });
    if (this.tf) {
      this.tf.setNativeProps({text: text});
    }
  }

  onNavigateToAgency(data) {
    NavigationService.navigate('Agency', {data: data});
  }

  _renderProgram(item) {
    const {navigation} = this.props;
    if (item.empty === true) {
      return <View style={styles.itemInvisible} />;
    }
    const {
      product_id,
      title,
      field_image,
      field_images,
      category_name,
      price__number,
      promotion_price,
      field_thumbnail,
    } = item;
    return (
      <ProductComponent
        key={product_id}
        title={title}
        field_image={field_image || field_images}
        category_name={category_name}
        price__number={price__number}
        promotion_price={promotion_price}
        field_thumbnail={field_thumbnail}
        onPress={() => {
          navigation.push('ProductDetailScreen', {
            data: item,
            onNavigateToAgency: this.onNavigateToAgency,
          });
        }}
      />
    );
  }

  onChangeText(text) {
    const {
      setTextSearch,
      drawerSeachProduct,
      hardDrawerProductList,
    } = this.props;
    setTextSearch(text);

    drawerSeachProduct(text, list => {
      if (!this.tf.isFocused()) {
        hardDrawerProductList(list);
      }
    });
  }

  renderFooter = () => {
    if (this.props.isLoadMore) {
      return (
        <View
          style={{
            paddingVertical: scale(10),
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Spinner
            style={{width: 25}}
            size={25}
            type="Wave"
            color={theme.pinkishColor}
          />
        </View>
      );
    }
    return null;
  };

  onRefresh = async () => {
    this.setState({refreshing: true}, async () => {
      this.pageIndex = 0;
      const {navigation} = this.props;
      const type = navigation.getParam('type', '');
      const id = navigation.getParam('id', 0);
      const {index} = this.state;
      if (type === ProductShowType.COLLECTION) {
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
      } else if (type === ProductShowType.CATEGORY) {
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
      } else if (type === ProductShowType.SEARCH) {
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
      } else if (type === ProductShowType.SALE) {
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
      } else if (type === ProductShowType.BRAND) {
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
      } else if (type === ProductShowType.PRODUCT_LIST) {
        if (index == 1) {
          this.pageIndex = 0;
          this.props.hardDrawerProductList(this.props.listBestSelling);
          setTimeout(() => {
            this.setState({refreshing: false});
          }, 1000);
        } else if (index == 2) {
          this.pageIndex = 0;
          this.props.hardDrawerProductList(this.props.listNewProduct);
          this.setState({refreshing: false});
          setTimeout(() => {
            this.setState({refreshing: false});
          }, 1000);
        }
      }
    });
  };

  async handleEnd() {
    const {
      isLoadMore,
      setIsLoadMore,
      isListEnd,
      setIsListEnd,
      listDrawerProduct,
      navigation,
    } = this.props;
    const {index} = this.state;
    if (isLoadMore) {
      return;
    }
    if (isListEnd) {
      return;
    }
    if (index == -1) {
      setIsLoadMore(false);
      setIsListEnd(true);
      return;
    }

    this.props.setIsLoadMore(true, async () => {
      this.pageIndex++;
      if (index == 1) {
        try {
          const list = await getListBestSelling(20, this.pageIndex * 20 + 1);
          const newList = [...listDrawerProduct, ...list];
          this.props.hardDrawerProductList(newList);
          setIsLoadMore(false);
        } catch (e) {
          setIsLoadMore(false);
          setIsListEnd(true);
        }
      } else {
        try {
          const list = await getListNewProduct(20, this.pageIndex * 20 + 1);
          const newList = [...listDrawerProduct, ...list];
          this.props.hardDrawerProductList(newList);
          setIsLoadMore(false);
        } catch (e) {
          setIsLoadMore(false);
          setIsListEnd(true);
        }
      }
    });
  }

  renderContent() {
    const {drawerSearchProcess, listDrawerProduct} = this.props;

    if (drawerSearchProcess) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Spinner
            style={{width: 25}}
            size={25}
            type="Wave"
            color={theme.pinkishColor}
          />
        </View>
      );
    }

    if (listDrawerProduct.length === 0) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingTop: scale(100),
            paddingHorizontal: scale(20),
          }}>
          <Image source={images.EMPTY_PRODUCT} />
          <Text
            style={{
              color: theme.blackIconColor,
              fontSize: scale(10),
              textAlign: 'center',
            }}>
            Chưa có sản phẩm nào trong danh mục này
          </Text>
        </View>
      );
    }

    if (listDrawerProduct.length < 20) {
      return (
        <FlatList
          style={styles.container}
          numColumns={2}
          data={formatData(listDrawerProduct)}
          renderItem={({item}) => this._renderProgram(item)}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
          extraData={this.props}
          keyExtractor={(item, index) =>
            `${item.product_id.toString()}-${index}`
          }
          removeClippedSubviews={false}
          getItemLayout={(data, index) => ({
            length: scale(130),
            offset: scale(130) * index,
            index,
          })}
          initialNumToRender={8}
          maxToRenderPerBatch={2}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
        />
      );
    }

    return (
      <FlatList
        style={styles.container}
        numColumns={2}
        data={formatData(listDrawerProduct)}
        renderItem={({item}) => this._renderProgram(item)}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        extraData={this.props}
        keyExtractor={(item, index) => `${item.product_id.toString()}-${index}`}
        //removeClippedSubviews={false}
        //getItemLayout={(data, index) => ({ length: scale(130), offset: scale(130) * index, index })}
        initialNumToRender={8}
        //maxToRenderPerBatch={2}
        ListFooterComponent={this.renderFooter}
        onEndReached={this.handleEnd}
        onEndReachedThreshold={0.5}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
      />
    );
  }

  render() {
    const {
      setFocus,
      focus,
      textSearch,
      setTextSearch,
      listDrawerProduct,
      listDrawerSearchResult,
      hardDrawerProductList,
    } = this.props;

    return (
      <View style={{flex: 1, backgroundColor: theme.placeholderColor}}>
        <Header
          back
          title={
            <View style={styles.titleHeader}>
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}>
                <Image
                  resizeMode="contain"
                  source={images.ICON_LIST}
                  style={styles.iconList}
                />
              </TouchableOpacity>
              <View style={styles.wrapperInput}>
                <Image
                  source={images.ICON_SEARCH}
                  style={styles.iconSearch}
                  resizeMode="contain"
                />
                <View style={styles.wrapperTextInput}>
                  <TextInput
                    ref={c => (this.tf = c)}
                    style={styles.inputStyleDefault}
                    maxLength={40}
                    autoCapitalize="none"
                    autoCorrect={false}
                    returnKeyType="search"
                    returnKeyLabel="Tìm"
                    placeholder="Bạn cần tìm gì?"
                    onChangeText={this.onChangeTextDelayed}
                    onFocus={() => setFocus(true)}
                    onSubmitEditing={async () => {
                      setFocus(false);
                      hardDrawerProductList(listDrawerSearchResult);
                    }}
                  />
                </View>
                {textSearch.length > 0 && focus && (
                  <TouchableOpacity
                    style={styles.wrapperClose}
                    onPress={() => {
                      this.onChangeText('');
                      this.tf.clear();
                      setTextSearch('');
                    }}>
                    <Image
                      source={images.ICON_CLOSE}
                      style={{width: '80%', height: '80%'}}
                    />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          }
          rightIcon={
            <Image source={images.NOTIFICATION} style={styles.rightIcon} />
          }
          onRightFunc={() =>
            this.props.navigation.navigate('NotificationScreen')
          }
        />
        <SortComponent
          onPopularClick={() => {
            this.setState({index: 1}, () => {
              this.pageIndex = 0;
              hardDrawerProductList(this.props.listBestSelling);
            });
          }}
          onBestSellingClick={() => {
            this.setState({index: 1}, () => {
              this.pageIndex = 0;
              hardDrawerProductList(this.props.listBestSelling);
            });
          }}
          onNewestClick={() => {
            this.setState({index: 2}, () => {
              this.pageIndex = 0;
              hardDrawerProductList(this.props.listNewProduct);
            });
          }}
          onAmountClick={async asc => {
            this.pageIndex = 0;
            const {index} = this.state;
            if (index == 1) {
              try {
                const list = await getListBestSelling(
                  20,
                  0,
                  asc ? 'list_price ASC' : 'list_price DESC',
                );
                const newList = [...list];
                this.props.hardDrawerProductList(newList);
              } catch (e) {}
            } else {
              try {
                const list = await getListNewProduct(
                  20,
                  0,
                  asc ? 'list_price ASC' : 'list_price DESC',
                );
                const newList = [...list];
                this.props.hardDrawerProductList(newList);
              } catch (e) {}
            }
          }}
        />
        <SearchView
          show={focus}
          searchResult={listDrawerSearchResult}
          onClickSearchResult={item => {
            setFocus(false);
            NavigationService.navigate('ProductDetailScreen', {
              data: item,
              onNavigateToAgency: this.onNavigateToAgency,
            });
          }}
          onTextClick={text => {
            setFocus(false);
            this.tf.blur();
            this.hardTextSearch(text);
          }}
        />
        {this.renderContent()}
      </View>
    );
  }
}

export default compose(
  withState('focus', 'setFocus', false),
  withState('textSearch', 'setTextSearch', ''),
  withState('isLoadMore', 'setIsLoadMore', false),
  withState('isListEnd', 'setIsListEnd', false),
  connect(
    state => ({
      drawerSearchProcess: get(state, 'app.drawerSearchProcess', false),
      listDrawerSearchResult: get(state, 'app.listDrawerSearchResult', []),
      listDrawerProduct: get(state, 'app.listDrawerProduct', []),
      listBestSelling: get(state, 'app.listBestSelling', []),
      listNewProduct: get(state, 'app.listNewProduct', []),
    }),
    {
      drawerSeachProduct: appActions.drawerSeachProduct,
      drawerGetProductCategory: appActions.drawerGetProductCategory,
      drawerGetProductCollection: appActions.drawerGetProductCollection,
      hardDrawerProductList: appActions.hardDrawerProductList,
      drawerGetProductBrand: appActions.drawerGetProductBrand,
      drawerGetProductSale: appActions.drawerGetProductSale,
    },
  ),
)(MainCategoryScreen);
