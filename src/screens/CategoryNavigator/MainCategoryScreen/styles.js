import { StyleSheet, Dimensions, } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const { width } = Dimensions.get('window');

export default StyleSheet.create({
    wrapper: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
    },
    titleHeader: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: scale(4),
        flexDirection: 'row',
        alignItems: 'flex-end',
        backgroundColor: 'transparent'
    },
    textInputView: {
        flex: 1,
        backgroundColor: 'white',
        width: '100%',
        height: scale(28),
        marginLeft: scale(5),
        marginRight: scale(0),
        borderRadius: scale(2),
    },
    wrapperInput: {
        flex: 1,
        backgroundColor: 'white',
        width: '100%',
        height: scale(28),
        borderRadius: scale(2),
        flexDirection: 'row',
        paddingHorizontal: scale(5),
        marginLeft: scale(15),
        marginRight: scale(10),
        alignItems: 'center'
    },
    wrapperClose: {
        width: scale(14),
        height: scale(14),
        position: 'absolute',
        right: scale(2),
        alignItems: 'center',
        justifyContent: 'center'
    },
    inputStyleDefault: {
        color: theme.blackIconColor,
        width: '90%',
        paddingRight: scale(10),
        fontSize: scale(9),
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconSearch: {
        width: scale(10)
    },
    wrapperTextInput: {
        marginHorizontal: scale(5),
        width: '100%',
        height: '100%',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    rightIcon: {
        width: scale(15),
        height: scale(20),
    },
    iconList: {
        height: scale(20),
        marginBottom: scale(4),
    },
    container: {
        flex: 1,
        marginHorizontal: scale(5),
        marginVertical: scale(5),
    },
    itemInvisible: {
        backgroundColor: 'transparent', flex: 1,
        width: scale(100),
        height: scale(115),
        marginRight: scale(5),
        justifyContent: 'center',
        alignItems: 'center',
    },
    placeholderView: {
        width: (width - scale(5) * 2) / 2,
        height: scale(115),
        padding: scale(2),
        justifyContent: 'center',
        alignItems: 'center',
    },
    placeholderContent: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: theme.placeholderColor,
        alignItems: 'center',
        padding: scale(5)
    },
    linePlaceholderStyle: {
        backgroundColor: theme.blackSuperLightColor,
        alignSelf: 'center',
    },
    separator: {
        height: scale(5)
    }
});
