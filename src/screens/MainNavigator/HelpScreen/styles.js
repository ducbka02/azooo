import {StyleSheet, Dimensions} from 'react-native';
import {scale} from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  wrapper: {},
  rightIcon: {
    width: scale(15),
    height: scale(20),
  },
  contentView: {
    flex: 1,
    alignItems: 'center',
  },
  brandView: {
    paddingVertical: scale(20),
    alignItems: 'center',
  },
  brandImage: {
    width: scale(103),
    height: scale(49.5),
  },
  brandText: {
    color: theme.blackIconColor,
    lineHeight: scale(20),
    textAlign: 'center',
  },
  brandEmail: {
    color: theme.blackLightColor,
    paddingTop: scale(12),
    fontSize: scale(10),
    textAlign: 'center',
  },
  contactView: {
    flexDirection: 'row',
    //paddingHorizontal: scale(10),
    justifyContent: 'space-around',
    paddingVertical: scale(12),
  },
  buttonContainer: {
    width: (width - scale(10) * 2 - 10) / 2,
    height: scale(30),
    borderRadius: 5,
  },
  wrapperList: {
    width: '100%',
    flex: 1,
  },
  programItem: {
    marginHorizontal: scale(10),
    borderRadius: scale(5),
    borderColor: theme.blackSuperLightColor,
    borderWidth: StyleSheet.hairlineWidth,
    paddingVertical: scale(15),
    paddingLeft: scale(5),
    paddingRight: scale(15),
    width: width - scale(20),
    marginVertical: scale(5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  phanhoiIcon: {
    width: scale(29),
    height: scale(29),
  },
  dailyIcon: {
    width: scale(29),
    height: scale(26.5),
  },
  trogiupIcon: {
    width: scale(27),
    height: scale(27),
  },
  textItemProgram: {
    color: theme.blackIconColor,
  },
  subTextItemProgram: {
    fontSize: scale(10),
    color: theme.blackLightColor,
  },
  connectSocialView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: scale(7),
  },
  connectIcon: {
    width: scale(30),
  },
});
