/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Image, FlatList, TouchableOpacity} from 'react-native';
import Header from 'src/components/Header';
import Text from 'src/components/Text';
import Button from 'src/components/Button';
import images from 'src/config/images';
import theme from 'src/config/theme';
import styles from './styles';
import Snackbar from 'react-native-snackbar';
import {scale} from 'src/utils/scallingHelper';
import {
  Call,
  FacebookPage,
  FacebookShopPage,
  FacebookYoutubeChannelPage,
  FacebookInstagramPage,
} from 'src/utils/linkingHelper';
import {getSocialInfo} from 'src/services/api/help';

class HelpScreen extends React.Component {
  constructor(props) {
    super(props);

    this._renderProgram = this._renderProgram.bind(this);
  }

  async componentDidMount() {
    //const info = await getSocialInfo();
    //console.warn(info);
  }

  _renderProgram(item) {
    const {onPress, title, subTitle, icon} = item;
    return (
      <TouchableOpacity style={styles.programItem} onPress={onPress}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          {icon}
          <View
            style={{marginLeft: 10, flex: 1, justifyContent: 'space-between'}}>
            <Text nOfLines={1} style={styles.textItemProgram}>
              {title}
            </Text>
            <Text nOfLines={1} style={styles.subTextItemProgram}>
              {subTitle}
            </Text>
          </View>
        </View>
        <Image source={images.FORWARD} />
      </TouchableOpacity>
    );
  }

  renderHeader() {
    return (
      <View style={{paddingHorizontal: scale(10),}}>
        <View style={styles.brandView}>
          <Image source={images.AZOOO_LOGO} style={styles.brandImage} />
        </View>
        <Text style={styles.brandText}>
          AZ Retail Association Joint Stock Company
        </Text>
        <Text style={styles.brandText}>
          Hệ thống thương mại điện tử tương tác Azooo
        </Text>
        <Text style={styles.brandEmail}>Email: hotro@azooo.vn</Text>
        <View style={styles.contactView}>
          <Button
            onPress={() => {
              Call('19001169', true);
            }}
            title="Hotline: 1900 1169"
            leftIcon={
              <Image
                source={images.CALL}
                style={{width: '100%', height: '100%'}}
              />
            }
            styleContainer={[styles.buttonContainer, {marginRight: 5}]}
          />
          <Button
            onPress={() => {
              FacebookPage('292209594717822');
            }}
            title="Chat với Azooo"
            titleStyle={{color: theme.darkBlue}}
            leftIcon={
              <Image
                source={images.MESS}
                style={{width: '100%', height: '100%'}}
              />
            }
            styleContainer={[
              styles.buttonContainer,
              {marginLeft: 5, borderColor: theme.darkBlue},
            ]}
          />
        </View>
        <View style={styles.connectSocialView}>
          <TouchableOpacity
            style={{paddingHorizontal: 5}}
            onPress={() => {
              FacebookShopPage('292209594717822');
            }}>
            <Image
              resizeMode="contain"
              source={images.CONNECT_FB}
              style={styles.connectIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{paddingHorizontal: 5}}
            onPress={() => {
              FacebookYoutubeChannelPage();
            }}>
            <Image
              resizeMode="contain"
              source={images.CONNECT_YOUTUBE}
              style={styles.connectIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{paddingHorizontal: 5}}
            onPress={() => {
              Snackbar.show({
                title: 'Chức năng đang phát triển',
                duration: Snackbar.LENGTH_SHORT,
              });
            }}>
            <Image
              resizeMode="contain"
              source={images.CONNECT_ZALO}
              style={styles.connectIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{paddingHorizontal: 5}}
            onPress={() => {
              FacebookInstagramPage();
            }}>
            <Image
              resizeMode="contain"
              source={images.CONNECT_INSTAGRAM}
              style={styles.connectIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    const {navigation} = this.props;
    const sections = [
      {
        key: '1',
        title: 'Phản hồi',
        subTitle: 'Gửi phản hồi của bạn về chất lượng dịch vụ',
        icon: <Image source={images.PHANHOI} style={styles.phanhoiIcon} />,
        onPress: () => {
          navigation.navigate('ReplyScreen');
        },
      },
      {
        key: '2',
        title: 'Đăng ký làm đại lý',
        subTitle: 'Đăng ký hợp tác làm đại lý với Azooo',
        icon: <Image source={images.DAILY_ICON} style={styles.dailyIcon} />,
        onPress: () => {
          navigation.navigate('RegisterAgencyScreen');
        },
      },
      {
        key: '3',
        title: 'Trung tâm trợ giúp',
        subTitle: 'Câu hỏi thường gặp tại Azooo',
        icon: <Image source={images.TROGIUP} style={styles.trogiupIcon} />,
        onPress: () => {
          navigation.navigate('HelpCenterScreen');
        },
      },
    ];
    return (
      <View style={{flex: 1}}>
        <Header
          title="Liên hệ với chúng tôi"
          rightIcon={
            <Image source={images.NOTIFICATION} style={styles.rightIcon} />
          }
          onRightFunc={() =>
            this.props.navigation.navigate('NotificationScreen')
          }
        />
        <View style={styles.contentView}>
          <View style={styles.wrapperList}>
            <FlatList
              keyboardShouldPersistTaps="always"
              keyboardDismissMode="on-drag"
              data={sections}
              renderItem={({item}) => this._renderProgram(item)}
              extraData={sections}
              keyExtractor={item => item.key.toString()}
              removeClippedSubviews={false}
              initialNumToRender={8}
              maxToRenderPerBatch={2}
              ListHeaderComponent={this.renderHeader()}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default HelpScreen;
