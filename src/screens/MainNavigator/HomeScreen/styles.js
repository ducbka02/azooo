import {
    Dimensions,
    StyleSheet,
} from 'react-native';
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const DEVICE_WIDTH = Dimensions.get(`window`).width;

const styles = StyleSheet.create({
    container: {
    },
    rightIcon: {
        width: scale(18),
        height: scale(16.4),
        tintColor: 'white'
    },
    wrapperBanner: {
        width: '100%',
        height: DEVICE_WIDTH * 0.29,
    },
    gridView: {
        width: '100%',
        height: DEVICE_WIDTH * 0.5 + scale(10),
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: theme.blackSuperLightColor,
        flex: 1,
    },
    gridCollectView: {
        width: '100%',
        height: DEVICE_WIDTH * 0.5 + scale(4),
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: theme.blackSuperLightColor,
        flex: 1,
    },
    itemContainer: {
        alignItems: 'center',
        justifyContent: 'space-around',
        width: DEVICE_WIDTH * 0.25,
        height: DEVICE_WIDTH * 0.25 + scale(5),
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderColor: theme.blackSuperLightColor,
        flex: 1,
    },
    itemCollectContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: DEVICE_WIDTH * 0.25,
        height: DEVICE_WIDTH * 0.25 + scale(2),
        borderTopWidth: StyleSheet.hairlineWidth,
        borderLeftWidth: StyleSheet.hairlineWidth,
        borderColor: theme.blackSuperLightColor,
    },
    itemImage: {
        width: scale(35),
        height: scale(35)
    },
    itemName: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '600',
    },
    itemCode: {
        fontSize: scale(8.5),
        color: theme.blackIconColor,
        textAlign: 'center',
        fontWeight: '800'
    },
    stepView: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: theme.blackSuperLightColor,
        paddingVertical: scale(10),
        marginLeft: scale(5),
        marginRight: scale(5),
    },
    stepView1: {
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: theme.blackSuperLightColor,
        paddingVertical: scale(10),
    },
    buttonWrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: scale(15),
    },
    buttonContainer: {
        marginBottom: scale(10),
        height: scale(30),
        width: scale(150),
        borderRadius: scale(30),
        borderWidth: 1,
        borderColor: theme.pinkishColor,
        backgroundColor: theme.pinkishColor,
    },
    buttonTextStyle: {
        color: theme.pureWhite,
        fontSize: scale(10),
        lineHeight: scale(18),
        fontWeight: '600',
    },
    iconLeftStyle: {
        width: scale(18),
        height: scale(18)
    },
    iconStepImage: {
        width: scale(18),
        height: scale(18)
    },
    stepTextView: {
        paddingLeft: scale(5),
        paddingRight: scale(10),
    },
    stepText: {
        color: theme.blackIconColor,
        fontSize: scale(10),
        fontWeight: 'bold'
    },
    stepSubText: {
        color: theme.blackLightColor,
        fontSize: scale(9),
    },
    stepViewAll: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        //alignItems: 'center'
    },
    stepViewAll1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: scale(5),
    },
    showAllText: {
        color: theme.pinkishColor,
        fontSize: scale(10),
        lineHeight: scale(18),
        textAlign: 'center',
        marginTop: scale(5),
    },
    productWrapperView: {
        width: 100,
        height: 100,
    },
    brandWrapper: {
        width: DEVICE_WIDTH * 0.5 - 12, 
        height: (DEVICE_WIDTH * 0.5 - 12) * 82 / 173, 
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: theme.blackIconColor,
        marginRight: scale(5),
    },
    promotionWrapper: {
        width: DEVICE_WIDTH * 0.93,
        height: DEVICE_WIDTH * 0.93 * 330 / 825,
        paddingRight: scale(5)
    },
    wrapperAboutView: {
        width: '100%',
        height: DEVICE_WIDTH * 226 / 561,
    },
    aboutView: {
        width: '100%',
        height: DEVICE_WIDTH * 226 / 561,
        flexDirection: 'row'
    },
    imageAboutView: {
        width: DEVICE_WIDTH * 0.4,
        height: '100%',
    },
    textAboutView: {
        width: DEVICE_WIDTH * 0.55,
        height: '100%',
        backgroundColor: theme.pinkishColor,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: scale(5),
        paddingVertical: scale(5)
    },
    textAbout: {
        fontSize: scale(10),
        color: 'white'
    },
    dkdlView: {
        width: '100%',
        height: '100%',
    },
    dangkyInfoView: {
        position: 'absolute',
        height: '100%',
        width: DEVICE_WIDTH * 0.5,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: scale(10),
    },
    buttonDkdlContainer: {
        marginTop: scale(2),
        height: scale(30),
        width: scale(100),
        borderRadius: scale(30),
        borderWidth: 1,
        borderColor: theme.pinkishColor,
        backgroundColor: 'white',
    },
    buttonDkdlTextStyle: {
        fontSize: scale(10),
        lineHeight: scale(18),
        fontWeight: 'bold',
    }
});

export default styles;