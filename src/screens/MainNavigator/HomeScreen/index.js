/* eslint-disable handle-callback-err */
/* eslint-disable eqeqeq */
import React from 'react';
import {
  View,
  Dimensions,
  Image,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
  Platform,
} from 'react-native';
import {compose, withState} from 'recompose';
import get from 'lodash/get';
import {connect} from 'react-redux';
import Snackbar from 'react-native-snackbar';
import SearchHeader from 'src/components/SearchHeader';
import Swiper from 'react-native-swiper';
import Button from 'src/components/Button';
import Text from 'src/components/Text';
import ProgressiveImage from 'src/components/ProgressiveImage';
import FlatGrid from 'src/components/FlatGridComponent';
import ProductComponent from 'src/components/ProductComponent';
import CollectionComponent from 'src/components/CollectionComponent';
import {scale} from 'src/utils/scallingHelper';
import styles from './styles';
import images from 'src/config/images';
import * as appActions from 'src/store/actions/app';
import NavigationService, {ProductShowType} from 'src/config/NavigationService';

import Permissions from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';

const DEVICE_WIDTH = Dimensions.get(`window`).width;

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.onNavigateToAgency = this.onNavigateToAgency.bind(this);
    this.renderCategoryItem = this.renderCategoryItem.bind(this);
    this.renderCollectionItem = this.renderCollectionItem.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  async componentDidMount() {
    if (Platform.OS === 'android') {
      this.checkLocationPermission();
    }
  }

  requestLocationPermission() {
    Permissions.request('location').then(response => {
      if (response == 'authorized') {
        if (Platform.OS === 'android') {
          RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
            interval: 10000,
            fastInterval: 5000,
          })
            .then(data => {})
            .catch(err => {});
        }
      }
    });
  }

  checkLocationPermission() {
    Permissions.check('location').then(response => {
      if (response != 'authorized') {
        this.requestLocationPermission();
      }
      if (Platform.OS === 'android') {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
          interval: 10000,
          fastInterval: 5000,
        })
          .then(data => {})
          .catch(err => {});
      }
    });
  }

  onNavigateToAgency(data) {
    NavigationService.navigate('Agency', {data: data});
  }

  renderCollectionItem({item, index}) {
    const {field_image} = item;
    return (
      <TouchableOpacity
        key={index}
        style={styles.itemCollectContainer}
        onPress={() => {
          NavigationService.goToCategoryNavigator({
            type: ProductShowType.COLLECTION,
            id: item.id,
          });
        }}>
        <ProgressiveImage
          thumbnailSource={images.DEFAULT}
          style={{width: DEVICE_WIDTH * 0.25, height: DEVICE_WIDTH * 0.25}}
          source={
            field_image.length === 0 ? images.DEFAULT : {uri: field_image}
          }
        />
      </TouchableOpacity>
    );
  }

  renderCategoryItem({item, index}) {
    const {category_name, field_image, category_id} = item;
    return (
      <TouchableOpacity
        key={`${category_id}-${index}`}
        style={styles.itemContainer}
        onPress={() => {
          NavigationService.goToCategoryNavigator({
            type: ProductShowType.CATEGORY,
            id: category_id,
          });
        }}>
        <View style={{flex: 0.7, justifyContent: 'center'}}>
          <Image
            source={{uri: field_image}}
            style={styles.itemImage}
            resizeMode="contain"
          />
        </View>
        <View style={{flex: 0.3, paddingHorizontal: 5}}>
          <Text nOfLines={2} style={styles.itemCode}>
            {category_name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  onRefresh() {
    const {setRefreshing, getAppInfo} = this.props;
    setRefreshing(true);
    getAppInfo(() => {
      setRefreshing(false);
    });
  }

  render() {
    const {
      listBanner,
      listCategoryProduct,
      listBestSelling,
      listNewProduct,
      listCollection,
      listBrand,
      listPromotion,
      refreshing,
    } = this.props;

    return (
      <View style={{flex: 1}}>
        <SearchHeader
          rightIcon={
            <Image
              resizeMode="contain"
              source={images.HDMH_ICON}
              style={styles.rightIcon}
            />
          }
          onRightFunc={() =>
            Snackbar.show({
              title: 'Chức năng đang phát triển',
              duration: Snackbar.LENGTH_SHORT,
            })
          }
          onFocus={() =>
            NavigationService.navigate('SearchScreen', {type: 'HOME'})
          }
        />
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.onRefresh}
            />
          }>
          <View style={styles.wrapperBanner}>
            <Swiper
              autoplayTimeout={5.0}
              showsPagination={false}
              autoplay={true}>
              {listBanner.map((banner, index) => {
                const {nid, field_image, field_image_web} = banner;
                return (
                  <View style={styles.wrapperBanner} key={`${nid}-${index}`}>
                    <ProgressiveImage
                      thumbnailSource={{
                        uri: `https://images.pexels.com/photos/671557/pexels-photo-671557.jpeg?w=50&buster=${Math.random()}`,
                      }}
                      style={{width: '100%', height: '100%'}}
                      source={{
                        uri: field_image === '' ? field_image_web : field_image,
                      }}
                    />
                  </View>
                );
              })}
            </Swiper>
          </View>
          <View style={styles.stepView}>
            <View style={styles.stepViewAll}>
              <Button
                disabled
                leftIcon={
                  <Image
                    source={images.BADGESTARNORMAL}
                    resizeMode="contain"
                    style={{height: '100%'}}
                  />
                }
                iconLeftStyle={styles.iconLeftStyle}
                title={'SẢN PHẨM BÁN CHẠY'}
                titleStyle={styles.buttonTextStyle}
                styleContainer={styles.buttonContainer}
              />
              <Text
                style={styles.showAllText}
                onPress={() => {
                  NavigationService.goToCategoryNavigator({
                    type: ProductShowType.PRODUCT_LIST,
                    id: listBestSelling,
                    index: 1,
                  });
                }}>
                Xem tất cả
              </Text>
            </View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {listBestSelling.map((item, index) => {
                const {
                  product_id,
                  title,
                  field_image,
                  category_name,
                  price__number,
                  promotion_price,
                  field_thumbnail,
                } = item;
                return (
                  <ProductComponent
                    key={`${product_id}-${index}`}
                    title={title}
                    field_image={field_image}
                    category_name={category_name}
                    price__number={price__number}
                    promotion_price={promotion_price}
                    field_thumbnail={field_thumbnail}
                    onPress={() => {
                      NavigationService.navigate('ProductDetailScreen', {
                        data: item,
                        onNavigateToAgency: this.onNavigateToAgency,
                      });
                    }}
                  />
                );
              })}
            </ScrollView>
          </View>
          <View style={styles.stepView1}>
            <View style={styles.stepViewAll1}>
              <Button
                disabled
                leftIcon={
                  <Image
                    source={images.COLLECTION}
                    resizeMode="contain"
                    style={{height: '100%'}}
                  />
                }
                iconLeftStyle={styles.iconLeftStyle}
                title={'DANH MỤC SẢN PHẨM'}
                titleStyle={styles.buttonTextStyle}
                styleContainer={styles.buttonContainer}
              />
            </View>
            <View style={styles.gridView}>
              <FlatGrid
                horizontal
                itemDimension={DEVICE_WIDTH * 0.25}
                scrollEnabled={listCategoryProduct.length > 8}
                items={listCategoryProduct}
                spacing={0}
                renderItem={this.renderCategoryItem}
              />
            </View>
          </View>
          <View style={styles.stepView}>
            <View>
              <Button
                disabled
                leftIcon={
                  <Image
                    source={images.KHUYENMAI}
                    resizeMode="contain"
                    style={{height: '100%'}}
                  />
                }
                iconLeftStyle={styles.iconLeftStyle}
                title={'KHUYẾN MẠI LỚN'}
                titleStyle={styles.buttonTextStyle}
                styleContainer={styles.buttonContainer}
              />
            </View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {listPromotion.map(item => {
                const {id, field_image} = item;
                return (
                  <TouchableOpacity
                    style={styles.promotionWrapper}
                    key={id}
                    onPress={() => {
                      NavigationService.goToCategoryNavigator({
                        type: ProductShowType.SALE,
                        id: id,
                      });
                    }}>
                    <ProgressiveImage
                      thumbnailSource={{
                        uri: `https://images.pexels.com/photos/671557/pexels-photo-671557.jpeg?w=50&buster=${Math.random()}`,
                      }}
                      style={{width: '100%', height: '100%'}}
                      source={{
                        uri: field_image,
                      }}
                    />
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
          <View style={styles.stepView}>
            <View style={styles.stepViewAll}>
              <Button
                disabled
                leftIcon={
                  <Image
                    source={images.STORE_NEW_BADGES}
                    resizeMode="contain"
                    style={{height: '100%'}}
                  />
                }
                iconLeftStyle={styles.iconLeftStyle}
                title={'SẢN PHẨM MỚI'}
                titleStyle={styles.buttonTextStyle}
                styleContainer={styles.buttonContainer}
              />
              <Text
                style={styles.showAllText}
                onPress={() => {
                  NavigationService.goToCategoryNavigator({
                    type: ProductShowType.PRODUCT_LIST,
                    id: listNewProduct,
                    index: 2,
                  });
                }}>
                Xem tất cả
              </Text>
            </View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {listNewProduct.map((item, index) => {
                const {
                  product_id,
                  title,
                  field_images,
                  category_name,
                  price__number,
                  promotion_price,
                  field_thumbnail,
                } = item;
                return (
                  <ProductComponent
                    key={`${product_id}-${index}`}
                    title={title}
                    field_image={field_images}
                    category_name={category_name}
                    price__number={price__number}
                    promotion_price={promotion_price}
                    field_thumbnail={field_thumbnail}
                    onPress={() => {
                      NavigationService.navigate('ProductDetailScreen', {
                        data: item,
                        onNavigateToAgency: this.onNavigateToAgency,
                      });
                    }}
                  />
                );
              })}
            </ScrollView>
          </View>
          <View style={styles.stepView1}>
            <View style={styles.stepViewAll1}>
              <Button
                disabled
                leftIcon={
                  <Image
                    source={images.COLLECTION}
                    resizeMode="contain"
                    style={{height: '100%'}}
                  />
                }
                iconLeftStyle={styles.iconLeftStyle}
                title={'BỘ SƯU TẬP'}
                titleStyle={styles.buttonTextStyle}
                styleContainer={styles.buttonContainer}
              />
              <Text
                style={styles.showAllText}
                onPress={() => {
                  NavigationService.navigate('CollectionListScreen', {
                    listCollection,
                  });
                }}>
                Xem tất cả
              </Text>
            </View>
            <View style={styles.gridCollectView}>
              <FlatGrid
                horizontal
                itemDimension={DEVICE_WIDTH * 0.25}
                scrollEnabled={listCollection.length > 8}
                items={listCollection}
                spacing={0}
                renderItem={this.renderCollectionItem}
              />
            </View>
          </View>
          <View style={styles.stepView}>
            <View>
              <Button
                disabled
                leftIcon={
                  <Image
                    source={images.INTERNATIONAL}
                    resizeMode="contain"
                    style={{height: '100%'}}
                  />
                }
                iconLeftStyle={styles.iconLeftStyle}
                title={'CÁC THƯƠNG HIỆU'}
                titleStyle={styles.buttonTextStyle}
                styleContainer={styles.buttonContainer}
              />
            </View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {listBrand.map(item => {
                const {brand_id, field_image} = item;
                return (
                  <TouchableOpacity
                    style={styles.brandWrapper}
                    key={brand_id}
                    onPress={() => {
                      NavigationService.goToCategoryNavigator({
                        type: ProductShowType.BRAND,
                        id: brand_id,
                      });
                    }}>
                    <Image
                      resizeMethod="scale"
                      resizeMode="stretch"
                      style={{width: '100%', height: '100%'}}
                      source={{
                        uri: field_image,
                      }}
                    />
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
          <View style={styles.stepView}>
            <Button
              disabled
              leftIcon={
                <Image
                  source={images.INTERNATIONAL}
                  resizeMode="contain"
                  style={{height: '100%'}}
                />
              }
              iconLeftStyle={styles.iconLeftStyle}
              title={'VỀ AZOOO'}
              titleStyle={styles.buttonTextStyle}
              styleContainer={styles.buttonContainer}
            />
            <View style={styles.wrapperAboutView}>
              <Swiper
                autoplayTimeout={5.0}
                showsPagination={false}
                autoplay={true}>
                <TouchableOpacity style={styles.aboutView} activeOpacity={1}>
                  <View style={styles.imageAboutView}>
                    <Image
                      source={images.AZOOO_ABOUT}
                      style={{height: '100%', width: '100%'}}
                    />
                  </View>
                  <View style={styles.textAboutView}>
                    <Text numberOfLines={5} style={styles.textAbout}>
                      "Azooo chỉ cung cấp các sản phẩm có thương hiệu, có uy tín
                      và được kiểm định chất lượng chặt chẽ"
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.aboutView} activeOpacity={1}>
                  <View style={styles.dkdlView}>
                    <Image
                      source={images.DKDL_ICON}
                      style={{height: '100%', width: '100%'}}
                    />
                    <View style={styles.dangkyInfoView}>
                      <Text numberOfLines={2} style={styles.textAbout}>
                        Bạn muốn trở thành đại lý?
                      </Text>
                      <Button
                        onPress={() =>
                          this.props.navigation.navigate('RegisterAgencyScreen')
                        }
                        title={'ĐĂNG KÝ NGAY'}
                        titleStyle={styles.buttonDkdlTextStyle}
                        styleContainer={styles.buttonDkdlContainer}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </Swiper>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default compose(
  withState('refreshing', 'setRefreshing', false),
  connect(
    state => ({
      listBestSelling: get(state, 'app.listBestSelling', []),
      listNewProduct: get(state, 'app.listNewProduct', []),
      listBanner: get(state, 'app.listBanner', []),
      listCategoryProduct: get(state, 'app.listCategoryProduct', []),
      listCollection: get(state, 'app.listCollection', []),
      listBrand: get(state, 'app.listBrand', []),
      listPromotion: get(state, 'app.listPromotion', []),
    }),
    {
      getLocation: appActions.getLocation,
      getAppInfo: appActions.getAppInfo,
    },
  ),
)(HomeScreen);
