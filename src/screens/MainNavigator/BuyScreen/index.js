import React from 'react';
import {View, Image, FlatList, Text, RefreshControl} from 'react-native';
import {compose, withState} from 'recompose';
import get from 'lodash/get';
import {connect} from 'react-redux';
import Header from 'src/components/Header';
import SearchHeader from 'src/components/SearchHeader';
import ProgressiveImage from 'src/components/ProgressiveImage';
import Placeholder, {Line} from 'src/components/PlaceholderAnimation';
import ProductComponent from 'src/components/ProductComponent';
import {scale} from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
import Spinner from 'react-native-spinkit';
import images from 'src/config/images';
import {getListProductAvailable} from 'src/services/api/app';
import NavigationService, {ProductShowType} from 'src/config/NavigationService';
import styles from './styles';

const dataPlaceholder = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const formatData = (data, numColumns = 2) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
  while (
    numberOfElementsLastRow !== numColumns &&
    numberOfElementsLastRow !== 0
  ) {
    data.push({product_id: `blank-${numberOfElementsLastRow}`, empty: true});
    numberOfElementsLastRow++;
  }

  return data;
};

class BuyScreen extends React.Component {
  static navigationOptions = {
    tabBarVisible: true,
  };

  constructor(props) {
    super(props);
    this.pageIndex = 0;
    this._renderProgram = this._renderProgram.bind(this);
    this.onNavigateToAgency = this.onNavigateToAgency.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.handleEnd = this.handleEnd.bind(this);
  }

  async componentDidMount() {
    this.getProductRegion(20, 0);
  }

  async getProductRegion(limit, offset) {
    const {setLoading, region, setListProducts} = this.props;
    const list = await getListProductAvailable(
      region.latitude,
      region.longitude,
      limit,
      offset,
    );
    console.log(list);
    setLoading(false);
    setListProducts(list);
  }

  async onRefresh() {
    const {setIsRefreshing, region, setListProducts} = this.props;
    setIsRefreshing(true);
    this.pageIndex = 0;
    const list = await getListProductAvailable(
      region.latitude,
      region.longitude,
      20,
      0,
    );
    setIsRefreshing(false);
    setListProducts(list);
  }

  onNavigateToAgency(data) {
    NavigationService.navigate('Agency', {data: data});
  }

  _renderProgram({item, index}) {
    const {navigation} = this.props;
    if (item.empty === true) {
      return <View style={styles.itemInvisible} />;
    }
    const {
      product_id,
      title,
      field_image,
      field_images,
      category_name,
      price__number,
      promotion_price,
      field_thumbnail,
    } = item;
    return (
      <ProductComponent
        key={`${product_id}_${index}`}
        title={title}
        field_image={field_image || field_images}
        category_name={category_name}
        price__number={price__number}
        promotion_price={promotion_price}
        field_thumbnail={field_thumbnail}
        onPress={() => {
          navigation.push('ProductDetailScreen', {
            data: item,
            onNavigateToAgency: this.onNavigateToAgency,
          });
        }}
      />
    );
  }

  renderFooter = () => {
    if (this.props.isLoadMore) {
      return (
        <View
          style={{
            paddingVertical: scale(10),
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Spinner
            style={{width: 25}}
            size={25}
            type="Wave"
            color={theme.pinkishColor}
          />
        </View>
      );
    }
    return null;
  };

  async handleEnd() {
    const {
      isLoadMore,
      setIsLoadMore,
      region,
      setListProducts,
      listProducts,
    } = this.props;
    if (isLoadMore) return;
    this.props.setIsLoadMore(true, async () => {
      this.pageIndex++;
      const list = await getListProductAvailable(
        region.latitude,
        region.longitude,
        20,
        this.pageIndex * 20 + 1,
      );
      const newList = [...listProducts, ...list];
      setListProducts(newList);
      setIsLoadMore(false);
    });
  }

  renderContent() {
    const {loading, listProducts, isRefreshing} = this.props;
    if (loading) {
      return (
        <FlatList
          showsVerticalScrollIndicator={false}
          style={styles.container}
          numColumns={2}
          data={dataPlaceholder}
          renderItem={() => (
            <Placeholder
              animation="fade"
              isReady={!loading}
              whenReadyRender={() => null}>
              <View style={styles.placeholderView}>
                <View style={styles.placeholderContent}>
                  <Image
                    resizeMode="contain"
                    source={images.IMG_PLACEHOLDER}
                    style={{
                      tintColor: theme.blackSuperLightColor,
                    }}
                  />
                  <Line
                    width="80%"
                    style={[
                      styles.linePlaceholderStyle,
                      {marginTop: scale(15)},
                    ]}
                  />
                </View>
              </View>
            </Placeholder>
          )}
          keyExtractor={item => item.toString()}
        />
      );
    }

    if (listProducts.length === 0) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingTop: scale(100),
            paddingHorizontal: scale(20),
          }}>
          <Image source={images.EMPTY_PRODUCT} />
          <Text
            style={{
              color: theme.blackIconColor,
              fontSize: scale(10),
              textAlign: 'center',
            }}>
            Chưa có sản phẩm nào trong danh mục này
          </Text>
        </View>
      );
    }

    return (
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
        ListFooterComponent={this.renderFooter}
        onEndReached={this.handleEnd}
        onEndReachedThreshold={0.5}
        style={styles.container}
        numColumns={2}
        data={formatData(listProducts)}
        renderItem={this._renderProgram}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        extraData={this.props}
        keyExtractor={item => item.product_id.toString()}
        //removeClippedSubviews={false}
        //getItemLayout={(data, index) => ({ length: scale(130), offset: scale(130) * index, index })}
        initialNumToRender={8}
        //maxToRenderPerBatch={2}
      />
    );
  }

  render() {
    const {loading} = this.props;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: loading ? 'while' : theme.placeholderColor,
        }}>
        {/* <Header
          title="Mua hàng"
        /> */}
        <SearchHeader
          //rightIcon={<Image resizeMode='contain' source={images.HDMH_ICON} style={styles.rightIcon} />}
          // onRightFunc={() => Snackbar.show({
          //   title: 'Chức năng đang phát triển',
          //   duration: Snackbar.LENGTH_SHORT,
          // })}
          onFocus={() =>
            NavigationService.navigate('SearchScreen', {type: 'HOME'})
          }
        />
        {this.renderContent()}
      </View>
    );
  }
}

export default compose(
  withState('loading', 'setLoading', true),
  withState('isRefreshing', 'setIsRefreshing', false),
  withState('isLoadMore', 'setIsLoadMore', false),
  withState('listProducts', 'setListProducts', []),
  connect(
    state => ({
      region: get(state, 'app.region', {}),
    }),
    {},
  ),
)(BuyScreen);
