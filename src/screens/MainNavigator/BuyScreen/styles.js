import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const width = Dimensions.get('window').width;

export default StyleSheet.create({
    wrapper: {

    },
    rightIcon: {
        width: scale(15),
        height: scale(20),
    },
    container: {
        flex: 1,
        marginHorizontal: scale(5),
        marginVertical: scale(5),
    },
    placeholderView: {
        width: (width - scale(5) * 2) / 2,
        height: scale(115),
        padding: scale(2),
        justifyContent: 'center',
        alignItems: 'center',
    },
    placeholderContent: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: theme.placeholderColor,
        alignItems: 'center',
        padding: scale(5)
    },
    linePlaceholderStyle: {
        backgroundColor: theme.blackSuperLightColor,
        alignSelf: 'center',
    },
    separator: {
        height: scale(5)
    }
});
