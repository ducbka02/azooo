import React from 'react';
import { View, Image, ScrollView, TouchableOpacity, Dimensions, InteractionManager } from 'react-native';
import { compose, withState } from 'recompose';
import get from 'lodash/get';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';
import Header from 'src/components/Header';
import ProgressiveImage from 'src/components/ProgressiveImage';
import Placeholder, { Line, Media } from 'src/components/PlaceholderAnimation';
import ProductComponent from 'src/components/ProductComponent';
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
import Button from 'src/components/Button';
import images from 'src/config/images';
import NavigationService, { ProductShowType } from 'src/config/NavigationService';
import styles from './styles';
import { getListProductInSale } from 'src/services/api/app';

class SaleScreen extends React.Component {
  static navigationOptions = {
    tabBarVisible: true,
  };

  constructor(props) {
    super(props);

    this.onNavigateToAgency = this.onNavigateToAgency.bind(this);
  }

  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async componentDidMount() {
    const { listPromotion, setListProductInPromotion, setLoading } = this.props;
    console.log(listPromotion);
    let listFinal = [];
    const promises = listPromotion.map(async (promotion, idx) => {
      const id = promotion.id;
      const listProduct = await getListProductInSale(promotion.id, false);
      listFinal.push(...listProduct);
    });
    await Promise.all(promises);
    setLoading(false);
    //console.warn(listFinal);
    setListProductInPromotion(listFinal);
  }

  onNavigateToAgency(data) {
    NavigationService.navigate('Agency', { data: data });
  }

  renderContent() {
    const { loading, listPromotion, listProductInPromotion } = this.props;
    if (loading) {
      return (
        <View style={{ flex: 1 }}>
          <View style={{ paddingHorizontal: scale(5), paddingVertical: scale(5) }}>
            <Placeholder
              isReady={false}
              animation="fade"
              renderLeft={() => <Media size={scale(80)} />}
            >
              <Line width="70%" />
              <Line />
              <Line />
              <Line width="30%" />
            </Placeholder>
          </View>
          <View style={{ paddingHorizontal: scale(5), paddingVertical: scale(5) }}>
            <Placeholder
              isReady={false}
              animation="fade"
              renderLeft={() => <Media size={scale(80)} />}
            >
              <Line width="70%" />
              <Line />
              <Line />
              <Line width="30%" />
            </Placeholder>
          </View>
          <View style={{ paddingHorizontal: scale(5), paddingVertical: scale(5) }}>
            <Placeholder
              isReady={false}
              animation="fade"
              renderLeft={() => <Media size={scale(80)} />}
            >
              <Line width="70%" />
              <Line />
              <Line />
              <Line width="30%" />
            </Placeholder>
            <View style={{ paddingHorizontal: scale(5), paddingVertical: scale(5) }}>
              <Placeholder
                isReady={false}
                animation="fade"
                renderLeft={() => <Media size={scale(80)} />}
              >
                <Line width="70%" />
                <Line />
                <Line />
                <Line width="30%" />
              </Placeholder>
            </View>
            <View style={{ paddingHorizontal: scale(5), paddingVertical: scale(5) }}>
              <Placeholder
                isReady={false}
                animation="fade"
                renderLeft={() => <Media size={scale(80)} />}
              >
                <Line width="70%" />
                <Line />
                <Line />
                <Line width="30%" />
              </Placeholder>
            </View>
          </View>
        </View>)
    }

    return (
      <View style={styles.stepView}>
        <View style={{ marginLeft: scale(5) }}>
          <Button
            disabled
            leftIcon={<Image source={images.KHUYENMAI} resizeMode='contain' style={{ height: '100%' }} />}
            iconLeftStyle={styles.iconLeftStyle}
            title={'ĐANG KHUYẾN MẠI'}
            titleStyle={styles.buttonTextStyle}
            styleContainer={styles.buttonContainer}
          />
        </View>
        <View>
          <View style={styles.wrapperSaleListView}>
            <ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
            >
              {listProductInPromotion.map(item => {
                const { product_id, title, field_image, category_name, price__number, promotion_price, field_thumbnail } = item;
                return (
                  <ProductComponent
                    inSale
                    key={product_id}
                    title={title}
                    field_image={field_image}
                    category_name={category_name}
                    price__number={price__number}
                    promotion_price={promotion_price}
                    field_thumbnail={field_thumbnail}
                    onPress={() => {
                      NavigationService.navigate('ProductDetailScreen', { data: item, onNavigateToAgency: this.onNavigateToAgency });
                    }}
                  />
                );
              })}
            </ScrollView>
          </View>
        </View>
      </View>
    )
  }

  render() {
    const { listBanner, loading } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <Header
          title="Có gì hot hôm nay?"
          rightIcon={<Image source={images.NOTIFICATION} style={styles.rightIcon} />}
          onRightFunc={() => this.props.navigation.navigate('NotificationScreen')}
        />
        <ScrollView>
          <View style={styles.wrapperBanner}>
            <Swiper
              autoplayTimeout={5.0}
              showsPagination={false}
              autoplay={true}
            >
              {listBanner.map(banner => {
                const { title, nid, field_image, field_image_web } = banner;
                return (
                  <View style={styles.wrapperBanner} key={nid}>
                    <ProgressiveImage
                      thumbnailSource={{ uri: `https://images.pexels.com/photos/671557/pexels-photo-671557.jpeg?w=50&buster=${Math.random()}` }}
                      style={{ width: "100%", height: "100%", }}
                      source={{
                        uri: field_image === "" ? field_image_web : field_image
                      }}
                    />
                  </View>
                );
              })}
            </Swiper>
          </View>
          {this.renderContent()}
          {/* <View style={styles.stepView}>
            <View style={{ marginLeft: scale(5) }}>
              <Button
                disabled
                leftIcon={<Image source={images.KHUYENMAI} resizeMode='contain' style={{ height: '100%' }} />}
                iconLeftStyle={styles.iconLeftStyle}
                title={'QUÀ TẶNG MAY MẮN'}
                titleStyle={styles.buttonTextStyle}
                styleContainer={styles.buttonContainer}
              />
            </View>
          </View> */}
        </ScrollView>
      </View>
    );
  }
}

export default compose(
  withState("loading", "setLoading", true),
  withState("listProductInPromotion", "setListProductInPromotion", []),
  connect(
    (state) => ({
      listBanner: get(state, 'app.listBanner', []),
      listPromotion: get(state, 'app.listPromotion', []),
    }),
    {

    }
  )
)(SaleScreen);
