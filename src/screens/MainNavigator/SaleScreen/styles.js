import { StyleSheet, Dimensions } from "react-native";
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const DEVICE_WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
    wrapper: {

    },
    rightIcon: {
        width: scale(15),
        height: scale(20),
    },
    wrapperBanner: {
        width: '100%',
        height: DEVICE_WIDTH * 0.29,
    },
    wrapperPromotionBanner: {
        width: DEVICE_WIDTH,
        height: DEVICE_WIDTH * 330 / 825,
    },
    stepView: {
        //borderBottomWidth: StyleSheet.hairlineWidth,
        //borderColor: theme.blackSuperLightColor,
        paddingTop: scale(10)
    },
    buttonWrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: scale(15),
    },
    buttonContainer: {
        marginBottom: scale(10),
        height: scale(30),
        width: scale(140),
        borderRadius: scale(30),
        borderWidth: 1,
        borderColor: theme.pinkishColor,
        backgroundColor: theme.pinkishColor,
    },
    buttonTextStyle: {
        color: theme.pureWhite,
        fontSize: scale(10),
        lineHeight: scale(18),
        fontWeight: '600',
    },
    wrapperSaleListView: {
        //backgroundColor: theme.placeholderColor,
        paddingVertical: scale(5),
        paddingHorizontal: scale(5),
        alignItems: 'center',
        justifyContent: 'center'
    }
});
