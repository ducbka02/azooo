/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import {Platform, Image} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
//import createNativeStackNavigator from 'react-native-screens/createNativeStackNavigator';
import HomeScreen from './HomeScreen';
import HelpScreen from './HelpScreen';
import SaleScreen from './SaleScreen';
import BuyScreen from './BuyScreen';

import images from 'src/config/images';
import theme from 'src/config/theme';
import fonts from 'src/config/fonts';
import {scale} from 'src/utils/scallingHelper';
import {isIpad} from 'src/utils/iphoneXhelper';

const HomeStack = createStackNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
    },
  },
  {
    initialRouteName: 'HomeScreen',
    headerMode: 'none',
    navigationOptions: ({navigation}) => {
      const route = navigation.state.routes[navigation.state.index].routeName;
      const extraConfig = {
        tabBarVisible: true,
      };
      if (route !== 'HomeScreen') {
        // if it is not InputReadingEntry then set tabbar to false
        extraConfig.tabBarVisible = false;
      }
      return {
        ...extraConfig,
        gesturesEnabled: false,
      };
    },
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
  },
);

export default createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: () => ({
        tabBarLabel: 'Home',
        tabBarIcon: ({tintColor}) => (
          <Image
            source={images.TAB_HIGHTLIGHT_HOME}
            style={{
              marginRight: isIpad() ? scale(10) : 0,
              tintColor: tintColor,
              width: scale(18),
              height: scale(18),
            }}
          />
        ),
        tabBarOnPress: ({navigation, defaultHandler}) => {
          const currentRoute =
            navigation.state.routes[navigation.state.index].routeName;
          if (currentRoute === 'ConfirmGoal' || currentRoute === 'FinishGoal') {
          } else {
            defaultHandler();
          }
        },
      }),
    },
    Buy: {
      screen: BuyScreen,
      navigationOptions: () => ({
        tabBarLabel: 'Mua hàng',
        tabBarIcon: ({tintColor}) => (
          <Image
            source={images.TAB_HIGHTLIGHT_AGENCY}
            style={{
              marginRight: isIpad() ? scale(10) : 0,
              tintColor: tintColor,
              width: scale(18),
              height: scale(18),
            }}
          />
        ),
      }),
    },
    Sale: {
      screen: SaleScreen,
      navigationOptions: () => ({
        tabBarLabel: 'Khuyến mại',
        tabBarIcon: ({tintColor}) => (
          <Image
            source={images.TAB_HIGHTLIGHT_SALE}
            style={{
              marginRight: isIpad() ? scale(10) : 0,
              tintColor: tintColor,
              width: scale(18),
              height: scale(21.5),
            }}
          />
        ),
      }),
    },
    Help: {
      screen: HelpScreen,
      navigationOptions: () => ({
        tabBarLabel: 'Trợ giúp',
        tabBarIcon: ({tintColor}) => (
          <Image
            source={images.TAB_HIGHTLIGHT_HELP}
            style={{
              marginRight: isIpad() ? scale(10) : 0,
              tintColor: tintColor,
              width: scale(18),
              height: scale(18),
            }}
          />
        ),
      }),
    },
  },
  {
    initialRouteName: 'Home',
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
    lazy: true,
    tabBarOptions: {
      activeTintColor: theme.pinkishColor,
      inactiveTintColor: theme.blackIconColor,
      style: {
        height: scale(58),
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: theme.pureWhite,
        //borderTopWidth: 1,
        ...Platform.select({
          ios: {
            shadowRadius: 2,
            shadowColor: 'rgba(0, 0, 0, 1.0)',
            shadowOpacity: 0.1,
            shadowOffset: {width: 0, height: 2},
          },
          android: {
            elevation: 5,
          },
        }),
      },
      showIcon: true,
      showLabel: true,
      labelStyle: {
        fontFamily: fonts.ROBOTO_REGULAR,
        fontSize: scale(10),
      },
    },
  },
);
