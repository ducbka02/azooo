import React from 'react';
import {
  View,
  StyleSheet,
  InteractionManager,
  Dimensions,
  Animated,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid
} from 'react-native';
import { withNavigationFocus } from "react-navigation";
import RNSettings from 'react-native-settings';
import Snackbar from 'react-native-snackbar';
import { compose, withState } from 'recompose';
import get from 'lodash/get';
import { connect } from 'react-redux';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Carousel from 'react-native-snap-carousel';
import SearchHeader from 'src/components/SearchHeader';
import ProductComponent from 'src/components/ProductComponent';
import ProductPlaceholder from 'src/components/ProductPlaceholder';
import Text from 'src/components/Text';
import styles from './styles';
import images from 'src/config/images';
import { scale } from 'src/utils/scallingHelper';
import NavigationService from 'src/config/NavigationService';
import { FacebookAgencyPage } from 'src/utils/linkingHelper';
import {
  getListAgency,
  azSearchKeyword
} from 'src/services/api/app';

const AnimatedView = Animated.createAnimatedComponent(View);
const { width, height } = Dimensions.get("window");

function wp(percentage) {
  const value = (percentage * width) / 100;
  return Math.round(value);
}

const slideWidth = wp(85);

export const sliderWidth = width;
export const itemWidth = slideWidth;


const ASPECT_RATIO = width / height;
const LATITUDE = 21.055121;
const LONGITUDE = 105.808466;
const LATITUDE_DELTA = 0.3;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const containerHeight = scale(120);
class AgencyScreen extends React.Component {
  static navigationOptions = {
    tabBarVisible: false,
  };
  constructor(props) {
    super(props);
    this.state = {
      paddingTop: -1
    }

    this.horizontalAnimated = new Animated.Value(1);
    this._renderItemWithParallax = this._renderItemWithParallax.bind(this);
    this.goBackFromSearchScreen = this.goBackFromSearchScreen.bind(this);
    this.onNavigateToAgency = this.onNavigateToAgency.bind(this);
    this._onMapReady = this._onMapReady.bind(this);
  }

  _onMapReady() {
    if (Platform.OS === 'ios') return;
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
      .then(granted => {
        this.setState({ paddingTop: 0 });
      });
  }

  onNavigateToAgency(data) {
    NavigationService.navigate('Agency', { data: data });
  }

  async componentDidMount() {
    if (Platform.OS === 'ios') {
      navigator.geolocation.getCurrentPosition(
        position => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          };
          if (this.map) {
            //this.map.animateToRegion(region);
          }
          //this.props.setRegion(region);
          this.props.setLoading(false);
        },
        error => {
          this.props.setLoading(false);
          console.log("fail to get location");
        }
      );
    } else {
      RNSettings.getSetting(RNSettings.LOCATION_SETTING).then(result => {
        if (result == RNSettings.ENABLED) {
          navigator.geolocation.getCurrentPosition(
            position => {
              const region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
              };
              if (this.map) {
                this.map.animateToRegion(region);
              }
              this.props.setRegion(region);
              this.props.setLoading(false);
            },
            error => {
              this.props.setLoading(false);
              console.log("fail to get location");
            }
          );
        } else {
          RNSettings.openSetting(
            RNSettings.ACTION_LOCATION_SOURCE_SETTINGS
          ).then(result => {
            if (result === RNSettings.ENABLED) {
              navigator.geolocation.getCurrentPosition(
                position => {
                  const region = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA
                  };
                  if (this.map) {
                    this.map.animateToRegion(region);
                  }
                  this.props.setRegion(region);
                  this.props.setLoading(false);
                },
                error => {
                  this.props.setLoading(false);
                  console.log("fail to get location");
                }
              );
            } else {

            }
          })
        }
      })
    }
    const data = this.props.navigation.getParam('data', '');
    const { listBestSelling, setListProduct, setRegion, setLoading, setMarkers } = this.props;
    if (data !== '') {
      setListProduct([data.data]);
      const listAgency = await getListAgency(get(data, 'data.product_id', 361), 21.004584, 105.804393);
      setMarkers(listAgency);
      return;
    }

    setListProduct(listBestSelling);
    // if (listBestSelling.length > 0) {
    //   const listAgency = await getListAgency(get(listBestSelling[0], 'product_id', 361));
    //   setMarkers(listAgency);
    // }
  }

  async goBackFromSearchScreen(data) {
    const { setMarkers, setLoading, setListProduct } = this.props;
    if (typeof data === 'string') {
      setLoading(true);
      try {
        const listSearch = await azSearchKeyword(data);
        setListProduct(listSearch);
        if (listSearch.length > 0) {
          const listAgency = await getListAgency(get(data, 'data.product_id', 361), 21.004584, 105.804393);
          setMarkers(listAgency);
        }
        setLoading(false);
      } catch (error) {
        setLoading(false);
        setMarkers([]);
      }
    } else {
      setLoading(true);
      setListProduct([data]);
      const listAgency = await getListAgency(get(data, 'data.product_id', 361), 21.004584, 105.804393);
      setMarkers(listAgency);
      setLoading(false);
    }
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      if (this.props.isFocused) {
        const data = this.props.navigation.getParam('data', '');
        const { setListProduct, setMarkers } = this.props;
        if (data !== '') {
          setListProduct([data.data]);
          const listAgency = await getListAgency(get(data, 'data.product_id', 361), 21.004584, 105.804393);
          setMarkers(listAgency);
        }
      }
    }
  }

  _renderItemWithParallax({ item, index }) {
    const { first, product_id, title, field_image, category_name, price__number, promotion_price, field_thumbnail } = item;
    if (first) {
      return (
        <View style={styles.wrapperProduct}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.desText}>Click </Text>
            <Image style={{ width: 60, height: 60 }} source={images.SHOP_MARKER} />
            <Text style={styles.desText}> để chọn đại lý gần bạn</Text>
          </View>
        </View>
      )
    }
    return (
      <View style={styles.wrapperProduct}>
        <ProductComponent
          key={product_id}
          inMap
          title={title}
          field_image={field_image}
          category_name={category_name}
          price__number={price__number}
          promotion_price={promotion_price}
          field_thumbnail={field_thumbnail}
          imageStyleProps={{ height: scale(65) }}
          onPress={() => {
            NavigationService.navigate('ProductDetailScreen', { data: item, onNavigateToAgency: this.onNavigateToAgency });
          }}
        />
      </View>
    );
  }

  renderBottomView() {
    const { setMarkers, listProduct, markers, loading } = this.props;
    const viewContainerTranslateY = this.horizontalAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [containerHeight, 0],
      extrapolateRight: "clamp"
    });

    if (loading) {
      return (
        <AnimatedView
          style={[
            styles.containerAnimateView,
            { transform: [{ translateY: viewContainerTranslateY }] }
          ]}
        >
          <View style={[styles.wrapperProduct, { width: '90%' }]}>
            <ProductPlaceholder />
          </View>
        </AnimatedView>
      )
    }

    if (markers.length === 0) {
      <AnimatedView
        style={[
          styles.containerAnimateView,
          { transform: [{ translateY: viewContainerTranslateY }] }
        ]}
      >
        <Text style={styles.emptyText}>Chưa có sản phẩm nào</Text>
      </AnimatedView>
    }

    let listProductTmp = listProduct.slice();
    listProductTmp.unshift({ "first": true });
    return (
      <AnimatedView
        style={[
          styles.containerAnimateView,
          { transform: [{ translateY: viewContainerTranslateY }], }
        ]}
      >
        <Carousel
          ref={c => this._slider1Ref = c}
          data={listProductTmp}
          renderItem={this._renderItemWithParallax}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          hasParallaxImages={true}
          firstItem={0}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
          containerCustomStyle={styles.slider}
          contentContainerCustomStyle={styles.sliderContentContainer}
          loop={false}
          scrollEnabled={true}
          loopClonesPerSide={2}
          autoplay={false}
          autoplayDelay={500}
          autoplayInterval={3000}
          onSnapToItem={async (index) => {
            if (index > 0) {
              const data = this.props.navigation.getParam('data', '');
              const listAgency = await getListAgency(get(data, 'data.product_id', 361), 21.004584, 105.804393);
              setMarkers(listAgency);
            } else {
              setMarkers([]);
            }
          }}
        />
      </AnimatedView>
    )
  }

  render() {
    const { region, markers } = this.props;
    console.warn(markers);
    const markerTmp = markers.slice(25);
    return (
      <View style={{
        ...StyleSheet.absoluteFillObject,
        alignItems: "center"
      }}>
        <SearchHeader
          rightIcon={<Image resizeMode='contain' source={images.HDMH_ICON} style={styles.rightIcon} />}
          onRightFunc={() => Snackbar.show({
            title: 'Chức năng đang phát triển',
            duration: Snackbar.LENGTH_SHORT,
          })}
          onFocus={() => NavigationService.navigate('SearchScreen', { type: "AGENCY", goBackFromSearchScreen: this.goBackFromSearchScreen })} />
        <MapView
          ref={ref => {
            this.map = ref;
          }}
          mapPadding={{
            top: 0,
            right: 0,
            bottom: containerHeight + 10,
            left: 0
          }}
          loadingEnabled
          minZoomLevel={5}
          provider={PROVIDER_GOOGLE}
          initialRegion={region}
          style={[styles.mapStyle, { paddingTop: this.state.paddingTop }]}
          showsUserLocation
          showsMyLocationButton
          onMapReady={this._onMapReady}
        >
          {markerTmp.map((marker, index) => (
            <Marker
              //tracksViewChanges={false}
              key={index}
              coordinate={{
                latitude: Number(marker.lat),
                longitude: Number(marker.lng)
              }}
              //image={images.SHOP_MARKER}
              onPress={() => {
                FacebookAgencyPage(get(marker, 'fb_id', '100007454411856'))
              }}
            >

            </Marker>
          ))}
        </MapView>
        {this.renderBottomView()}
      </View>
    );
  }
}

export default compose(
  withNavigationFocus,
  withState('loading', 'setLoading', false),
  withState('listProduct', 'setListProduct', []),
  withState('markers', 'setMarkers', []),
  withState('region', 'setRegion', {
    latitude: LATITUDE,
    longitude: LONGITUDE,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA
  }),
  connect(
    (state) => ({
      listBestSelling: get(state, 'app.listBestSelling', []),
    }),
    {

    }
  )
)(AgencyScreen);
