import {
    Dimensions,
    StyleSheet,
    Platform,
} from 'react-native';
import { scale } from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
import { isIphoneX } from 'src/utils/iphoneXhelper';

const DEVICE_WIDTH = Dimensions.get(`window`).width;

const styles = StyleSheet.create({
    rightIcon: {
        width: scale(18),
        height: scale(16.4),
        tintColor: 'white'
    },
    mapStyle: {
        position: "absolute",
        top: isIphoneX() ? scale(64) : scale(60),
        left: 0,
        right: 0,
        bottom: 0
    },
    wrapIconStyle: {
        position: 'absolute'
    },
    navigateIconStyle: {
        //width: "100%",
        //height: "100%"
    },
    productView: {
        width: '100%',
        height: scale(120),
        alignItems: 'center',
        backgroundColor: 'white'
    },
    choiceAgencyText: {
        fontSize: scale(9),
        color: theme.blackIconColor,
        paddingBottom: 3,
        textAlign: 'center',
    },
    containerAnimateView: {
        position: "absolute",
        backgroundColor: "transparent",
        width: DEVICE_WIDTH,
        height: scale(120),
        bottom: scale(4),
        alignItems: 'center',
        justifyContent: 'center',
    },
    slider: {
        overflow: 'visible'
    },
    sliderContentContainer: {

    },
    wrapperProduct: {
        height: scale(120),
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: scale(10),
        marginHorizontal: scale(5),
        paddingTop: scale(2),
        ...Platform.select({
            ios: {
                shadowRadius: 5,
                shadowColor: "rgba(0, 0, 0, 1.0)",
                shadowOpacity: 0.3,
                shadowOffset: { width: 0, height: 5 }
            },
            android: {
                elevation: 5
            }
        })
    },
    desText: {
        color: theme.pinkishColor,
        paddingTop: scale(10)
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center"
    },
    marker: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: theme.pinkishColor,
        borderWidth: 3,
        borderColor: 'white'
    },
    emptyText: {
        fontSize: scale(9),
        color: theme.blackIconColor,
        textAlign: 'center'
    }
});

export default styles;