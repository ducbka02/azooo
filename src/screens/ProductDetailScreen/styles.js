import {StyleSheet, Dimensions, Platform} from 'react-native';
import {scale} from 'src/utils/scallingHelper';
import theme from 'src/config/theme';
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  rightIcon: {
    width: scale(15),
    height: scale(20),
  },
  bannerWrapper: {
    width: width,
    height: (width * 325) / 500,
  },
  contentWrapper: {
    paddingHorizontal: scale(5),
  },
  titleView: {
    paddingVertical: scale(10),
  },
  textTitle: {
    fontSize: scale(10),
    color: theme.blackIconColor,
  },
  tabBarUnderlineStyle: {
    backgroundColor: theme.pinkishColor,
  },
  priceInfoView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: scale(10),
  },
  priceNumberText: {
    fontSize: scale(15),
    lineHeight: scale(18),
    color: theme.green,
  },
  preSaleText: {
    fontSize: scale(10),
    lineHeight: scale(18),
    color: theme.blackLightColor,
  },
  countDownView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  finishText: {
    fontSize: scale(10),
    lineHeight: scale(18),
    color: theme.blackLightColor,
  },
  burnImage: {
    width: scale(100),
  },
  dabanText: {
    position: 'absolute',
    color: 'white',
    fontSize: scale(8),
    top: scale(17),
    right: scale(20),
  },
  privacyStyle: {
    //backgroundColor: theme.pinkLightColor,
    //borderRadius: scale(10),
    paddingHorizontal: scale(5),
    paddingVertical: scale(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  privacyText: {
    color: theme.pinkishColor,
    fontStyle: 'italic',
    fontSize: scale(10),
    lineHeight: scale(18),
  },
  privacyItemView: {
    flexDirection: 'row',
    width: (width - scale(20)) / 3,
    paddingVertical: scale(5),
    alignItems: 'center',
  },
  textItemStyle: {
    fontSize: scale(8),
    lineHeight: scale(12),
    color: theme.blackIconColor,
  },
  tabBarTextStyle: {
    fontSize: scale(9),
  },
  buttonWrapper: {
    flex: 1,
    justifyContent: 'flex-end',
    marginVertical: scale(15),
  },
  buttonContainer: {
    marginBottom: scale(10),
    height: scale(40),
    //marginLeft: scale(15),
    //marginRight: scale(15),
    borderRadius: scale(30),
    borderWidth: 1,
    borderColor: theme.pinkishColor,
    backgroundColor: theme.pinkishColor,
    justifyContent: 'center',
  },
  buttonTextStyle: {
    color: theme.pureWhite,
    fontSize: scale(12),
    lineHeight: scale(18),
    fontWeight: '600',
  },
  iconLeftStyle: {
    width: scale(20),
    height: scale(25),
  },
  detailAzooo: {
    width: width * 0.9,
    height: ((width * 0.9) / 595) * 113,
  },
  stepView: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderColor: theme.blackSuperLightColor,
    paddingVertical: scale(10),
  },
  stepViewAll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: scale(5),
    marginRight: scale(5),
    //alignItems: 'center'
  },
  showAllText: {
    color: theme.pinkishColor,
    fontSize: scale(10),
    lineHeight: scale(18),
    textAlign: 'center',
    marginTop: scale(5),
  },
  iconLeftStyle1: {
    width: scale(18),
    height: scale(18),
  },
  buttonTextStyle1: {
    color: theme.pureWhite,
    fontSize: scale(9),
    lineHeight: scale(12),
    fontWeight: '400',
  },
  buttonContainer1: {
    marginBottom: scale(10),
    height: scale(30),
    width: scale(130),
    borderRadius: scale(30),
    borderWidth: 1,
    borderColor: theme.pinkishColor,
    backgroundColor: theme.pinkishColor,
  },
  mapView: {
    width: width - scale(10),
    height: width - scale(10),
    marginBottom: scale(10),
  },
  mapStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  markerImgStyle: {
    width: scale(40),
    height: scale(40),
  },
  guideView: {
    position: 'absolute',
    backgroundColor: 'transparent',
    width: width - scale(10),
    height: scale(45),
    bottom: scale(20),
    alignItems: 'center',
    justifyContent: 'center',
  },
  guideDetailView: {
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scale(25),
    marginHorizontal: scale(10),
    paddingHorizontal: scale(5),
    ...Platform.select({
      ios: {
        shadowRadius: 2,
        shadowColor: 'rgba(0, 0, 0, 1.0)',
        shadowOpacity: 0.3,
        shadowOffset: {width: 0, height: 2},
      },
      android: {
        elevation: 5,
      },
    }),
  },
  desText: {
    color: theme.blackIconColor,
    textAlign: 'center',
    //fontSize: scale(10),
    //paddingTop: scale(6)
  },
});
