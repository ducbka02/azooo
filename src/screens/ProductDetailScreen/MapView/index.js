import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Platform,
  Dimensions,
  LayoutAnimation
} from 'react-native';
// map-related libs
import MapView from 'react-native-maps';
import SuperCluster from './SuperCluster';
import GeoViewport from '@mapbox/geo-viewport';
// components / views
import ClusterMarker from './ClusterMarker';
// libs / utils
import {
  regionToBoundingBox,
  itemToGeoJSONFeature
} from './util';

export default class ClusteredMapView extends Component {

  constructor (props) {
    super(props);

    this.state = {
      data: [], // helds renderable clusters and markers
      region: props.region || props.initialRegion, // helds current map region,
    };

    this.isAndroid = Platform.OS === 'android';
    this.dimensions = [props.width, props.height];

    this.mapRef = this.mapRef.bind(this);
    this.onClusterPress = this.onClusterPress.bind(this);
    this.onRegionChangeComplete = this.onRegionChangeComplete.bind(this);
  }

  componentDidMount () {
    this.clusterize(this.props.data);
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.data !== nextProps.data)
      this.clusterize(nextProps.data);
  }

  componentWillUpdate (nextProps, nextState) {
    if (!this.isAndroid && this.props.animateClusters && this.clustersChanged(nextState))
      LayoutAnimation.configureNext(this.props.layoutAnimationConf);
  }

  mapRef (ref) {
    this.mapview = ref;
    if (this.props._ref)
      this.props._ref(ref);
  }

  getMapRef () {
    this.mapview;
  }

  getClusteringEngine () {
    this.index;
  }

  clusterize (dataset) {
    this.index = new SuperCluster({ // eslint-disable-line new-cap
      extent: this.props.extent,
      minZoom: this.props.minZoom,
      maxZoom: this.props.maxZoom,
      radius: this.props.radius || (this.dimensions[0] * .13), // 4.5% of screen width
    });

    // get formatted GeoPoints for cluster
    const rawData = dataset.map(itemToGeoJSONFeature);

    // load geopoints into SuperCluster
    this.index.load(rawData);

    const data = this.getClusters(this.state.region);
    this.setState({data});
  }

  clustersChanged (nextState) {
    return this.state.data.length !== nextState.data.length;
  }

  onRegionChangeComplete (region) {
    let data = this.getClusters(region);
    this.setState({region, data}, () => {
      this.props.onRegionChangeComplete && this.props.onRegionChangeComplete(region, data);
    });
  }

  getClusters (region) {
    const bbox = regionToBoundingBox(region),
      viewport = (region.longitudeDelta) >= 30 ? {zoom: this.props.minZoom} : GeoViewport.viewport(bbox, this.dimensions);

    return this.index.getClusters(bbox, viewport.zoom);
  }

  onClusterPress (cluster) {

    // cluster press behavior might be extremely custom.
    if (!this.props.preserveClusterPressBehavior) {
      this.props.onClusterPress && this.props.onClusterPress(cluster.properties.cluster_id);
      return;
    }

    // //////////////////////////////////////////////////////////////////////////////////
    // NEW IMPLEMENTATION (with fitToCoordinates)
    // //////////////////////////////////////////////////////////////////////////////////
    // get cluster children
    const children = this.index.getLeaves(cluster.properties.cluster_id, this.props.clusterPressMaxChildren),
      markers = children.map(c => c.properties.item);

    // fit right around them, considering edge padding
    this.mapview.fitToCoordinates(markers.map(m => m.location), {edgePadding: this.props.edgePadding});

    this.props.onClusterPress && this.props.onClusterPress(cluster.properties.cluster_id, markers);
  }

  render () {
    let minPoint, maxPoint;
    if (this.state.region != null) {
      minPoint = {
        latitude: this.state.region.latitude + (this.state.region.latitudeDelta / 2),
        longitude: this.state.region.longitude - (this.state.region.longitudeDelta / 2)
      };

      maxPoint = {
        latitude: this.state.region.latitude - (this.state.region.latitudeDelta / 2),
        longitude: this.state.region.longitude + (this.state.region.longitudeDelta / 2)
      };
    }
    return (
      <MapView
        {...this.props}
        showsTraffic={false}
        ref={this.mapRef}
        //CuongNM add change user locion icon
        showsUserLocation={true}
        onRegionChangeComplete={this.onRegionChangeComplete}>
        {
          this.props.clusteringEnabled && this.state.data.map((d) => {
            if (d.properties.point_count === 0) {
              const {latitude, longitude} = d.properties.item.location;
              if (latitude <= minPoint.latitude && latitude >= maxPoint.latitude && longitude >= minPoint.longitude && longitude <= maxPoint.longitude) {
                return this.props.renderMarker(d.properties.item);
              } else {
                return null;
              }
            } else {
              const latitude = d.geometry.coordinates[1], longitude = d.geometry.coordinates[0];
              if (latitude <= minPoint.latitude && latitude >= maxPoint.latitude && longitude >= minPoint.longitude && longitude <= maxPoint.longitude) {
                return (
                  <ClusterMarker
                    {...d}
                    onPress={this.onClusterPress}
                    textStyle={this.props.textStyle}
                    scaleUpRatio={this.props.scaleUpRatio}
                    renderCluster={this.props.renderCluster}
                    key={`cluster-${d.properties.cluster_id}`}
                    containerStyle={this.props.containerStyle}
                    clusterInitialFontSize={this.props.clusterInitialFontSize}
                    clusterInitialDimension={this.props.clusterInitialDimension}/>
                );
              } else {
                return null;
              }
            }
          })
        }
        {
          !this.props.clusteringEnabled && this.props.data.map(d => this.props.renderMarker(d))
        }
        {this.props.children}
      </MapView>
    );
  }
}

ClusteredMapView.defaultProps = {
  minZoom: 1,
  maxZoom: 12,
  extent: 512,
  textStyle: {},
  containerStyle: {},
  animateClusters: true,
  clusteringEnabled: true,
  clusterInitialFontSize: 10,
  clusterInitialDimension: 24,
  clusterPressMaxChildren: 100,
  preserveClusterPressBehavior: true,
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
  layoutAnimationConf: LayoutAnimation.Presets.spring,
  edgePadding: {top: 50, left: 50, right: 50, bottom: 50}
};

ClusteredMapView.propTypes = {
  ...MapView.propTypes,
  // number
  radius: PropTypes.number,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  extent: PropTypes.number.isRequired,
  minZoom: PropTypes.number.isRequired,
  maxZoom: PropTypes.number.isRequired,
  clusterInitialFontSize: PropTypes.number.isRequired,
  clusterPressMaxChildren: PropTypes.number.isRequired,
  clusterInitialDimension: PropTypes.number.isRequired,
  // array
  data: PropTypes.array.isRequired,
  // func
  onExplode: PropTypes.func,
  onImplode: PropTypes.func,
  scaleUpRatio: PropTypes.func,
  renderCluster: PropTypes.func,
  onClusterPress: PropTypes.func,
  renderMarker: PropTypes.func.isRequired,
  // bool
  animateClusters: PropTypes.bool.isRequired,
  clusteringEnabled: PropTypes.bool.isRequired,
  preserveClusterPressBehavior: PropTypes.bool.isRequired,
  // object
  textStyle: PropTypes.object,
  containerStyle: PropTypes.object,
  layoutAnimationConf: PropTypes.object,
  edgePadding: PropTypes.object.isRequired,
  // string
};