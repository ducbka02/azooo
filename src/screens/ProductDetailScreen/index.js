import React from 'react';
import {
  View,
  Image,
  InteractionManager,
  ScrollView,
  Platform,
} from 'react-native';
import {compose, withState} from 'recompose';
import {connect} from 'react-redux';
import get from 'lodash/get';
import ScrollableTabView, {
  DefaultTabBar,
} from 'react-native-scrollable-tab-view';
import HTMLComponent from 'src/components/HTMLComponent';
import Text from 'src/components/Text';
import Header from 'src/components/Header';
import Button from 'src/components/Button';
import ProgressiveImage from 'src/components/ProgressiveImage';
import ProductComponent from 'src/components/ProductComponent';
import LazyComponent from 'src/components/LazyComponent';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
//import MapView from './MapView';
import images from 'src/config/images';
import theme from 'src/config/theme';
import {scale} from 'src/utils/scallingHelper';
import {isIpad} from 'src/utils/iphoneXhelper';
import styles from './styles';
import {FacebookAgencyPage} from 'src/utils/linkingHelper';
import NavigationService, {ProductShowType} from 'src/config/NavigationService';
import {
  getDetailProduct,
  getProductInCategory,
  getListAgency,
} from 'src/services/api/app';

import Permissions from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';

import * as appActions from 'src/store/actions/app';

class ProductDetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.onNavigateToAgency = this.onNavigateToAgency.bind(this);
  }

  requestLocationPermission() {
    Permissions.request('location').then(response => {
      if (response == 'authorized') {
        if (Platform.OS === 'android') {
          RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
            interval: 10000,
            fastInterval: 5000,
          })
            .then(data => {
              this.getCurrentLocation();
            })
            .catch(err => {});
        } else {
          this.getCurrentLocation();
        }
      }
    });
  }

  checkLocationPermission() {
    Permissions.check('location').then(response => {
      if (response != 'authorized') {
        this.requestLocationPermission();
      } else {
        this.getCurrentLocation();
      }
      if (Platform.OS === 'android') {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
          interval: 10000,
          fastInterval: 5000,
        })
          .then(data => {})
          .catch(err => {});
      }
    });
  }

  getCurrentLocation() {
    Geolocation.getCurrentPosition(
      position => {
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.08,
          longitudeDelta: 0.08 * 0.5,
        };

        this.onRegionChange(region);
      },
      error => {
        this.onRegionChange(this.props.currentRegion);
      },
      //{ enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  async onRegionChange(region) {
    const {setCurrentRegion, setMarkers, navigation} = this.props;
    setCurrentRegion(region);
    if (this.map) {
      this.map.animateToRegion(region);
    }
    const data = navigation.getParam('data', {});
    const product_id = get(data, 'product_id', undefined);
    if (product_id !== undefined) {
      const listAgency = await getListAgency(
        product_id,
        region.latitude,
        region.longitude,
      );
      setMarkers(listAgency);
    }
  }

  async componentDidMount() {
    const {
      navigation,
      setContentProduct,
      setLoading,
      setListProductRelate,
      setMarkers,
    } = this.props;
    InteractionManager.runAfterInteractions(async () => {
      setLoading(1);
      this.checkLocationPermission();
      this.mapTimeout = setTimeout(async () => {
        const {currentRegion} = this.props;
        try {
          const data = navigation.getParam('data', {});
          const product_id = get(data, 'product_id', undefined);
          if (product_id !== undefined) {
            const content = await getDetailProduct(product_id);
            setContentProduct(content[0]);
            setLoading(2);
            const listAgency = await getListAgency(
              product_id,
              currentRegion.latitude,
              currentRegion.longitude,
            );
            setMarkers(listAgency);
            setLoading(3);
            if (this.map) {
              this.map.animateToRegion(currentRegion);
            }
            const category_id = get(content[0], 'category_id', 34);
            let list = await getProductInCategory(category_id);
            setListProductRelate(list);
            setLoading(4);
          }
        } catch (err) {
          setLoading(4);
          setListProductRelate([]);
          setContentProduct('');
          if (this.map) {
            this.map.animateToRegion(currentRegion);
          }
        }
      }, 500);
    });
  }

  componentWillUnmount() {
    if (this.mapTimeout) {
      clearTimeout(this.mapTimeout);
    }
  }

  onNavigateToAgency(data) {
    NavigationService.navigate('Agency', {data: data});
  }

  renderHtml() {
    const {contentProduct} = this.props;

    return (
      <ScrollableTabView
        style={{}}
        initialPage={0}
        tabBarActiveTextColor={theme.pinkishColor}
        tabBarInactiveTextColor={theme.blackIconColor}
        tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
        renderTabBar={() => <DefaultTabBar />}>
        <View tabLabel="CHI TIẾT SẢN PHẨM">
          <HTMLComponent
            onLoadCompleted={() => {}}
            html={get(contentProduct, 'body_detail', undefined)}
          />
        </View>
        <View tabLabel="HƯỚNG DẪN SỬ DỤNG">
          <HTMLComponent
            onLoadCompleted={() => {}}
            html={get(contentProduct, 'body_guide', undefined)}
          />
        </View>
      </ScrollableTabView>
    );
  }

  renderContent() {
    const {
      navigation,
      loading,
      listProductRelate,
      markers,
      currentRegion,
    } = this.props;
    const data = navigation.getParam('data', {});
    const title = get(data, 'title', 'Chi tiết sản phẩm');
    const field_image =
      get(data, 'field_image', undefined) === undefined
        ? get(data, 'field_images', undefined)
        : get(data, 'field_image', undefined);
    const price__number = get(data, 'price__number', '0 đ');
    //const markersFilter = markers.filter(marker => Functions.getDistanceFromLatLonInKm(Number(marker.lat), Number(marker.lng), currentRegion.latitude, currentRegion.longitude) < 5);
    //console.warn(markers.length);

    return (
      <ScrollView style={{flex: 1}}>
        <View style={styles.bannerWrapper}>
          <ProgressiveImage
            thumbnailSource={images.DEFAULT}
            style={{width: '100%', height: '100%'}}
            source={{
              uri: field_image,
            }}
          />
        </View>
        <View style={styles.contentWrapper}>
          <View style={styles.titleView}>
            <Text style={styles.textTitle}>{title}</Text>
            <Text style={styles.priceNumberText}>{price__number}</Text>
          </View>
          <View style={styles.privacyStyle}>
            <Image style={styles.detailAzooo} source={images.DETAIL_AZOOO} />
          </View>
          {this.renderHtml()}

          <View style={styles.stepView}>
            <View style={styles.mapView}>
              <LazyComponent showLoading={loading < 3}>
                <MapView
                  ref={ref => {
                    this.map = ref;
                  }}
                  mapPadding={{
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                  }}
                  loadingEnabled
                  minZoomLevel={5}
                  provider={PROVIDER_GOOGLE}
                  initialRegion={currentRegion}
                  style={styles.mapStyle}
                  showsUserLocation
                  //showsMyLocationButton
                >
                  {markers.map((marker, index) => (
                    <Marker
                      //tracksViewChanges={false}
                      key={index}
                      anchor={{x: 0.5, y: 0.5}}
                      style={{
                        width: isIpad() ? scale(30) : scale(40),
                        height: isIpad() ? scale(30) : scale(40),
                      }}
                      coordinate={{
                        latitude: Number(marker.lat),
                        longitude: Number(marker.lng),
                      }}
                      //image={images.SHOP_MARKER}
                      onPress={() => {
                        FacebookAgencyPage(
                          get(marker, 'fb_id', '100007454411856'),
                        );
                      }}>
                      <Image
                        style={styles.markerImgStyle}
                        source={images.SHOP_MARKER}
                      />
                    </Marker>
                  ))}
                </MapView>
                <View style={styles.guideView}>
                  <View style={styles.guideDetailView}>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={styles.desText}>
                        Click{' '}
                        <Image
                          style={{width: scale(25), height: scale(25)}}
                          source={images.SHOP_MARKER}
                        />
                        để chọn đại lý gần bạn, chat và mua hàng.
                      </Text>
                    </View>
                  </View>
                </View>
              </LazyComponent>
            </View>
            {listProductRelate.length > 0 && (
              <View>
                <View style={styles.stepViewAll}>
                  <Button
                    disabled
                    leftIcon={
                      <Image
                        source={images.BADGESTARNORMAL}
                        resizeMode="contain"
                        style={{height: '100%'}}
                      />
                    }
                    iconLeftStyle={styles.iconLeftStyle1}
                    title={'SẢN PHẨM LIÊN QUAN'}
                    titleStyle={styles.buttonTextStyle1}
                    styleContainer={styles.buttonContainer1}
                  />
                  <Text
                    style={styles.showAllText}
                    onPress={() => {
                      NavigationService.goToCategoryNavigator({
                        type: ProductShowType.PRODUCT_LIST,
                        id: listProductRelate,
                      });
                    }}>
                    Xem tất cả
                  </Text>
                </View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {listProductRelate.map((item, index) => {
                    const {
                      product_id,
                      title,
                      field_image,
                      category_name,
                      price__number,
                      promotion_price,
                      field_thumbnail,
                    } = item;
                    return (
                      <ProductComponent
                        key={`${product_id}-${index}`}
                        title={title}
                        field_image={field_image}
                        category_name={category_name}
                        price__number={price__number}
                        promotion_price={promotion_price}
                        field_thumbnail={field_thumbnail}
                        onPress={() => {
                          navigation.push('ProductDetailScreen', {
                            data: item,
                            onNavigateToAgency: this.onNavigateToAgency,
                          });
                        }}
                      />
                    );
                  })}
                </ScrollView>
              </View>
            )}
          </View>
        </View>
      </ScrollView>
    );
  }

  render() {
    const {navigation} = this.props;
    const data = navigation.getParam('data', {});
    const title = get(data, 'title', 'Chi tiết sản phẩm');
    return (
      <View style={styles.wrapper}>
        <Header
          back
          titleClick
          title={title}
          rightIcon={
            <Image source={images.NOTIFICATION} style={styles.rightIcon} />
          }
          onRightFunc={() =>
            this.props.navigation.navigate('NotificationScreen')
          }
        />
        {this.renderContent()}
      </View>
    );
  }
}

export default compose(
  withState('contentProduct', 'setContentProduct', []),
  withState('listProductRelate', 'setListProductRelate', []),
  withState('markers', 'setMarkers', []),
  withState('loading', 'setLoading', 0),
  withState('currentRegion', 'setCurrentRegion', {
    latitude: 21.004584,
    longitude: 105.804393,
    latitudeDelta: 0.08,
    longitudeDelta: 0.08 * 0.5,
  }),
  connect(
    state => ({
      region: get(state, 'app.region', {}),
    }),
    {getAppInfo: appActions.getAppInfo},
  ),
)(ProductDetailScreen);
