import * as React from 'react';
import NavigationService from 'src/config/NavigationService';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
//import createNativeStackNavigator from 'react-native-screens/createNativeStackNavigator';
import {fadeIn} from 'src/utils/transitionHelper';
import AuthLoadingScreen from './AuthLoading';
import MainNavigator from './MainNavigator';
import CategoryNavigator from './CategoryNavigator';
import ReplyScreen from './Help/ReplyScreen';
import RegisterAgencyScreen from './Help/RegisterAgencyScreen';
import HelpCenterScreen from './Help/HelpCenterScreen';
import DetailHelpScreen from './Help/DetailHelpScreen';
import SuccessScreen from './Help/SuccessScreen';
import ProductDetailScreen from './ProductDetailScreen';
import AgencyScreen from './MainNavigator/AgencyScreen';
import SearchScreen from './SearchScreen';
import CollectionListScreen from './CollectionListScreen';
import NotificationScreen from './NotificationScreen';

const handleCustomTransition = ({scenes}) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  if (prevScene && nextScene.route.routeName === 'SearchScreen') {
    return fadeIn();
  }
};

const AuthStack = createStackNavigator(
  {
    AuthLoadingScreen,
  },
  {
    initialRouteName: 'AuthLoadingScreen',
    headerMode: 'none',
    defaultNavigationOptions: {
      //gesturesEnabled: false,
    },
    navigationOptions: {
      //gesturesEnabled: false,
    },
  },
);

const AppStack = createStackNavigator(
  {
    MainNavigator,
    CategoryNavigator,
    ReplyScreen,
    RegisterAgencyScreen,
    HelpCenterScreen,
    DetailHelpScreen,
    SuccessScreen,
    ProductDetailScreen,
    AgencyScreen,
    SearchScreen,
    CollectionListScreen,
    NotificationScreen,
  },
  {
    initialRouteName: 'MainNavigator',
    transitionConfig: nav => handleCustomTransition(nav),
    headerMode: 'none',
    defaultNavigationOptions: {
      //gesturesEnabled: false,
    },
    navigationOptions: {
      //gesturesEnabled: false,
    },
  },
);

const MainContainer = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthStack,
      App: AppStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);

class Screens extends React.Component {
  render() {
    return (
      <MainContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}

export default Screens;
